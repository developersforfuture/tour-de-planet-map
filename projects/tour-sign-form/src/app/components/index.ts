import { AppComponent } from './app.component';
import { SignInFormComponent } from './sign-in-form/sign-in-form.component';

export const components: any[] = [
    AppComponent,
    SignInFormComponent,
];

export * from './app.component';
export * from './sign-in-form/sign-in-form.component';
