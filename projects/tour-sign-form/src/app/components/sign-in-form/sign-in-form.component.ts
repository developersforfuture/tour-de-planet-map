import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TourPoint } from 'projects/tour-commons/src/lib/model';

export interface SignInFormData {
  name: string;
  points: TourPoint[];
}
@Component({
  selector: 'app-sign-in-form',
  templateUrl: './sign-in-form.component.html',
  styleUrls: ['./sign-in-form.component.scss']
})
export class SignInFormComponent implements OnInit {
  signInForm;
  @Output() newData = new EventEmitter<SignInFormData>();
  constructor(private formBuilder: FormBuilder) {
    this.signInForm = this.formBuilder.group({name: ''});
  }

  ngOnInit() {
  }
  onSubmit(signInData: SignInFormData) {
    this.signInForm.reset();
  }
}
