import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { GeoData, TourModel } from 'projects/tour-commons/src/lib/model';

@Injectable({
  providedIn: 'root'
})
export class GeoDataService {

  constructor(private $http: HttpClient) { }

  getGeoEntries(): Observable<GeoData> {
    return this.$http.get<GeoData>(environment.apiUrl + '/geo')
    .pipe(catchError((error: any) => throwError(error.json)));
  }
}
