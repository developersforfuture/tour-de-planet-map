import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import * as fromComponents from '../components';

import { AppComponent } from '../components/app.component';
import { TourCommonsModule } from 'projects/tour-commons/src/public-api';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    ...fromComponents.components
  ],
  imports: [
    BrowserModule,
    TourCommonsModule,
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
