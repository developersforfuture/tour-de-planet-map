import { GeoDataService } from './geo_data.service';

export const services = [
    GeoDataService,
];

export * from './geo_data.service';
