import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';

export const components: any[] = [
    AppComponent,
    MapComponent,
];

export * from './app.component';
export * from './map/map.component';
