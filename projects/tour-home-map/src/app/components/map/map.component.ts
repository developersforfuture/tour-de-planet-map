import { AfterViewInit, Component, Input } from '@angular/core';
import { GeoDataService } from '../../services/geo_data.service';
import { OlMapDecoratorService, GeoData, TourEvent, TourSegmentModel, TourModel } from 'projects/tour-commons/src/public-api';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit {
  private map;
  @Input() zoom: number;
  @Input() latitude: number;
  @Input() longitude: number;
  eventsInPopup: TourEvent[] = [];
  segmentsInPopup: TourSegmentModel[] = [];

  constructor(private mapService: OlMapDecoratorService, private geoDataService: GeoDataService) { }

  ngAfterViewInit(): void {
    this.geoDataService.getGeoEntries().subscribe((geoData: GeoData) => {
      this.mapService.initMap(
        {
          zoom: this.zoom,
          latitude: this.latitude,
          longitude: this.longitude,
          popup: 'popup',
          map: 'map'
        },
        geoData
      );

      this.mapService.onSingleClick((event: {events: TourEvent[], segments: TourSegmentModel[], tours: TourModel[]}) => {
        this.eventsInPopup = event.events;
        this.segmentsInPopup = event.segments;
      });
    });
  }

  getEventTitle(event: TourEvent, short: boolean): string {
    let title = (event.author !== undefined ? event.author + ': ' : '') + event.title + ', ';
    title += (event.point.name ? event.point.name + ', ' : '');
    if (short) {
      return title;
    }
    title += new Date(event.date_from * 1000)
    .toLocaleString(undefined, {year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit'});
    if (event.date_to) {
      title += ' - ' + new Date(event.date_to * 1000)
      .toLocaleString(undefined, {year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit'});
    }
    return title;
  }

  getSegmentTitle(segment: TourSegmentModel): string {
    let title = '';
    if (segment.from.name || segment.to.name) {
      title += '(' + (segment.from.name ? segment.from.name + ' - ' : '') + segment.to.name + ')';
    }
    if (segment.tour) {
      title += ' - Tour: ' + segment.tour.title;
      if (segment.tour.from.name || segment.tour.to.name) {
        title += ' (' + (segment.tour.from.name ? segment.tour.from.name + ' - ' : '') + segment.tour.to.name + ')';
      }
    }
    return title;
  }

  dateToString(dateInSeconds: number): string {
    return new Date(dateInSeconds * 1000)
      .toLocaleString(undefined, {year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit'});
  }
}
