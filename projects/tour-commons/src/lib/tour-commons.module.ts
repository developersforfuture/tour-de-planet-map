import { NgModule } from '@angular/core';
import { TourCommonsComponent } from './tour-commons.component';
import * as fromServices from './services';

@NgModule({
  declarations: [TourCommonsComponent],
  imports: [
  ],
  exports: [TourCommonsComponent]
})
export class TourCommonsModule { }
