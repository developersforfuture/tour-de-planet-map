import { TourSegmentModel } from './tour_segment.model';
import { TourOrganizerModel } from './tour_organizer.model';
import { TourPoint } from './tour_point.model';

export interface TourModel {
    id: number;
    title: string;
    organizer: TourOrganizerModel;
    segments: TourSegmentModel[];
    from: TourPoint;
    to: TourPoint;
}
