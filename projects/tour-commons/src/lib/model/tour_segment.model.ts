import { TourPoint } from './tour_point.model';
import { TourModel } from './tour.model';

export class TourSegmentModel {
    id: number;
    title?: string;
    from: TourPoint;
    to: TourPoint;
    tour?: TourModel;
}
