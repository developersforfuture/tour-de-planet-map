import { TourEvent } from './tour_event.model';
import { TourModel } from './tour.model';

export interface GeoData {
    events: TourEvent;
    tours: TourModel[];
}
