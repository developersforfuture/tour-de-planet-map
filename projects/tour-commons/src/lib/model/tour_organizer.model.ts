export enum TourOrganizerType {
    SINGLE = 'single',
    GROUP = 'group',
}
export interface TourOrganizerModel {
    name: string;
    type: TourOrganizerType.SINGLE | TourOrganizerType.GROUP;
}
