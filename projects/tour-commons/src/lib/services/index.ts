import { OlMapDecoratorService } from './ol-map-decorator.service';

export const services = [
    OlMapDecoratorService
];

export * from './ol-map-decorator.service';
