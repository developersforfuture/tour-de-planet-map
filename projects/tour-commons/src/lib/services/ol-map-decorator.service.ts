import { Injectable } from '@angular/core';
import * as ol from 'openlayers';
import { MapConfigModel } from '../model/map_config.model';
import { GeoData, TourPoint, TourEvent, TourSegmentModel} from '../model';

@Injectable({
  providedIn: 'root'
})
export class OlMapDecoratorService {
  private map: ol.Map;
  private overlay: ol.OverLay;
  private popup: HTMLElement;
  private source: ol.source.OSM;
  private events: {[id: string]: TourEvent} = {};
  private tourSegments: {[id: string]: TourSegmentModel} = {};
  private points: {[id: string]: TourPoint} = {};

  constructor() { }


  initMap(config: MapConfigModel, geoData: GeoData): void {
    this.source = new ol.source.OSM();
    this.popup = document.getElementById(config.popup);
    this.overlay = new ol.Overlay({element: this.popup});    const entries = {};
    const features = [];

    for (const [eventId, event] of Object.entries(geoData.events)) {
      const feature = new ol.Feature(
        new ol.geom.Point(ol.proj.fromLonLat([event.point.lng, event.point.lat]))
      );
      const id = 'event_' + eventId;
      feature.setId(id);
      features.push(feature);
      this.events[id] = event;
    }

    for (const [counter, tour] of Object.entries(geoData.tours)) {
      const tourId = tour.id;
      const tourFromFeature = new ol.Feature(
        new ol.geom.Point(ol.proj.fromLonLat([tour.from.lng, tour.from.lat]))
      );
      tourFromFeature.setId('tour_from_' + tourId);
      features.push(tourFromFeature);
      this.points['tour_from_' + tourId] = tour.from;

      const tourToFeature = new ol.Feature(
        new ol.geom.Point(ol.proj.fromLonLat([tour.from.lng, tour.from.lat]))
      );
      tourToFeature.setId('tour_to_' + tourId);
      features.push(tourToFeature);
      this.points['tour_to_' + tourId] = tour.to;

      for (const [segmentCounter, tourSegment] of Object.entries(tour.segments)) {
        const segmentId = tourSegment.id;
        tourSegment.tour = tour;
        const feature = new ol.Feature(
          new ol.geom.LineString([
            ol.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat]),
            ol.proj.fromLonLat([tourSegment.to.lng, tourSegment.to.lat])
          ])
        );
        feature.setId('segment_' + segmentId);
        features.push(feature);
        this.tourSegments['segment_' + segmentId] = tourSegment;

        const fromFeature = new ol.Feature(
          new ol.geom.Point(ol.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat]))
        );
        fromFeature.setId('segment_from_' + segmentId);
        features.push(fromFeature);
        this.points['segment_from_' + segmentId] = tourSegment.from;

        const toFeature = new ol.Feature(
          new ol.geom.Point(ol.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat]))
        );
        toFeature.setId('segment_to_' + segmentId);
        features.push(toFeature);
        this.points['segment_to_' + segmentId] = tourSegment.to;
      }
    }

    const layers = [
      new ol.layer.Tile({source: this.source}),
      new ol.layer.Vector({
        source: new ol.source.Vector({features}),
        zIndex: Infinity,
        style: (feature) => {
          const id = feature.getId();
          if (this.events[id]) {
            const entry = this.events[id];

            return new ol.style.Style({
              image: new ol.style.Circle({
                radius: 6,
                fill: new ol.style.Fill({color: [225, 0, 255, ]})
              })
            });
          }

          if (this.points[id]) {
            const [domain, loc, entryId] = id.split(/\_/);
            if (loc === 'from' || loc === 'to') {
              if (domain === 'segment') {
                return new ol.style.Style({
                  image: new ol.style.Circle({
                    radius: 7,
                    fill: new ol.style.Fill({color: [0, 0, 255, ]})
                  })
                });
              } else if (domain === 'tour') {
                return new ol.style.Style({
                  image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({color: [0, 255, 255, 0.6]})
                  })
                });
              }
            }
          }

          return new ol.style.Style({
            stroke: new ol.style.Stroke({
              width: 3,
              color: [255, 0, 0, 1]
            }),
            fill: new ol.style.Fill({
              color: [0, 0, 255, 0.6]
            })
          });
        },
      })
    ];

    this.map = new ol.Map({
      controls: ol.control.defaults({rotate: false}).extend([
        new ol.control.FullScreen(),
        new ol.control.OverviewMap({
          layers: [
            new ol.layer.Tile({source: this.source})
          ]
        }),
        new ol.control.ScaleLine({minWidth: 120})
      ]),
      interactions: ol.interaction.defaults({
        altShiftDragRotate: false,
        pinchRotate: false
      }),
      layers,
      overlays: [this.overlay],
      target: config.map,
      view: new ol.View({
        center: ol.proj.fromLonLat([config.longitude, config.latitude]),
        zoom: config.zoom
      })
    });

    // todo: move them back to component, where it belongs to.
    this.map.on('pointermove', (event) => {
      const pixel = this.map.getEventPixel(event.originalEvent);
      const hit = this.map.hasFeatureAtPixel(pixel, {hitTolerance: 10});
      document.getElementById(this.map.getTarget()).style.cursor = hit ? 'pointer' : '';
    });
  }

  onSingleClick(fn: CallableFunction) {
    this.map.on('singleclick', (event) => {
      const usedEvents = [];
      const usedTourSegments = [];
      this.map.forEachFeatureAtPixel(event.pixel, (feature, layer) => {
        const id: string = feature.getId();
        if (id === undefined) {
          return;
        }

        if (this.events[id]) {
          usedEvents.push(this.events[id]);
          return;
        } else if (this.points[id]) {
          const [domain, loc, entryId] = id.split(/\_/);

          if (this.tourSegments[domain + '_' + entryId]) {
            usedTourSegments.push(this.tourSegments[domain + '_' + entryId]);
            return;
          }
        }
      }, {hitTolerance: 10});
      usedTourSegments.sort((x, y) => x - y);
      usedEvents.sort((x, y) => x - y);

      let lng = 0;
      let lat = 0;
      let length = 0;

      usedEvents.forEach(tourEvent => {
        lng += tourEvent.point.lng;
        lat += tourEvent.point.lat;
      });

      length += usedEvents.length;
      // usedTourSegments.forEach(segment => {
      //   lng += segment.to.lng - segment.from.lng;
      //   lat += segment.to.lat - segment.from.lat;
      // });
      // length += usedTourSegments.length;

      lng /= length;
      lat /= length;

      if (usedEvents.length || usedTourSegments.length) {
        this.overlay.setPosition(ol.proj.fromLonLat([lng, lat]));
      } else {
        this.overlay.setPosition(undefined);
      }

      fn({events: usedEvents, segments: usedTourSegments});
    });
  }

  private dateToColor =  (date) => {
    const minDate = new Date(date.getFullYear(), date.getMonth() - 1, 0);
    const maxDate = new Date(date.getFullYear(), date.getMonth(), 0);
    return 'hsl(' + ((date.getTime() - minDate.getTime()) / (maxDate.getTime() - minDate.getTime())) + 'turn, 100%, 50%)';
  }
}
