# TourCommons

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.14.

## Code scaffolding

Run `ng generate component component-name --project tour-commons` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project tour-commons`.
> Note: Don't forget to add `--project tour-commons` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

Run `ng build tour-commons`, `ng build tour-home-map` to run them separate.

## Running localy

Run `npm run json-server` (as a fake backend) and  `ng serve -o --p 4203 tour-home-map` in separate terminals or run `ng serve -o tour-sign-form` for the sign in form.

## Publishing

After building your library with `ng build tour-commons`, go to the dist folder `cd dist/tour-commons` and run `npm publish`.

## Running unit tests

Run `ng test tour-commons` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
