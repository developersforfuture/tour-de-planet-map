function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "../../node_modules/raw-loader/dist/cjs.js!./src/app/components/app.component.html":
  /*!*****************************************************************************************************************************************************!*\
    !*** /home/maximilian/OpenSource/DevelopersForFuture/tour-de-rebel-map/node_modules/raw-loader/dist/cjs.js!./src/app/components/app.component.html ***!
    \*****************************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<app-sign-in-form></app-sign-in-form>";
    /***/
  },

  /***/
  "../../node_modules/raw-loader/dist/cjs.js!./src/app/components/sign-in-form/sign-in-form.component.html":
  /*!***************************************************************************************************************************************************************************!*\
    !*** /home/maximilian/OpenSource/DevelopersForFuture/tour-de-rebel-map/node_modules/raw-loader/dist/cjs.js!./src/app/components/sign-in-form/sign-in-form.component.html ***!
    \***************************************************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsSignInFormSignInFormComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<form [formGroup]=\"signInForm\" (ngSubmit)=\"onSubmit(signInForm.value)\">\n    <div class=\"form-group form-inline\">\n        <label for=\"name\">Name/Organisation</label>\n        <input type=\"text\" class=\"form-control\" id=\"name\" placeholder=\"Name/Organisation\" formControlName=\"name\">\n    </div>\n    <div class=\"form-group form-inline\">\n        <label for=\"name\">Name/Organisation</label>\n        <input type=\"text\" class=\"form-control\" id=\"name\" placeholder=\"Name/Organisation\">\n    </div>\n\n    <button class=\"button\" type=\"submit\">Submit</button>\n</form>";
    /***/
  },

  /***/
  "../../node_modules/tslib/tslib.es6.js":
  /*!*********************************************************************************************************!*\
    !*** /home/maximilian/OpenSource/DevelopersForFuture/tour-de-rebel-map/node_modules/tslib/tslib.es6.js ***!
    \*********************************************************************************************************/

  /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */

  /***/
  function node_modulesTslibTslibEs6Js(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__extends", function () {
      return __extends;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__assign", function () {
      return _assign;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__rest", function () {
      return __rest;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__decorate", function () {
      return __decorate;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__param", function () {
      return __param;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__metadata", function () {
      return __metadata;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__awaiter", function () {
      return __awaiter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__generator", function () {
      return __generator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__exportStar", function () {
      return __exportStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__values", function () {
      return __values;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__read", function () {
      return __read;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spread", function () {
      return __spread;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () {
      return __spreadArrays;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__await", function () {
      return __await;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () {
      return __asyncGenerator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () {
      return __asyncDelegator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncValues", function () {
      return __asyncValues;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () {
      return __makeTemplateObject;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importStar", function () {
      return __importStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importDefault", function () {
      return __importDefault;
    });
    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0
    
    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.
    
    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    /* global Reflect, Promise */


    var _extendStatics = function extendStatics(d, b) {
      _extendStatics = Object.setPrototypeOf || {
        __proto__: []
      } instanceof Array && function (d, b) {
        d.__proto__ = b;
      } || function (d, b) {
        for (var p in b) {
          if (b.hasOwnProperty(p)) d[p] = b[p];
        }
      };

      return _extendStatics(d, b);
    };

    function __extends(d, b) {
      _extendStatics(d, b);

      function __() {
        this.constructor = d;
      }

      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var _assign = function __assign() {
      _assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];

          for (var p in s) {
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
          }
        }

        return t;
      };

      return _assign.apply(this, arguments);
    };

    function __rest(s, e) {
      var t = {};

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
      }

      if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
      }
      return t;
    }

    function __decorate(decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    }

    function __metadata(metadataKey, metadataValue) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    }

    function __generator(thisArg, body) {
      var _ = {
        label: 0,
        sent: function sent() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
          f,
          y,
          t,
          g;
      return g = {
        next: verb(0),
        "throw": verb(1),
        "return": verb(2)
      }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
        return this;
      }), g;

      function verb(n) {
        return function (v) {
          return step([n, v]);
        };
      }

      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");

        while (_) {
          try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];

            switch (op[0]) {
              case 0:
              case 1:
                t = op;
                break;

              case 4:
                _.label++;
                return {
                  value: op[1],
                  done: false
                };

              case 5:
                _.label++;
                y = op[1];
                op = [0];
                continue;

              case 7:
                op = _.ops.pop();

                _.trys.pop();

                continue;

              default:
                if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                  _ = 0;
                  continue;
                }

                if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                  _.label = op[1];
                  break;
                }

                if (op[0] === 6 && _.label < t[1]) {
                  _.label = t[1];
                  t = op;
                  break;
                }

                if (t && _.label < t[2]) {
                  _.label = t[2];

                  _.ops.push(op);

                  break;
                }

                if (t[2]) _.ops.pop();

                _.trys.pop();

                continue;
            }

            op = body.call(thisArg, _);
          } catch (e) {
            op = [6, e];
            y = 0;
          } finally {
            f = t = 0;
          }
        }

        if (op[0] & 5) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    }

    function __exportStar(m, exports) {
      for (var p in m) {
        if (!exports.hasOwnProperty(p)) exports[p] = m[p];
      }
    }

    function __values(o) {
      var m = typeof Symbol === "function" && o[Symbol.iterator],
          i = 0;
      if (m) return m.call(o);
      return {
        next: function next() {
          if (o && i >= o.length) o = void 0;
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
    }

    function __read(o, n) {
      var m = typeof Symbol === "function" && o[Symbol.iterator];
      if (!m) return o;
      var i = m.call(o),
          r,
          ar = [],
          e;

      try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) {
          ar.push(r.value);
        }
      } catch (error) {
        e = {
          error: error
        };
      } finally {
        try {
          if (r && !r.done && (m = i["return"])) m.call(i);
        } finally {
          if (e) throw e.error;
        }
      }

      return ar;
    }

    function __spread() {
      for (var ar = [], i = 0; i < arguments.length; i++) {
        ar = ar.concat(__read(arguments[i]));
      }

      return ar;
    }

    function __spreadArrays() {
      for (var s = 0, i = 0, il = arguments.length; i < il; i++) {
        s += arguments[i].length;
      }

      for (var r = Array(s), k = 0, i = 0; i < il; i++) {
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) {
          r[k] = a[j];
        }
      }

      return r;
    }

    ;

    function __await(v) {
      return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var g = generator.apply(thisArg, _arguments || []),
          i,
          q = [];
      return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i;

      function verb(n) {
        if (g[n]) i[n] = function (v) {
          return new Promise(function (a, b) {
            q.push([n, v, a, b]) > 1 || resume(n, v);
          });
        };
      }

      function resume(n, v) {
        try {
          step(g[n](v));
        } catch (e) {
          settle(q[0][3], e);
        }
      }

      function step(r) {
        r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
      }

      function fulfill(value) {
        resume("next", value);
      }

      function reject(value) {
        resume("throw", value);
      }

      function settle(f, v) {
        if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
      }
    }

    function __asyncDelegator(o) {
      var i, p;
      return i = {}, verb("next"), verb("throw", function (e) {
        throw e;
      }), verb("return"), i[Symbol.iterator] = function () {
        return this;
      }, i;

      function verb(n, f) {
        i[n] = o[n] ? function (v) {
          return (p = !p) ? {
            value: __await(o[n](v)),
            done: n === "return"
          } : f ? f(v) : v;
        } : f;
      }
    }

    function __asyncValues(o) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var m = o[Symbol.asyncIterator],
          i;
      return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i);

      function verb(n) {
        i[n] = o[n] && function (v) {
          return new Promise(function (resolve, reject) {
            v = o[n](v), settle(resolve, reject, v.done, v.value);
          });
        };
      }

      function settle(resolve, reject, d, v) {
        Promise.resolve(v).then(function (v) {
          resolve({
            value: v,
            done: d
          });
        }, reject);
      }
    }

    function __makeTemplateObject(cooked, raw) {
      if (Object.defineProperty) {
        Object.defineProperty(cooked, "raw", {
          value: raw
        });
      } else {
        cooked.raw = raw;
      }

      return cooked;
    }

    ;

    function __importStar(mod) {
      if (mod && mod.__esModule) return mod;
      var result = {};
      if (mod != null) for (var k in mod) {
        if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
      }
      result.default = mod;
      return result;
    }

    function __importDefault(mod) {
      return mod && mod.__esModule ? mod : {
        default: mod
      };
    }
    /***/

  },

  /***/
  "../tour-commons/src/lib/model/index.ts":
  /*!**********************************************!*\
    !*** ../tour-commons/src/lib/model/index.ts ***!
    \**********************************************/

  /*! exports provided: TourOrganizerType, TourSegmentModel */

  /***/
  function tourCommonsSrcLibModelIndexTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _tour_organizer_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./tour_organizer.model */
    "../tour-commons/src/lib/model/tour_organizer.model.ts");
    /* harmony reexport (safe) */


    __webpack_require__.d(__webpack_exports__, "TourOrganizerType", function () {
      return _tour_organizer_model__WEBPACK_IMPORTED_MODULE_1__["TourOrganizerType"];
    });
    /* harmony import */


    var _tour_segment_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./tour_segment.model */
    "../tour-commons/src/lib/model/tour_segment.model.ts");
    /* harmony reexport (safe) */


    __webpack_require__.d(__webpack_exports__, "TourSegmentModel", function () {
      return _tour_segment_model__WEBPACK_IMPORTED_MODULE_2__["TourSegmentModel"];
    });
    /***/

  },

  /***/
  "../tour-commons/src/lib/model/tour_organizer.model.ts":
  /*!*************************************************************!*\
    !*** ../tour-commons/src/lib/model/tour_organizer.model.ts ***!
    \*************************************************************/

  /*! exports provided: TourOrganizerType */

  /***/
  function tourCommonsSrcLibModelTour_organizerModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TourOrganizerType", function () {
      return TourOrganizerType;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");

    var TourOrganizerType;

    (function (TourOrganizerType) {
      TourOrganizerType["SINGLE"] = "single";
      TourOrganizerType["GROUP"] = "group";
    })(TourOrganizerType || (TourOrganizerType = {}));
    /***/

  },

  /***/
  "../tour-commons/src/lib/model/tour_segment.model.ts":
  /*!***********************************************************!*\
    !*** ../tour-commons/src/lib/model/tour_segment.model.ts ***!
    \***********************************************************/

  /*! exports provided: TourSegmentModel */

  /***/
  function tourCommonsSrcLibModelTour_segmentModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TourSegmentModel", function () {
      return TourSegmentModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");

    var TourSegmentModel = function TourSegmentModel() {
      _classCallCheck(this, TourSegmentModel);
    };
    /***/

  },

  /***/
  "../tour-commons/src/lib/services/index.ts":
  /*!*************************************************!*\
    !*** ../tour-commons/src/lib/services/index.ts ***!
    \*************************************************/

  /*! exports provided: services, OlMapDecoratorService */

  /***/
  function tourCommonsSrcLibServicesIndexTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "services", function () {
      return services;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _ol_map_decorator_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./ol-map-decorator.service */
    "../tour-commons/src/lib/services/ol-map-decorator.service.ts");
    /* harmony reexport (safe) */


    __webpack_require__.d(__webpack_exports__, "OlMapDecoratorService", function () {
      return _ol_map_decorator_service__WEBPACK_IMPORTED_MODULE_1__["OlMapDecoratorService"];
    });

    var services = [_ol_map_decorator_service__WEBPACK_IMPORTED_MODULE_1__["OlMapDecoratorService"]];
    /***/
  },

  /***/
  "../tour-commons/src/lib/services/ol-map-decorator.service.ts":
  /*!********************************************************************!*\
    !*** ../tour-commons/src/lib/services/ol-map-decorator.service.ts ***!
    \********************************************************************/

  /*! exports provided: OlMapDecoratorService */

  /***/
  function tourCommonsSrcLibServicesOlMapDecoratorServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "OlMapDecoratorService", function () {
      return OlMapDecoratorService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var openlayers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! openlayers */
    "../tour-commons/node_modules/openlayers/dist/ol.js");
    /* harmony import */


    var openlayers__WEBPACK_IMPORTED_MODULE_2___default =
    /*#__PURE__*/
    __webpack_require__.n(openlayers__WEBPACK_IMPORTED_MODULE_2__);

    var OlMapDecoratorService =
    /*#__PURE__*/
    function () {
      function OlMapDecoratorService() {
        _classCallCheck(this, OlMapDecoratorService);

        this.events = {};
        this.tourSegments = {};
        this.points = {};

        this.dateToColor = function (date) {
          var minDate = new Date(date.getFullYear(), date.getMonth() - 1, 0);
          var maxDate = new Date(date.getFullYear(), date.getMonth(), 0);
          return 'hsl(' + (date.getTime() - minDate.getTime()) / (maxDate.getTime() - minDate.getTime()) + 'turn, 100%, 50%)';
        };
      }

      _createClass(OlMapDecoratorService, [{
        key: "initMap",
        value: function initMap(config, geoData) {
          var _this = this;

          this.source = new openlayers__WEBPACK_IMPORTED_MODULE_2__["source"].OSM();
          this.popup = document.getElementById(config.popup);
          this.overlay = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Overlay"]({
            element: this.popup
          });
          var entries = {};
          var features = [];

          for (var _i = 0, _Object$entries = Object.entries(geoData.events); _i < _Object$entries.length; _i++) {
            var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
                eventId = _Object$entries$_i[0],
                event = _Object$entries$_i[1];

            var feature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].Point(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([event.point.lng, event.point.lat])));
            var id = 'event_' + eventId;
            feature.setId(id);
            features.push(feature);
            this.events[id] = event;
          }

          for (var _i2 = 0, _Object$entries2 = Object.entries(geoData.tours); _i2 < _Object$entries2.length; _i2++) {
            var _Object$entries2$_i = _slicedToArray(_Object$entries2[_i2], 2),
                counter = _Object$entries2$_i[0],
                tour = _Object$entries2$_i[1];

            var tourId = tour.id;
            var tourFromFeature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].Point(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tour.from.lng, tour.from.lat])));
            tourFromFeature.setId('tour_from_' + tourId);
            features.push(tourFromFeature);
            this.points['tour_from_' + tourId] = tour.from;
            var tourToFeature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].Point(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tour.from.lng, tour.from.lat])));
            tourToFeature.setId('tour_to_' + tourId);
            features.push(tourToFeature);
            this.points['tour_to_' + tourId] = tour.to;

            for (var _i3 = 0, _Object$entries3 = Object.entries(tour.segments); _i3 < _Object$entries3.length; _i3++) {
              var _Object$entries3$_i = _slicedToArray(_Object$entries3[_i3], 2),
                  segmentCounter = _Object$entries3$_i[0],
                  tourSegment = _Object$entries3$_i[1];

              var segmentId = tourSegment.id;
              tourSegment.tour = tour;

              var _feature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].LineString([openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tourSegment.from.lng, tourSegment.from.lat]), openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tourSegment.to.lng, tourSegment.to.lat])]));

              _feature.setId('segment_' + segmentId);

              features.push(_feature);
              this.tourSegments['segment_' + segmentId] = tourSegment;
              var fromFeature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].Point(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
              fromFeature.setId('segment_from_' + segmentId);
              features.push(fromFeature);
              this.points['segment_from_' + segmentId] = tourSegment.from;
              var toFeature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].Point(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
              toFeature.setId('segment_to_' + segmentId);
              features.push(toFeature);
              this.points['segment_to_' + segmentId] = tourSegment.to;
            }
          }

          var layers = [new openlayers__WEBPACK_IMPORTED_MODULE_2__["layer"].Tile({
            source: this.source
          }), new openlayers__WEBPACK_IMPORTED_MODULE_2__["layer"].Vector({
            source: new openlayers__WEBPACK_IMPORTED_MODULE_2__["source"].Vector({
              features: features
            }),
            zIndex: Infinity,
            style: function style(feature) {
              var id = feature.getId();

              if (_this.events[id]) {
                var entry = _this.events[id];
                return new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Style({
                  image: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Circle({
                    radius: 6,
                    fill: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Fill({
                      color: [225, 0, 255]
                    })
                  })
                });
              }

              if (_this.points[id]) {
                var _id$split = id.split(/\_/),
                    _id$split2 = _slicedToArray(_id$split, 3),
                    domain = _id$split2[0],
                    loc = _id$split2[1],
                    entryId = _id$split2[2];

                if (loc === 'from' || loc === 'to') {
                  if (domain === 'segment') {
                    return new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Style({
                      image: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Circle({
                        radius: 7,
                        fill: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Fill({
                          color: [0, 0, 255]
                        })
                      })
                    });
                  } else if (domain === 'tour') {
                    return new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Style({
                      image: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Circle({
                        radius: 5,
                        fill: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Fill({
                          color: [0, 255, 255, 0.6]
                        })
                      })
                    });
                  }
                }
              }

              return new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Style({
                stroke: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Stroke({
                  width: 3,
                  color: [255, 0, 0, 1]
                }),
                fill: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Fill({
                  color: [0, 0, 255, 0.6]
                })
              });
            }
          })];
          this.map = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Map"]({
            controls: openlayers__WEBPACK_IMPORTED_MODULE_2__["control"].defaults({
              rotate: false
            }).extend([new openlayers__WEBPACK_IMPORTED_MODULE_2__["control"].FullScreen(), new openlayers__WEBPACK_IMPORTED_MODULE_2__["control"].OverviewMap({
              layers: [new openlayers__WEBPACK_IMPORTED_MODULE_2__["layer"].Tile({
                source: this.source
              })]
            }), new openlayers__WEBPACK_IMPORTED_MODULE_2__["control"].ScaleLine({
              minWidth: 120
            })]),
            interactions: openlayers__WEBPACK_IMPORTED_MODULE_2__["interaction"].defaults({
              altShiftDragRotate: false,
              pinchRotate: false
            }),
            layers: layers,
            overlays: [this.overlay],
            target: config.map,
            view: new openlayers__WEBPACK_IMPORTED_MODULE_2__["View"]({
              center: openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([config.longitude, config.latitude]),
              zoom: config.zoom
            })
          }); // todo: move them back to component, where it belongs to.

          this.map.on('pointermove', function (event) {
            var pixel = _this.map.getEventPixel(event.originalEvent);

            var hit = _this.map.hasFeatureAtPixel(pixel, {
              hitTolerance: 10
            });

            document.getElementById(_this.map.getTarget()).style.cursor = hit ? 'pointer' : '';
          });
        }
      }, {
        key: "onSingleClick",
        value: function onSingleClick(fn) {
          var _this2 = this;

          this.map.on('singleclick', function (event) {
            var usedEvents = [];
            var usedTourSegments = [];

            _this2.map.forEachFeatureAtPixel(event.pixel, function (feature, layer) {
              var id = feature.getId();

              if (id === undefined) {
                return;
              }

              if (_this2.events[id]) {
                usedEvents.push(_this2.events[id]);
                return;
              } else if (_this2.points[id]) {
                var _id$split3 = id.split(/\_/),
                    _id$split4 = _slicedToArray(_id$split3, 3),
                    domain = _id$split4[0],
                    loc = _id$split4[1],
                    entryId = _id$split4[2];

                if (_this2.tourSegments[domain + '_' + entryId]) {
                  usedTourSegments.push(_this2.tourSegments[domain + '_' + entryId]);
                  return;
                }
              }
            }, {
              hitTolerance: 10
            });

            usedTourSegments.sort(function (x, y) {
              return x - y;
            });
            usedEvents.sort(function (x, y) {
              return x - y;
            });
            var lng = 0;
            var lat = 0;
            var length = 0;
            usedEvents.forEach(function (tourEvent) {
              lng += tourEvent.point.lng;
              lat += tourEvent.point.lat;
            });
            length += usedEvents.length; // usedTourSegments.forEach(segment => {
            //   lng += segment.to.lng - segment.from.lng;
            //   lat += segment.to.lat - segment.from.lat;
            // });
            // length += usedTourSegments.length;

            lng /= length;
            lat /= length;

            if (usedEvents.length || usedTourSegments.length) {
              _this2.overlay.setPosition(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([lng, lat]));
            } else {
              _this2.overlay.setPosition(undefined);
            }

            fn({
              events: usedEvents,
              segments: usedTourSegments
            });
          });
        }
      }]);

      return OlMapDecoratorService;
    }();

    OlMapDecoratorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], OlMapDecoratorService);
    /***/
  },

  /***/
  "../tour-commons/src/lib/tour-commons.component.ts":
  /*!*********************************************************!*\
    !*** ../tour-commons/src/lib/tour-commons.component.ts ***!
    \*********************************************************/

  /*! exports provided: TourCommonsComponent */

  /***/
  function tourCommonsSrcLibTourCommonsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TourCommonsComponent", function () {
      return TourCommonsComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/fesm2015/core.js");

    var TourCommonsComponent = function TourCommonsComponent() {
      _classCallCheck(this, TourCommonsComponent);
    };

    TourCommonsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'tour-commons',
      template: "\n        <h1>Tour CommonsComponent</h1>\n    "
    })], TourCommonsComponent);
    /***/
  },

  /***/
  "../tour-commons/src/lib/tour-commons.module.ts":
  /*!******************************************************!*\
    !*** ../tour-commons/src/lib/tour-commons.module.ts ***!
    \******************************************************/

  /*! exports provided: TourCommonsModule */

  /***/
  function tourCommonsSrcLibTourCommonsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TourCommonsModule", function () {
      return TourCommonsModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _tour_commons_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./tour-commons.component */
    "../tour-commons/src/lib/tour-commons.component.ts");

    var TourCommonsModule = function TourCommonsModule() {
      _classCallCheck(this, TourCommonsModule);
    };

    TourCommonsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_tour_commons_component__WEBPACK_IMPORTED_MODULE_2__["TourCommonsComponent"]],
      imports: [],
      exports: [_tour_commons_component__WEBPACK_IMPORTED_MODULE_2__["TourCommonsComponent"]]
    })], TourCommonsModule);
    /***/
  },

  /***/
  "../tour-commons/src/public-api.ts":
  /*!*****************************************!*\
    !*** ../tour-commons/src/public-api.ts ***!
    \*****************************************/

  /*! exports provided: TourCommonsModule, services, TourOrganizerType, TourSegmentModel, OlMapDecoratorService */

  /***/
  function tourCommonsSrcPublicApiTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _lib_tour_commons_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./lib/tour-commons.module */
    "../tour-commons/src/lib/tour-commons.module.ts");
    /* harmony reexport (safe) */


    __webpack_require__.d(__webpack_exports__, "TourCommonsModule", function () {
      return _lib_tour_commons_module__WEBPACK_IMPORTED_MODULE_1__["TourCommonsModule"];
    });
    /* harmony import */


    var _lib_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./lib/model */
    "../tour-commons/src/lib/model/index.ts");
    /* harmony reexport (safe) */


    __webpack_require__.d(__webpack_exports__, "TourOrganizerType", function () {
      return _lib_model__WEBPACK_IMPORTED_MODULE_2__["TourOrganizerType"];
    });
    /* harmony reexport (safe) */


    __webpack_require__.d(__webpack_exports__, "TourSegmentModel", function () {
      return _lib_model__WEBPACK_IMPORTED_MODULE_2__["TourSegmentModel"];
    });
    /* harmony import */


    var _lib_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./lib/services */
    "../tour-commons/src/lib/services/index.ts");
    /* harmony reexport (safe) */


    __webpack_require__.d(__webpack_exports__, "services", function () {
      return _lib_services__WEBPACK_IMPORTED_MODULE_3__["services"];
    });
    /* harmony reexport (safe) */


    __webpack_require__.d(__webpack_exports__, "OlMapDecoratorService", function () {
      return _lib_services__WEBPACK_IMPORTED_MODULE_3__["OlMapDecoratorService"];
    });
    /*
     * Public API Surface of tour-commons
     */

    /***/

  },

  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "../../node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./components */
    "./src/app/components/index.ts");
    /* harmony import */


    var _components_app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./components/app.component */
    "./src/app/components/app.component.ts");
    /* harmony import */


    var projects_tour_commons_src_public_api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! projects/tour-commons/src/public-api */
    "../tour-commons/src/public-api.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common/http */
    "../../node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/forms */
    "../../node_modules/@angular/forms/fesm2015/forms.js");

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
      declarations: _toConsumableArray(_components__WEBPACK_IMPORTED_MODULE_3__["components"]),
      imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], projects_tour_commons_src_public_api__WEBPACK_IMPORTED_MODULE_5__["TourCommonsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"]],
      bootstrap: [_components_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })], AppModule);
    /***/
  },

  /***/
  "./src/app/components/app.component.scss":
  /*!***********************************************!*\
    !*** ./src/app/components/app.component.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsAppComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9qZWN0cy90b3VyLXNpZ24tZm9ybS9zcmMvYXBwL2NvbXBvbmVudHMvYXBwLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/components/app.component.ts":
  /*!*********************************************!*\
    !*** ./src/app/components/app.component.ts ***!
    \*********************************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppComponentsAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/fesm2015/core.js");

    var AppComponent = function AppComponent() {
      _classCallCheck(this, AppComponent);

      this.title = 'tour-home-map';
    };

    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-root',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./app.component.html */
      "../../node_modules/raw-loader/dist/cjs.js!./src/app/components/app.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./app.component.scss */
      "./src/app/components/app.component.scss")).default]
    })], AppComponent);
    /***/
  },

  /***/
  "./src/app/components/index.ts":
  /*!*************************************!*\
    !*** ./src/app/components/index.ts ***!
    \*************************************/

  /*! exports provided: components, AppComponent, SignInFormComponent */

  /***/
  function srcAppComponentsIndexTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "components", function () {
      return components;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/components/app.component.ts");
    /* harmony import */


    var _sign_in_form_sign_in_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./sign-in-form/sign-in-form.component */
    "./src/app/components/sign-in-form/sign-in-form.component.ts");
    /* harmony reexport (safe) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return _app_component__WEBPACK_IMPORTED_MODULE_1__["AppComponent"];
    });
    /* harmony reexport (safe) */


    __webpack_require__.d(__webpack_exports__, "SignInFormComponent", function () {
      return _sign_in_form_sign_in_form_component__WEBPACK_IMPORTED_MODULE_2__["SignInFormComponent"];
    });

    var components = [_app_component__WEBPACK_IMPORTED_MODULE_1__["AppComponent"], _sign_in_form_sign_in_form_component__WEBPACK_IMPORTED_MODULE_2__["SignInFormComponent"]];
    /***/
  },

  /***/
  "./src/app/components/sign-in-form/sign-in-form.component.scss":
  /*!*********************************************************************!*\
    !*** ./src/app/components/sign-in-form/sign-in-form.component.scss ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsSignInFormSignInFormComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9qZWN0cy90b3VyLXNpZ24tZm9ybS9zcmMvYXBwL2NvbXBvbmVudHMvc2lnbi1pbi1mb3JtL3NpZ24taW4tZm9ybS5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/components/sign-in-form/sign-in-form.component.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/components/sign-in-form/sign-in-form.component.ts ***!
    \*******************************************************************/

  /*! exports provided: SignInFormComponent */

  /***/
  function srcAppComponentsSignInFormSignInFormComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SignInFormComponent", function () {
      return SignInFormComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "../../node_modules/@angular/forms/fesm2015/forms.js");

    var SignInFormComponent =
    /*#__PURE__*/
    function () {
      function SignInFormComponent(formBuilder) {
        _classCallCheck(this, SignInFormComponent);

        this.formBuilder = formBuilder;
        this.newData = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.signInForm = this.formBuilder.group({
          name: ''
        });
      }

      _createClass(SignInFormComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "onSubmit",
        value: function onSubmit(signInData) {
          this.signInForm.reset();
        }
      }]);

      return SignInFormComponent;
    }();

    SignInFormComponent.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], SignInFormComponent.prototype, "newData", void 0);
    SignInFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sign-in-form',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sign-in-form.component.html */
      "../../node_modules/raw-loader/dist/cjs.js!./src/app/components/sign-in-form/sign-in-form.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sign-in-form.component.scss */
      "./src/app/components/sign-in-form/sign-in-form.component.scss")).default]
    })], SignInFormComponent);
    /***/
  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js"); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false,
      apiUrl: 'http://localhost:3031'
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "../../node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "../../node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser-dynamic */
    "../../node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
    }

    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"]).catch(function (err) {
      return console.error(err);
    });
    /***/
  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! /home/maximilian/OpenSource/DevelopersForFuture/tour-de-rebel-map/projects/tour-sign-form/src/main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map