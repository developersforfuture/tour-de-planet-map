import { Component, NgModule, Injectable, ɵɵdefineInjectable } from '@angular/core';
import { __values, __read } from 'tslib';
import { source, Overlay, Feature, geom, proj, layer, style, Map, control, interaction, View } from 'openlayers';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/tour-commons.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TourCommonsComponent = /** @class */ (function () {
    function TourCommonsComponent() {
    }
    TourCommonsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'tour-commons',
                    template: "\n        <h1>Tour CommonsComponent</h1>\n    "
                }] }
    ];
    return TourCommonsComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/tour-commons.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TourCommonsModule = /** @class */ (function () {
    function TourCommonsModule() {
    }
    TourCommonsModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [TourCommonsComponent],
                    imports: [],
                    exports: [TourCommonsComponent]
                },] }
    ];
    return TourCommonsModule;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_event.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function TourEvent() { }
if (false) {
    /** @type {?} */
    TourEvent.prototype.point;
    /** @type {?} */
    TourEvent.prototype.author;
    /** @type {?} */
    TourEvent.prototype.title;
    /** @type {?} */
    TourEvent.prototype.url;
    /** @type {?} */
    TourEvent.prototype.date_to;
    /** @type {?} */
    TourEvent.prototype.date_from;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_point.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function TourPoint() { }
if (false) {
    /** @type {?} */
    TourPoint.prototype.lat;
    /** @type {?} */
    TourPoint.prototype.lng;
    /** @type {?|undefined} */
    TourPoint.prototype.name;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/geo_data.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function GeoData() { }
if (false) {
    /** @type {?} */
    GeoData.prototype.events;
    /** @type {?} */
    GeoData.prototype.tours;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function TourModel() { }
if (false) {
    /** @type {?} */
    TourModel.prototype.title;
    /** @type {?} */
    TourModel.prototype.organizer;
    /** @type {?} */
    TourModel.prototype.segments;
    /** @type {?} */
    TourModel.prototype.from;
    /** @type {?} */
    TourModel.prototype.to;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_organizer.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var TourOrganizerType = {
    SINGLE: "single",
    GROUP: "group",
};
/**
 * @record
 */
function TourOrganizerModel() { }
if (false) {
    /** @type {?} */
    TourOrganizerModel.prototype.name;
    /** @type {?} */
    TourOrganizerModel.prototype.type;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_segment.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TourSegmentModel = /** @class */ (function () {
    function TourSegmentModel() {
    }
    return TourSegmentModel;
}());
if (false) {
    /** @type {?} */
    TourSegmentModel.prototype.title;
    /** @type {?} */
    TourSegmentModel.prototype.from;
    /** @type {?} */
    TourSegmentModel.prototype.to;
    /** @type {?} */
    TourSegmentModel.prototype.tour;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/index.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lib/services/ol-map-decorator.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var OlMapDecoratorService = /** @class */ (function () {
    function OlMapDecoratorService() {
        this.events = {};
        this.tourSegments = {};
        this.points = {};
        this.dateToColor = (/**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var minDate = new Date(date.getFullYear(), date.getMonth() - 1, 0);
            /** @type {?} */
            var maxDate = new Date(date.getFullYear(), date.getMonth(), 0);
            return 'hsl(' + ((date.getTime() - minDate.getTime()) / (maxDate.getTime() - minDate.getTime())) + 'turn, 100%, 50%)';
        });
    }
    /**
     * @param {?} config
     * @param {?} geoData
     * @return {?}
     */
    OlMapDecoratorService.prototype.initMap = /**
     * @param {?} config
     * @param {?} geoData
     * @return {?}
     */
    function (config, geoData) {
        var e_1, _a, e_2, _b, e_3, _c;
        var _this = this;
        this.source = new source.OSM();
        this.popup = document.getElementById(config.popup);
        this.overlay = new Overlay({ element: this.popup });
        /** @type {?} */
        var entries = {};
        /** @type {?} */
        var features = [];
        try {
            for (var _d = __values(Object.entries(geoData.events)), _e = _d.next(); !_e.done; _e = _d.next()) {
                var _f = __read(_e.value, 2), eventId = _f[0], event_1 = _f[1];
                /** @type {?} */
                var feature = new Feature(new geom.Point(proj.fromLonLat([event_1.point.lng, event_1.point.lat])));
                /** @type {?} */
                var id = 'event_' + eventId;
                feature.setId(id);
                features.push(feature);
                this.events[id] = event_1;
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_e && !_e.done && (_a = _d.return)) _a.call(_d);
            }
            finally { if (e_1) throw e_1.error; }
        }
        try {
            for (var _g = __values(Object.entries(geoData.tours)), _h = _g.next(); !_h.done; _h = _g.next()) {
                var _j = __read(_h.value, 2), tourId = _j[0], tour = _j[1];
                /** @type {?} */
                var tourFromFeature = new Feature(new geom.Point(proj.fromLonLat([tour.from.lng, tour.from.lat])));
                tourFromFeature.setId('tour_from_' + tourId);
                features.push(tourFromFeature);
                this.points['tour_from_' + tourId] = tour.from;
                /** @type {?} */
                var tourToFeature = new Feature(new geom.Point(proj.fromLonLat([tour.from.lng, tour.from.lat])));
                tourToFeature.setId('tour_to_' + tourId);
                features.push(tourToFeature);
                this.points['tour_to_' + tourId] = tour.to;
                try {
                    for (var _k = (e_3 = void 0, __values(Object.entries(tour.segments))), _l = _k.next(); !_l.done; _l = _k.next()) {
                        var _m = __read(_l.value, 2), segmentId = _m[0], tourSegment = _m[1];
                        tourSegment.tour = tour;
                        /** @type {?} */
                        var feature = new Feature(new geom.LineString([
                            proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat]),
                            proj.fromLonLat([tourSegment.to.lng, tourSegment.to.lat])
                        ]));
                        feature.setId('segment_' + segmentId);
                        features.push(feature);
                        this.tourSegments['segment_' + segmentId] = tourSegment;
                        /** @type {?} */
                        var fromFeature = new Feature(new geom.Point(proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                        fromFeature.setId('segment_from_' + segmentId);
                        features.push(fromFeature);
                        this.points['segment_from_' + segmentId] = tourSegment.from;
                        /** @type {?} */
                        var toFeature = new Feature(new geom.Point(proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                        toFeature.setId('segment_to_' + segmentId);
                        features.push(toFeature);
                        this.points['segment_to_' + segmentId] = tourSegment.to;
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (_l && !_l.done && (_c = _k.return)) _c.call(_k);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (_h && !_h.done && (_b = _g.return)) _b.call(_g);
            }
            finally { if (e_2) throw e_2.error; }
        }
        /** @type {?} */
        var layers = [
            new layer.Tile({ source: this.source }),
            new layer.Vector({
                source: new source.Vector({ features: features }),
                zIndex: Infinity,
                style: (/**
                 * @param {?} feature
                 * @return {?}
                 */
                function (feature) {
                    /** @type {?} */
                    var id = feature.getId();
                    if (_this.events[id]) {
                        /** @type {?} */
                        var entry = _this.events[id];
                        /** @type {?} */
                        var date = new Date(entry.date_from * 1000);
                        return new style.Style({
                            image: new style.Circle({
                                radius: 6,
                                fill: new style.Fill({ color: [225, 0, 255,] })
                            })
                        });
                    }
                    if (_this.points[id]) {
                        var _a = __read(id.split(/\_/), 3), domain = _a[0], loc = _a[1], entryId = _a[2];
                        if (loc === 'from' || loc === 'to') {
                            if (domain === 'segment') {
                                return new style.Style({
                                    image: new style.Circle({
                                        radius: 7,
                                        fill: new style.Fill({ color: [0, 0, 255,] })
                                    })
                                });
                            }
                            else if (domain === 'tour') {
                                return new style.Style({
                                    image: new style.Circle({
                                        radius: 5,
                                        fill: new style.Fill({ color: [0, 255, 255, 0.6] })
                                    })
                                });
                            }
                        }
                    }
                    return new style.Style({
                        stroke: new style.Stroke({
                            width: 3,
                            color: [255, 0, 0, 1]
                        }),
                        fill: new style.Fill({
                            color: [0, 0, 255, 0.6]
                        })
                    });
                }),
            })
        ];
        this.map = new Map({
            controls: control.defaults({ rotate: false }).extend([
                new control.FullScreen(),
                new control.OverviewMap({
                    layers: [
                        new layer.Tile({ source: this.source })
                    ]
                }),
                new control.ScaleLine({ minWidth: 120 })
            ]),
            interactions: interaction.defaults({
                altShiftDragRotate: false,
                pinchRotate: false
            }),
            layers: layers,
            overlays: [this.overlay],
            target: config.map,
            view: new View({
                center: proj.fromLonLat([config.longitude, config.latitude]),
                zoom: config.zoom
            })
        });
        // todo: move them back to component, where it belongs to.
        this.map.on('pointermove', (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            /** @type {?} */
            var pixel = _this.map.getEventPixel(event.originalEvent);
            /** @type {?} */
            var hit = _this.map.hasFeatureAtPixel(pixel, { hitTolerance: 10 });
            document.getElementById(_this.map.getTarget()).style.cursor = hit ? 'pointer' : '';
        }));
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    OlMapDecoratorService.prototype.onSingleClick = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        var _this = this;
        this.map.on('singleclick', (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            /** @type {?} */
            var usedEvents = [];
            /** @type {?} */
            var usedTourSegments = [];
            _this.map.forEachFeatureAtPixel(event.pixel, (/**
             * @param {?} feature
             * @param {?} layer
             * @return {?}
             */
            function (feature, layer) {
                /** @type {?} */
                var id = feature.getId();
                if (id === undefined) {
                    return;
                }
                if (_this.events[id]) {
                    usedEvents.push(_this.events[id]);
                    return;
                }
                else if (_this.points[id]) {
                    var _a = __read(id.split(/\_/), 3), domain = _a[0], loc = _a[1], entryId = _a[2];
                    if (_this.tourSegments[domain + '_' + entryId]) {
                        usedTourSegments.push(_this.tourSegments[domain + '_' + entryId]);
                        return;
                    }
                }
            }), { hitTolerance: 10 });
            usedTourSegments.sort((/**
             * @param {?} x
             * @param {?} y
             * @return {?}
             */
            function (x, y) { return x - y; }));
            usedEvents.sort((/**
             * @param {?} x
             * @param {?} y
             * @return {?}
             */
            function (x, y) { return x - y; }));
            /** @type {?} */
            var lng = 0;
            /** @type {?} */
            var lat = 0;
            /** @type {?} */
            var length = 0;
            usedEvents.forEach((/**
             * @param {?} tourEvent
             * @return {?}
             */
            function (tourEvent) {
                lng += tourEvent.point.lng;
                lat += tourEvent.point.lat;
            }));
            length += usedEvents.length;
            // usedTourSegments.forEach(segment => {
            //   lng += segment.to.lng - segment.from.lng;
            //   lat += segment.to.lat - segment.from.lat;
            // });
            // length += usedTourSegments.length;
            lng /= length;
            lat /= length;
            if (usedEvents.length || usedTourSegments.length) {
                _this.overlay.setPosition(proj.fromLonLat([lng, lat]));
            }
            else {
                _this.overlay.setPosition(undefined);
            }
            fn({ events: usedEvents, segments: usedTourSegments });
        }));
    };
    OlMapDecoratorService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    OlMapDecoratorService.ctorParameters = function () { return []; };
    /** @nocollapse */ OlMapDecoratorService.ngInjectableDef = ɵɵdefineInjectable({ factory: function OlMapDecoratorService_Factory() { return new OlMapDecoratorService(); }, token: OlMapDecoratorService, providedIn: "root" });
    return OlMapDecoratorService;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.map;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.overlay;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.popup;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.source;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.events;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.tourSegments;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.points;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.dateToColor;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/services/index.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: tour-commons.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { OlMapDecoratorService, TourCommonsModule, TourOrganizerType, TourSegmentModel, TourCommonsComponent as ɵa };
//# sourceMappingURL=tour-commons.js.map
