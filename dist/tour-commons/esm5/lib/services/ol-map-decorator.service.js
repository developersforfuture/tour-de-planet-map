/**
 * @fileoverview added by tsickle
 * Generated from: lib/services/ol-map-decorator.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import * as ol from 'openlayers';
import * as i0 from "@angular/core";
var OlMapDecoratorService = /** @class */ (function () {
    function OlMapDecoratorService() {
        this.events = {};
        this.tourSegments = {};
        this.points = {};
        this.dateToColor = (/**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var minDate = new Date(date.getFullYear(), date.getMonth() - 1, 0);
            /** @type {?} */
            var maxDate = new Date(date.getFullYear(), date.getMonth(), 0);
            return 'hsl(' + ((date.getTime() - minDate.getTime()) / (maxDate.getTime() - minDate.getTime())) + 'turn, 100%, 50%)';
        });
    }
    /**
     * @param {?} config
     * @param {?} geoData
     * @return {?}
     */
    OlMapDecoratorService.prototype.initMap = /**
     * @param {?} config
     * @param {?} geoData
     * @return {?}
     */
    function (config, geoData) {
        var e_1, _a, e_2, _b, e_3, _c;
        var _this = this;
        this.source = new ol.source.OSM();
        this.popup = document.getElementById(config.popup);
        this.overlay = new ol.Overlay({ element: this.popup });
        /** @type {?} */
        var entries = {};
        /** @type {?} */
        var features = [];
        try {
            for (var _d = tslib_1.__values(Object.entries(geoData.events)), _e = _d.next(); !_e.done; _e = _d.next()) {
                var _f = tslib_1.__read(_e.value, 2), eventId = _f[0], event_1 = _f[1];
                /** @type {?} */
                var feature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([event_1.point.lng, event_1.point.lat])));
                /** @type {?} */
                var id = 'event_' + eventId;
                feature.setId(id);
                features.push(feature);
                this.events[id] = event_1;
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_e && !_e.done && (_a = _d.return)) _a.call(_d);
            }
            finally { if (e_1) throw e_1.error; }
        }
        try {
            for (var _g = tslib_1.__values(Object.entries(geoData.tours)), _h = _g.next(); !_h.done; _h = _g.next()) {
                var _j = tslib_1.__read(_h.value, 2), tourId = _j[0], tour = _j[1];
                /** @type {?} */
                var tourFromFeature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([tour.from.lng, tour.from.lat])));
                tourFromFeature.setId('tour_from_' + tourId);
                features.push(tourFromFeature);
                this.points['tour_from_' + tourId] = tour.from;
                /** @type {?} */
                var tourToFeature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([tour.from.lng, tour.from.lat])));
                tourToFeature.setId('tour_to_' + tourId);
                features.push(tourToFeature);
                this.points['tour_to_' + tourId] = tour.to;
                try {
                    for (var _k = (e_3 = void 0, tslib_1.__values(Object.entries(tour.segments))), _l = _k.next(); !_l.done; _l = _k.next()) {
                        var _m = tslib_1.__read(_l.value, 2), segmentId = _m[0], tourSegment = _m[1];
                        tourSegment.tour = tour;
                        /** @type {?} */
                        var feature = new ol.Feature(new ol.geom.LineString([
                            ol.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat]),
                            ol.proj.fromLonLat([tourSegment.to.lng, tourSegment.to.lat])
                        ]));
                        feature.setId('segment_' + segmentId);
                        features.push(feature);
                        this.tourSegments['segment_' + segmentId] = tourSegment;
                        /** @type {?} */
                        var fromFeature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                        fromFeature.setId('segment_from_' + segmentId);
                        features.push(fromFeature);
                        this.points['segment_from_' + segmentId] = tourSegment.from;
                        /** @type {?} */
                        var toFeature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                        toFeature.setId('segment_to_' + segmentId);
                        features.push(toFeature);
                        this.points['segment_to_' + segmentId] = tourSegment.to;
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (_l && !_l.done && (_c = _k.return)) _c.call(_k);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (_h && !_h.done && (_b = _g.return)) _b.call(_g);
            }
            finally { if (e_2) throw e_2.error; }
        }
        /** @type {?} */
        var layers = [
            new ol.layer.Tile({ source: this.source }),
            new ol.layer.Vector({
                source: new ol.source.Vector({ features: features }),
                zIndex: Infinity,
                style: (/**
                 * @param {?} feature
                 * @return {?}
                 */
                function (feature) {
                    /** @type {?} */
                    var id = feature.getId();
                    if (_this.events[id]) {
                        /** @type {?} */
                        var entry = _this.events[id];
                        /** @type {?} */
                        var date = new Date(entry.date_from * 1000);
                        return new ol.style.Style({
                            image: new ol.style.Circle({
                                radius: 6,
                                fill: new ol.style.Fill({ color: [225, 0, 255,] })
                            })
                        });
                    }
                    if (_this.points[id]) {
                        var _a = tslib_1.__read(id.split(/\_/), 3), domain = _a[0], loc = _a[1], entryId = _a[2];
                        if (loc === 'from' || loc === 'to') {
                            if (domain === 'segment') {
                                return new ol.style.Style({
                                    image: new ol.style.Circle({
                                        radius: 7,
                                        fill: new ol.style.Fill({ color: [0, 0, 255,] })
                                    })
                                });
                            }
                            else if (domain === 'tour') {
                                return new ol.style.Style({
                                    image: new ol.style.Circle({
                                        radius: 5,
                                        fill: new ol.style.Fill({ color: [0, 255, 255, 0.6] })
                                    })
                                });
                            }
                        }
                    }
                    return new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            width: 3,
                            color: [255, 0, 0, 1]
                        }),
                        fill: new ol.style.Fill({
                            color: [0, 0, 255, 0.6]
                        })
                    });
                }),
            })
        ];
        this.map = new ol.Map({
            controls: ol.control.defaults({ rotate: false }).extend([
                new ol.control.FullScreen(),
                new ol.control.OverviewMap({
                    layers: [
                        new ol.layer.Tile({ source: this.source })
                    ]
                }),
                new ol.control.ScaleLine({ minWidth: 120 })
            ]),
            interactions: ol.interaction.defaults({
                altShiftDragRotate: false,
                pinchRotate: false
            }),
            layers: layers,
            overlays: [this.overlay],
            target: config.map,
            view: new ol.View({
                center: ol.proj.fromLonLat([config.longitude, config.latitude]),
                zoom: config.zoom
            })
        });
        // todo: move them back to component, where it belongs to.
        this.map.on('pointermove', (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            /** @type {?} */
            var pixel = _this.map.getEventPixel(event.originalEvent);
            /** @type {?} */
            var hit = _this.map.hasFeatureAtPixel(pixel, { hitTolerance: 10 });
            document.getElementById(_this.map.getTarget()).style.cursor = hit ? 'pointer' : '';
        }));
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    OlMapDecoratorService.prototype.onSingleClick = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        var _this = this;
        this.map.on('singleclick', (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            /** @type {?} */
            var usedEvents = [];
            /** @type {?} */
            var usedTourSegments = [];
            _this.map.forEachFeatureAtPixel(event.pixel, (/**
             * @param {?} feature
             * @param {?} layer
             * @return {?}
             */
            function (feature, layer) {
                /** @type {?} */
                var id = feature.getId();
                if (id === undefined) {
                    return;
                }
                if (_this.events[id]) {
                    usedEvents.push(_this.events[id]);
                    return;
                }
                else if (_this.points[id]) {
                    var _a = tslib_1.__read(id.split(/\_/), 3), domain = _a[0], loc = _a[1], entryId = _a[2];
                    if (_this.tourSegments[domain + '_' + entryId]) {
                        usedTourSegments.push(_this.tourSegments[domain + '_' + entryId]);
                        return;
                    }
                }
            }), { hitTolerance: 10 });
            usedTourSegments.sort((/**
             * @param {?} x
             * @param {?} y
             * @return {?}
             */
            function (x, y) { return x - y; }));
            usedEvents.sort((/**
             * @param {?} x
             * @param {?} y
             * @return {?}
             */
            function (x, y) { return x - y; }));
            /** @type {?} */
            var lng = 0;
            /** @type {?} */
            var lat = 0;
            /** @type {?} */
            var length = 0;
            usedEvents.forEach((/**
             * @param {?} tourEvent
             * @return {?}
             */
            function (tourEvent) {
                lng += tourEvent.point.lng;
                lat += tourEvent.point.lat;
            }));
            length += usedEvents.length;
            // usedTourSegments.forEach(segment => {
            //   lng += segment.to.lng - segment.from.lng;
            //   lat += segment.to.lat - segment.from.lat;
            // });
            // length += usedTourSegments.length;
            lng /= length;
            lat /= length;
            if (usedEvents.length || usedTourSegments.length) {
                _this.overlay.setPosition(ol.proj.fromLonLat([lng, lat]));
            }
            else {
                _this.overlay.setPosition(undefined);
            }
            fn({ events: usedEvents, segments: usedTourSegments });
        }));
    };
    OlMapDecoratorService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    OlMapDecoratorService.ctorParameters = function () { return []; };
    /** @nocollapse */ OlMapDecoratorService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function OlMapDecoratorService_Factory() { return new OlMapDecoratorService(); }, token: OlMapDecoratorService, providedIn: "root" });
    return OlMapDecoratorService;
}());
export { OlMapDecoratorService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.map;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.overlay;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.popup;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.source;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.events;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.tourSegments;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.points;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.dateToColor;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2wtbWFwLWRlY29yYXRvci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdG91ci1jb21tb25zLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL29sLW1hcC1kZWNvcmF0b3Iuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sS0FBSyxFQUFFLE1BQU0sWUFBWSxDQUFDOztBQUlqQztJQVlFO1FBSlEsV0FBTSxHQUE4QixFQUFFLENBQUM7UUFDdkMsaUJBQVksR0FBcUMsRUFBRSxDQUFDO1FBQ3BELFdBQU0sR0FBOEIsRUFBRSxDQUFDO1FBME12QyxnQkFBVzs7OztRQUFJLFVBQUMsSUFBSTs7Z0JBQ3BCLE9BQU8sR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7O2dCQUM5RCxPQUFPLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDaEUsT0FBTyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsR0FBRyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsR0FBRyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxHQUFHLGtCQUFrQixDQUFDO1FBQ3hILENBQUMsRUFBQTtJQTVNZSxDQUFDOzs7Ozs7SUFHakIsdUNBQU87Ozs7O0lBQVAsVUFBUSxNQUFzQixFQUFFLE9BQWdCOztRQUFoRCxpQkE2SUM7UUE1SUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDbEMsSUFBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFDLENBQUMsQ0FBQzs7WUFBVSxPQUFPLEdBQUcsRUFBRTs7WUFDckUsUUFBUSxHQUFHLEVBQUU7O1lBRW5CLEtBQStCLElBQUEsS0FBQSxpQkFBQSxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQSxnQkFBQSw0QkFBRTtnQkFBcEQsSUFBQSxnQ0FBZ0IsRUFBZixlQUFPLEVBQUUsZUFBSzs7b0JBQ2xCLE9BQU8sR0FBRyxJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQzVCLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxPQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxPQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FDMUU7O29CQUNLLEVBQUUsR0FBRyxRQUFRLEdBQUcsT0FBTztnQkFDN0IsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFLLENBQUM7YUFDekI7Ozs7Ozs7Ozs7WUFFRCxLQUE2QixJQUFBLEtBQUEsaUJBQUEsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQWpELElBQUEsZ0NBQWMsRUFBYixjQUFNLEVBQUUsWUFBSTs7b0JBQ2hCLGVBQWUsR0FBRyxJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQ3BDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FDdEU7Z0JBQ0QsZUFBZSxDQUFDLEtBQUssQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLENBQUM7Z0JBQzdDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7O29CQUV6QyxhQUFhLEdBQUcsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUNsQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQ3RFO2dCQUNELGFBQWEsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxDQUFDO2dCQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDOztvQkFFM0MsS0FBdUMsSUFBQSxvQkFBQSxpQkFBQSxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQSxDQUFBLGdCQUFBLDRCQUFFO3dCQUEzRCxJQUFBLGdDQUF3QixFQUF2QixpQkFBUyxFQUFFLG1CQUFXO3dCQUNoQyxXQUFXLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzs7NEJBQ2xCLE9BQU8sR0FBRyxJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQzVCLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7NEJBQ3JCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzs0QkFDaEUsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3lCQUM3RCxDQUFDLENBQ0g7d0JBQ0QsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLENBQUM7d0JBQ3RDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxHQUFHLFdBQVcsQ0FBQzs7NEJBRWxELFdBQVcsR0FBRyxJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQ2hDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FDcEY7d0JBQ0QsV0FBVyxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDLENBQUM7d0JBQy9DLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7d0JBQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUM7OzRCQUV0RCxTQUFTLEdBQUcsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUM5QixJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQ3BGO3dCQUNELFNBQVMsQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQyxDQUFDO3dCQUMzQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUN6QixJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUMsR0FBRyxXQUFXLENBQUMsRUFBRSxDQUFDO3FCQUN6RDs7Ozs7Ozs7O2FBQ0Y7Ozs7Ozs7Ozs7WUFFSyxNQUFNLEdBQUc7WUFDYixJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUMsQ0FBQztZQUN4QyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO2dCQUNsQixNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFDLFFBQVEsVUFBQSxFQUFDLENBQUM7Z0JBQ3hDLE1BQU0sRUFBRSxRQUFRO2dCQUNoQixLQUFLOzs7O2dCQUFFLFVBQUMsT0FBTzs7d0JBQ1AsRUFBRSxHQUFHLE9BQU8sQ0FBQyxLQUFLLEVBQUU7b0JBQzFCLElBQUksS0FBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsRUFBRTs7NEJBQ2IsS0FBSyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDOzs0QkFDdkIsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO3dCQUU3QyxPQUFPLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7NEJBQ3hCLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO2dDQUN6QixNQUFNLEVBQUUsQ0FBQztnQ0FDVCxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFDLEtBQUssRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFHLEVBQUMsQ0FBQzs2QkFDbEQsQ0FBQzt5QkFDSCxDQUFDLENBQUM7cUJBQ0o7b0JBRUQsSUFBSSxLQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxFQUFFO3dCQUNiLElBQUEsc0NBQXVDLEVBQXRDLGNBQU0sRUFBRSxXQUFHLEVBQUUsZUFBeUI7d0JBQzdDLElBQUksR0FBRyxLQUFLLE1BQU0sSUFBSSxHQUFHLEtBQUssSUFBSSxFQUFFOzRCQUNsQyxJQUFJLE1BQU0sS0FBSyxTQUFTLEVBQUU7Z0NBQ3hCLE9BQU8sSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztvQ0FDeEIsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7d0NBQ3pCLE1BQU0sRUFBRSxDQUFDO3dDQUNULElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUcsRUFBQyxDQUFDO3FDQUNoRCxDQUFDO2lDQUNILENBQUMsQ0FBQzs2QkFDSjtpQ0FBTSxJQUFJLE1BQU0sS0FBSyxNQUFNLEVBQUU7Z0NBQzVCLE9BQU8sSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztvQ0FDeEIsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7d0NBQ3pCLE1BQU0sRUFBRSxDQUFDO3dDQUNULElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUMsQ0FBQztxQ0FDckQsQ0FBQztpQ0FDSCxDQUFDLENBQUM7NkJBQ0o7eUJBQ0Y7cUJBQ0Y7b0JBRUQsT0FBTyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO3dCQUN4QixNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzs0QkFDMUIsS0FBSyxFQUFFLENBQUM7NEJBQ1IsS0FBSyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO3lCQUN0QixDQUFDO3dCQUNGLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDOzRCQUN0QixLQUFLLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUM7eUJBQ3hCLENBQUM7cUJBQ0gsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQTthQUNGLENBQUM7U0FDSDtRQUVELElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDO1lBQ3BCLFFBQVEsRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztnQkFDcEQsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRTtnQkFDM0IsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztvQkFDekIsTUFBTSxFQUFFO3dCQUNOLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBQyxDQUFDO3FCQUN6QztpQkFDRixDQUFDO2dCQUNGLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBQyxRQUFRLEVBQUUsR0FBRyxFQUFDLENBQUM7YUFDMUMsQ0FBQztZQUNGLFlBQVksRUFBRSxFQUFFLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztnQkFDcEMsa0JBQWtCLEVBQUUsS0FBSztnQkFDekIsV0FBVyxFQUFFLEtBQUs7YUFDbkIsQ0FBQztZQUNGLE1BQU0sUUFBQTtZQUNOLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDeEIsTUFBTSxFQUFFLE1BQU0sQ0FBQyxHQUFHO1lBQ2xCLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLE1BQU0sRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUMvRCxJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUk7YUFDbEIsQ0FBQztTQUNILENBQUMsQ0FBQztRQUVILDBEQUEwRDtRQUMxRCxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxhQUFhOzs7O1FBQUUsVUFBQyxLQUFLOztnQkFDekIsS0FBSyxHQUFHLEtBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUM7O2dCQUNuRCxHQUFHLEdBQUcsS0FBSSxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsRUFBQyxZQUFZLEVBQUUsRUFBRSxFQUFDLENBQUM7WUFDakUsUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3BGLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCw2Q0FBYTs7OztJQUFiLFVBQWMsRUFBb0I7UUFBbEMsaUJBb0RDO1FBbkRDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLGFBQWE7Ozs7UUFBRSxVQUFDLEtBQUs7O2dCQUN6QixVQUFVLEdBQUcsRUFBRTs7Z0JBQ2YsZ0JBQWdCLEdBQUcsRUFBRTtZQUMzQixLQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxLQUFLOzs7OztZQUFFLFVBQUMsT0FBTyxFQUFFLEtBQUs7O29CQUNuRCxFQUFFLEdBQVcsT0FBTyxDQUFDLEtBQUssRUFBRTtnQkFDbEMsSUFBSSxFQUFFLEtBQUssU0FBUyxFQUFFO29CQUNwQixPQUFPO2lCQUNSO2dCQUVELElBQUksS0FBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsRUFBRTtvQkFDbkIsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLE9BQU87aUJBQ1I7cUJBQU0sSUFBSSxLQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxFQUFFO29CQUNwQixJQUFBLHNDQUF1QyxFQUF0QyxjQUFNLEVBQUUsV0FBRyxFQUFFLGVBQXlCO29CQUU3QyxJQUFJLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxPQUFPLENBQUMsRUFBRTt3QkFDN0MsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO3dCQUNqRSxPQUFPO3FCQUNSO2lCQUNGO1lBQ0gsQ0FBQyxHQUFFLEVBQUMsWUFBWSxFQUFFLEVBQUUsRUFBQyxDQUFDLENBQUM7WUFDdkIsZ0JBQWdCLENBQUMsSUFBSTs7Ozs7WUFBQyxVQUFDLENBQUMsRUFBRSxDQUFDLElBQUssT0FBQSxDQUFDLEdBQUcsQ0FBQyxFQUFMLENBQUssRUFBQyxDQUFDO1lBQ3ZDLFVBQVUsQ0FBQyxJQUFJOzs7OztZQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLENBQUMsR0FBRyxDQUFDLEVBQUwsQ0FBSyxFQUFDLENBQUM7O2dCQUU3QixHQUFHLEdBQUcsQ0FBQzs7Z0JBQ1AsR0FBRyxHQUFHLENBQUM7O2dCQUNQLE1BQU0sR0FBRyxDQUFDO1lBRWQsVUFBVSxDQUFDLE9BQU87Ozs7WUFBQyxVQUFBLFNBQVM7Z0JBQzFCLEdBQUcsSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztnQkFDM0IsR0FBRyxJQUFJLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1lBQzdCLENBQUMsRUFBQyxDQUFDO1lBRUgsTUFBTSxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUM7WUFDNUIsd0NBQXdDO1lBQ3hDLDhDQUE4QztZQUM5Qyw4Q0FBOEM7WUFDOUMsTUFBTTtZQUNOLHFDQUFxQztZQUVyQyxHQUFHLElBQUksTUFBTSxDQUFDO1lBQ2QsR0FBRyxJQUFJLE1BQU0sQ0FBQztZQUVkLElBQUksVUFBVSxDQUFDLE1BQU0sSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2hELEtBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUMxRDtpQkFBTTtnQkFDTCxLQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNyQztZQUVELEVBQUUsQ0FBQyxFQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFDLENBQUMsQ0FBQztRQUN2RCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7O2dCQWxORixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7OztnQ0FQRDtDQThOQyxBQXpORCxJQXlOQztTQXROWSxxQkFBcUI7Ozs7OztJQUNoQyxvQ0FBb0I7Ozs7O0lBQ3BCLHdDQUE0Qjs7Ozs7SUFDNUIsc0NBQTJCOzs7OztJQUMzQix1Q0FBOEI7Ozs7O0lBQzlCLHVDQUErQzs7Ozs7SUFDL0MsNkNBQTREOzs7OztJQUM1RCx1Q0FBK0M7Ozs7O0lBME0vQyw0Q0FJQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIG9sIGZyb20gJ29wZW5sYXllcnMnO1xuaW1wb3J0IHsgTWFwQ29uZmlnTW9kZWwgfSBmcm9tICcuLi9tb2RlbC9tYXBfY29uZmlnLm1vZGVsJztcbmltcG9ydCB7IEdlb0RhdGEsIFRvdXJQb2ludCwgVG91ckV2ZW50LCBUb3VyU2VnbWVudE1vZGVsfSBmcm9tICcuLi9tb2RlbCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE9sTWFwRGVjb3JhdG9yU2VydmljZSB7XG4gIHByaXZhdGUgbWFwOiBvbC5NYXA7XG4gIHByaXZhdGUgb3ZlcmxheTogb2wuT3ZlckxheTtcbiAgcHJpdmF0ZSBwb3B1cDogSFRNTEVsZW1lbnQ7XG4gIHByaXZhdGUgc291cmNlOiBvbC5zb3VyY2UuT1NNO1xuICBwcml2YXRlIGV2ZW50czoge1tpZDogc3RyaW5nXTogVG91ckV2ZW50fSA9IHt9O1xuICBwcml2YXRlIHRvdXJTZWdtZW50czoge1tpZDogc3RyaW5nXTogVG91clNlZ21lbnRNb2RlbH0gPSB7fTtcbiAgcHJpdmF0ZSBwb2ludHM6IHtbaWQ6IHN0cmluZ106IFRvdXJQb2ludH0gPSB7fTtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG5cbiAgaW5pdE1hcChjb25maWc6IE1hcENvbmZpZ01vZGVsLCBnZW9EYXRhOiBHZW9EYXRhKTogdm9pZCB7XG4gICAgdGhpcy5zb3VyY2UgPSBuZXcgb2wuc291cmNlLk9TTSgpO1xuICAgIHRoaXMucG9wdXAgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb25maWcucG9wdXApO1xuICAgIHRoaXMub3ZlcmxheSA9IG5ldyBvbC5PdmVybGF5KHtlbGVtZW50OiB0aGlzLnBvcHVwfSk7ICAgIGNvbnN0IGVudHJpZXMgPSB7fTtcbiAgICBjb25zdCBmZWF0dXJlcyA9IFtdO1xuXG4gICAgZm9yIChjb25zdCBbZXZlbnRJZCwgZXZlbnRdIG9mIE9iamVjdC5lbnRyaWVzKGdlb0RhdGEuZXZlbnRzKSkge1xuICAgICAgY29uc3QgZmVhdHVyZSA9IG5ldyBvbC5GZWF0dXJlKFxuICAgICAgICBuZXcgb2wuZ2VvbS5Qb2ludChvbC5wcm9qLmZyb21Mb25MYXQoW2V2ZW50LnBvaW50LmxuZywgZXZlbnQucG9pbnQubGF0XSkpXG4gICAgICApO1xuICAgICAgY29uc3QgaWQgPSAnZXZlbnRfJyArIGV2ZW50SWQ7XG4gICAgICBmZWF0dXJlLnNldElkKGlkKTtcbiAgICAgIGZlYXR1cmVzLnB1c2goZmVhdHVyZSk7XG4gICAgICB0aGlzLmV2ZW50c1tpZF0gPSBldmVudDtcbiAgICB9XG5cbiAgICBmb3IgKGNvbnN0IFt0b3VySWQsIHRvdXJdIG9mIE9iamVjdC5lbnRyaWVzKGdlb0RhdGEudG91cnMpKSB7XG4gICAgICBjb25zdCB0b3VyRnJvbUZlYXR1cmUgPSBuZXcgb2wuRmVhdHVyZShcbiAgICAgICAgbmV3IG9sLmdlb20uUG9pbnQob2wucHJvai5mcm9tTG9uTGF0KFt0b3VyLmZyb20ubG5nLCB0b3VyLmZyb20ubGF0XSkpXG4gICAgICApO1xuICAgICAgdG91ckZyb21GZWF0dXJlLnNldElkKCd0b3VyX2Zyb21fJyArIHRvdXJJZCk7XG4gICAgICBmZWF0dXJlcy5wdXNoKHRvdXJGcm9tRmVhdHVyZSk7XG4gICAgICB0aGlzLnBvaW50c1sndG91cl9mcm9tXycgKyB0b3VySWRdID0gdG91ci5mcm9tO1xuXG4gICAgICBjb25zdCB0b3VyVG9GZWF0dXJlID0gbmV3IG9sLkZlYXR1cmUoXG4gICAgICAgIG5ldyBvbC5nZW9tLlBvaW50KG9sLnByb2ouZnJvbUxvbkxhdChbdG91ci5mcm9tLmxuZywgdG91ci5mcm9tLmxhdF0pKVxuICAgICAgKTtcbiAgICAgIHRvdXJUb0ZlYXR1cmUuc2V0SWQoJ3RvdXJfdG9fJyArIHRvdXJJZCk7XG4gICAgICBmZWF0dXJlcy5wdXNoKHRvdXJUb0ZlYXR1cmUpO1xuICAgICAgdGhpcy5wb2ludHNbJ3RvdXJfdG9fJyArIHRvdXJJZF0gPSB0b3VyLnRvO1xuXG4gICAgICBmb3IgKGNvbnN0IFtzZWdtZW50SWQsIHRvdXJTZWdtZW50XSBvZiBPYmplY3QuZW50cmllcyh0b3VyLnNlZ21lbnRzKSkge1xuICAgICAgICB0b3VyU2VnbWVudC50b3VyID0gdG91cjtcbiAgICAgICAgY29uc3QgZmVhdHVyZSA9IG5ldyBvbC5GZWF0dXJlKFxuICAgICAgICAgIG5ldyBvbC5nZW9tLkxpbmVTdHJpbmcoW1xuICAgICAgICAgICAgb2wucHJvai5mcm9tTG9uTGF0KFt0b3VyU2VnbWVudC5mcm9tLmxuZywgdG91clNlZ21lbnQuZnJvbS5sYXRdKSxcbiAgICAgICAgICAgIG9sLnByb2ouZnJvbUxvbkxhdChbdG91clNlZ21lbnQudG8ubG5nLCB0b3VyU2VnbWVudC50by5sYXRdKVxuICAgICAgICAgIF0pXG4gICAgICAgICk7XG4gICAgICAgIGZlYXR1cmUuc2V0SWQoJ3NlZ21lbnRfJyArIHNlZ21lbnRJZCk7XG4gICAgICAgIGZlYXR1cmVzLnB1c2goZmVhdHVyZSk7XG4gICAgICAgIHRoaXMudG91clNlZ21lbnRzWydzZWdtZW50XycgKyBzZWdtZW50SWRdID0gdG91clNlZ21lbnQ7XG5cbiAgICAgICAgY29uc3QgZnJvbUZlYXR1cmUgPSBuZXcgb2wuRmVhdHVyZShcbiAgICAgICAgICBuZXcgb2wuZ2VvbS5Qb2ludChvbC5wcm9qLmZyb21Mb25MYXQoW3RvdXJTZWdtZW50LmZyb20ubG5nLCB0b3VyU2VnbWVudC5mcm9tLmxhdF0pKVxuICAgICAgICApO1xuICAgICAgICBmcm9tRmVhdHVyZS5zZXRJZCgnc2VnbWVudF9mcm9tXycgKyBzZWdtZW50SWQpO1xuICAgICAgICBmZWF0dXJlcy5wdXNoKGZyb21GZWF0dXJlKTtcbiAgICAgICAgdGhpcy5wb2ludHNbJ3NlZ21lbnRfZnJvbV8nICsgc2VnbWVudElkXSA9IHRvdXJTZWdtZW50LmZyb207XG5cbiAgICAgICAgY29uc3QgdG9GZWF0dXJlID0gbmV3IG9sLkZlYXR1cmUoXG4gICAgICAgICAgbmV3IG9sLmdlb20uUG9pbnQob2wucHJvai5mcm9tTG9uTGF0KFt0b3VyU2VnbWVudC5mcm9tLmxuZywgdG91clNlZ21lbnQuZnJvbS5sYXRdKSlcbiAgICAgICAgKTtcbiAgICAgICAgdG9GZWF0dXJlLnNldElkKCdzZWdtZW50X3RvXycgKyBzZWdtZW50SWQpO1xuICAgICAgICBmZWF0dXJlcy5wdXNoKHRvRmVhdHVyZSk7XG4gICAgICAgIHRoaXMucG9pbnRzWydzZWdtZW50X3RvXycgKyBzZWdtZW50SWRdID0gdG91clNlZ21lbnQudG87XG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgbGF5ZXJzID0gW1xuICAgICAgbmV3IG9sLmxheWVyLlRpbGUoe3NvdXJjZTogdGhpcy5zb3VyY2V9KSxcbiAgICAgIG5ldyBvbC5sYXllci5WZWN0b3Ioe1xuICAgICAgICBzb3VyY2U6IG5ldyBvbC5zb3VyY2UuVmVjdG9yKHtmZWF0dXJlc30pLFxuICAgICAgICB6SW5kZXg6IEluZmluaXR5LFxuICAgICAgICBzdHlsZTogKGZlYXR1cmUpID0+IHtcbiAgICAgICAgICBjb25zdCBpZCA9IGZlYXR1cmUuZ2V0SWQoKTtcbiAgICAgICAgICBpZiAodGhpcy5ldmVudHNbaWRdKSB7XG4gICAgICAgICAgICBjb25zdCBlbnRyeSA9IHRoaXMuZXZlbnRzW2lkXTtcbiAgICAgICAgICAgIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZShlbnRyeS5kYXRlX2Zyb20gKiAxMDAwKTtcblxuICAgICAgICAgICAgcmV0dXJuIG5ldyBvbC5zdHlsZS5TdHlsZSh7XG4gICAgICAgICAgICAgIGltYWdlOiBuZXcgb2wuc3R5bGUuQ2lyY2xlKHtcbiAgICAgICAgICAgICAgICByYWRpdXM6IDYsXG4gICAgICAgICAgICAgICAgZmlsbDogbmV3IG9sLnN0eWxlLkZpbGwoe2NvbG9yOiBbMjI1LCAwLCAyNTUsIF19KVxuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKHRoaXMucG9pbnRzW2lkXSkge1xuICAgICAgICAgICAgY29uc3QgW2RvbWFpbiwgbG9jLCBlbnRyeUlkXSA9IGlkLnNwbGl0KC9cXF8vKTtcbiAgICAgICAgICAgIGlmIChsb2MgPT09ICdmcm9tJyB8fCBsb2MgPT09ICd0bycpIHtcbiAgICAgICAgICAgICAgaWYgKGRvbWFpbiA9PT0gJ3NlZ21lbnQnKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBvbC5zdHlsZS5TdHlsZSh7XG4gICAgICAgICAgICAgICAgICBpbWFnZTogbmV3IG9sLnN0eWxlLkNpcmNsZSh7XG4gICAgICAgICAgICAgICAgICAgIHJhZGl1czogNyxcbiAgICAgICAgICAgICAgICAgICAgZmlsbDogbmV3IG9sLnN0eWxlLkZpbGwoe2NvbG9yOiBbMCwgMCwgMjU1LCBdfSlcbiAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoZG9tYWluID09PSAndG91cicpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IG9sLnN0eWxlLlN0eWxlKHtcbiAgICAgICAgICAgICAgICAgIGltYWdlOiBuZXcgb2wuc3R5bGUuQ2lyY2xlKHtcbiAgICAgICAgICAgICAgICAgICAgcmFkaXVzOiA1LFxuICAgICAgICAgICAgICAgICAgICBmaWxsOiBuZXcgb2wuc3R5bGUuRmlsbCh7Y29sb3I6IFswLCAyNTUsIDI1NSwgMC42XX0pXG4gICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcmV0dXJuIG5ldyBvbC5zdHlsZS5TdHlsZSh7XG4gICAgICAgICAgICBzdHJva2U6IG5ldyBvbC5zdHlsZS5TdHJva2Uoe1xuICAgICAgICAgICAgICB3aWR0aDogMyxcbiAgICAgICAgICAgICAgY29sb3I6IFsyNTUsIDAsIDAsIDFdXG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIGZpbGw6IG5ldyBvbC5zdHlsZS5GaWxsKHtcbiAgICAgICAgICAgICAgY29sb3I6IFswLCAwLCAyNTUsIDAuNl1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0sXG4gICAgICB9KVxuICAgIF07XG5cbiAgICB0aGlzLm1hcCA9IG5ldyBvbC5NYXAoe1xuICAgICAgY29udHJvbHM6IG9sLmNvbnRyb2wuZGVmYXVsdHMoe3JvdGF0ZTogZmFsc2V9KS5leHRlbmQoW1xuICAgICAgICBuZXcgb2wuY29udHJvbC5GdWxsU2NyZWVuKCksXG4gICAgICAgIG5ldyBvbC5jb250cm9sLk92ZXJ2aWV3TWFwKHtcbiAgICAgICAgICBsYXllcnM6IFtcbiAgICAgICAgICAgIG5ldyBvbC5sYXllci5UaWxlKHtzb3VyY2U6IHRoaXMuc291cmNlfSlcbiAgICAgICAgICBdXG4gICAgICAgIH0pLFxuICAgICAgICBuZXcgb2wuY29udHJvbC5TY2FsZUxpbmUoe21pbldpZHRoOiAxMjB9KVxuICAgICAgXSksXG4gICAgICBpbnRlcmFjdGlvbnM6IG9sLmludGVyYWN0aW9uLmRlZmF1bHRzKHtcbiAgICAgICAgYWx0U2hpZnREcmFnUm90YXRlOiBmYWxzZSxcbiAgICAgICAgcGluY2hSb3RhdGU6IGZhbHNlXG4gICAgICB9KSxcbiAgICAgIGxheWVycyxcbiAgICAgIG92ZXJsYXlzOiBbdGhpcy5vdmVybGF5XSxcbiAgICAgIHRhcmdldDogY29uZmlnLm1hcCxcbiAgICAgIHZpZXc6IG5ldyBvbC5WaWV3KHtcbiAgICAgICAgY2VudGVyOiBvbC5wcm9qLmZyb21Mb25MYXQoW2NvbmZpZy5sb25naXR1ZGUsIGNvbmZpZy5sYXRpdHVkZV0pLFxuICAgICAgICB6b29tOiBjb25maWcuem9vbVxuICAgICAgfSlcbiAgICB9KTtcblxuICAgIC8vIHRvZG86IG1vdmUgdGhlbSBiYWNrIHRvIGNvbXBvbmVudCwgd2hlcmUgaXQgYmVsb25ncyB0by5cbiAgICB0aGlzLm1hcC5vbigncG9pbnRlcm1vdmUnLCAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0IHBpeGVsID0gdGhpcy5tYXAuZ2V0RXZlbnRQaXhlbChldmVudC5vcmlnaW5hbEV2ZW50KTtcbiAgICAgIGNvbnN0IGhpdCA9IHRoaXMubWFwLmhhc0ZlYXR1cmVBdFBpeGVsKHBpeGVsLCB7aGl0VG9sZXJhbmNlOiAxMH0pO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQodGhpcy5tYXAuZ2V0VGFyZ2V0KCkpLnN0eWxlLmN1cnNvciA9IGhpdCA/ICdwb2ludGVyJyA6ICcnO1xuICAgIH0pO1xuICB9XG5cbiAgb25TaW5nbGVDbGljayhmbjogQ2FsbGFibGVGdW5jdGlvbikge1xuICAgIHRoaXMubWFwLm9uKCdzaW5nbGVjbGljaycsIChldmVudCkgPT4ge1xuICAgICAgY29uc3QgdXNlZEV2ZW50cyA9IFtdO1xuICAgICAgY29uc3QgdXNlZFRvdXJTZWdtZW50cyA9IFtdO1xuICAgICAgdGhpcy5tYXAuZm9yRWFjaEZlYXR1cmVBdFBpeGVsKGV2ZW50LnBpeGVsLCAoZmVhdHVyZSwgbGF5ZXIpID0+IHtcbiAgICAgICAgY29uc3QgaWQ6IHN0cmluZyA9IGZlYXR1cmUuZ2V0SWQoKTtcbiAgICAgICAgaWYgKGlkID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5ldmVudHNbaWRdKSB7XG4gICAgICAgICAgdXNlZEV2ZW50cy5wdXNoKHRoaXMuZXZlbnRzW2lkXSk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucG9pbnRzW2lkXSkge1xuICAgICAgICAgIGNvbnN0IFtkb21haW4sIGxvYywgZW50cnlJZF0gPSBpZC5zcGxpdCgvXFxfLyk7XG5cbiAgICAgICAgICBpZiAodGhpcy50b3VyU2VnbWVudHNbZG9tYWluICsgJ18nICsgZW50cnlJZF0pIHtcbiAgICAgICAgICAgIHVzZWRUb3VyU2VnbWVudHMucHVzaCh0aGlzLnRvdXJTZWdtZW50c1tkb21haW4gKyAnXycgKyBlbnRyeUlkXSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9LCB7aGl0VG9sZXJhbmNlOiAxMH0pO1xuICAgICAgdXNlZFRvdXJTZWdtZW50cy5zb3J0KCh4LCB5KSA9PiB4IC0geSk7XG4gICAgICB1c2VkRXZlbnRzLnNvcnQoKHgsIHkpID0+IHggLSB5KTtcblxuICAgICAgbGV0IGxuZyA9IDA7XG4gICAgICBsZXQgbGF0ID0gMDtcbiAgICAgIGxldCBsZW5ndGggPSAwO1xuXG4gICAgICB1c2VkRXZlbnRzLmZvckVhY2godG91ckV2ZW50ID0+IHtcbiAgICAgICAgbG5nICs9IHRvdXJFdmVudC5wb2ludC5sbmc7XG4gICAgICAgIGxhdCArPSB0b3VyRXZlbnQucG9pbnQubGF0O1xuICAgICAgfSk7XG5cbiAgICAgIGxlbmd0aCArPSB1c2VkRXZlbnRzLmxlbmd0aDtcbiAgICAgIC8vIHVzZWRUb3VyU2VnbWVudHMuZm9yRWFjaChzZWdtZW50ID0+IHtcbiAgICAgIC8vICAgbG5nICs9IHNlZ21lbnQudG8ubG5nIC0gc2VnbWVudC5mcm9tLmxuZztcbiAgICAgIC8vICAgbGF0ICs9IHNlZ21lbnQudG8ubGF0IC0gc2VnbWVudC5mcm9tLmxhdDtcbiAgICAgIC8vIH0pO1xuICAgICAgLy8gbGVuZ3RoICs9IHVzZWRUb3VyU2VnbWVudHMubGVuZ3RoO1xuXG4gICAgICBsbmcgLz0gbGVuZ3RoO1xuICAgICAgbGF0IC89IGxlbmd0aDtcblxuICAgICAgaWYgKHVzZWRFdmVudHMubGVuZ3RoIHx8IHVzZWRUb3VyU2VnbWVudHMubGVuZ3RoKSB7XG4gICAgICAgIHRoaXMub3ZlcmxheS5zZXRQb3NpdGlvbihvbC5wcm9qLmZyb21Mb25MYXQoW2xuZywgbGF0XSkpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5vdmVybGF5LnNldFBvc2l0aW9uKHVuZGVmaW5lZCk7XG4gICAgICB9XG5cbiAgICAgIGZuKHtldmVudHM6IHVzZWRFdmVudHMsIHNlZ21lbnRzOiB1c2VkVG91clNlZ21lbnRzfSk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGRhdGVUb0NvbG9yID0gIChkYXRlKSA9PiB7XG4gICAgY29uc3QgbWluRGF0ZSA9IG5ldyBEYXRlKGRhdGUuZ2V0RnVsbFllYXIoKSwgZGF0ZS5nZXRNb250aCgpIC0gMSwgMCk7XG4gICAgY29uc3QgbWF4RGF0ZSA9IG5ldyBEYXRlKGRhdGUuZ2V0RnVsbFllYXIoKSwgZGF0ZS5nZXRNb250aCgpLCAwKTtcbiAgICByZXR1cm4gJ2hzbCgnICsgKChkYXRlLmdldFRpbWUoKSAtIG1pbkRhdGUuZ2V0VGltZSgpKSAvIChtYXhEYXRlLmdldFRpbWUoKSAtIG1pbkRhdGUuZ2V0VGltZSgpKSkgKyAndHVybiwgMTAwJSwgNTAlKSc7XG4gIH1cbn1cbiJdfQ==