/**
 * @fileoverview added by tsickle
 * Generated from: lib/tour-commons.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var TourCommonsComponent = /** @class */ (function () {
    function TourCommonsComponent() {
    }
    TourCommonsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'tour-commons',
                    template: "\n        <h1>Tour CommonsComponent</h1>\n    "
                }] }
    ];
    return TourCommonsComponent;
}());
export { TourCommonsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG91ci1jb21tb25zLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3RvdXItY29tbW9ucy8iLCJzb3VyY2VzIjpbImxpYi90b3VyLWNvbW1vbnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUxQztJQUFBO0lBUUEsQ0FBQzs7Z0JBUkEsU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxjQUFjO29CQUN4QixRQUFRLEVBQUUsZ0RBRVQ7aUJBQ0o7O0lBR0QsMkJBQUM7Q0FBQSxBQVJELElBUUM7U0FGWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd0b3VyLWNvbW1vbnMnLFxuICAgIHRlbXBsYXRlOiBgXG4gICAgICAgIDxoMT5Ub3VyIENvbW1vbnNDb21wb25lbnQ8L2gxPlxuICAgIGBcbn0pXG5leHBvcnQgY2xhc3MgVG91ckNvbW1vbnNDb21wb25lbnQge1xuXG59XG4iXX0=