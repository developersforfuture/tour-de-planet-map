/**
 * @fileoverview added by tsickle
 * Generated from: lib/tour-commons.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { TourCommonsComponent } from './tour-commons.component';
var TourCommonsModule = /** @class */ (function () {
    function TourCommonsModule() {
    }
    TourCommonsModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [TourCommonsComponent],
                    imports: [],
                    exports: [TourCommonsComponent]
                },] }
    ];
    return TourCommonsModule;
}());
export { TourCommonsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG91ci1jb21tb25zLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3RvdXItY29tbW9ucy8iLCJzb3VyY2VzIjpbImxpYi90b3VyLWNvbW1vbnMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUVoRTtJQUFBO0lBTWlDLENBQUM7O2dCQU5qQyxRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsb0JBQW9CLENBQUM7b0JBQ3BDLE9BQU8sRUFBRSxFQUNSO29CQUNELE9BQU8sRUFBRSxDQUFDLG9CQUFvQixDQUFDO2lCQUNoQzs7SUFDZ0Msd0JBQUM7Q0FBQSxBQU5sQyxJQU1rQztTQUFyQixpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgVG91ckNvbW1vbnNDb21wb25lbnQgfSBmcm9tICcuL3RvdXItY29tbW9ucy5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtUb3VyQ29tbW9uc0NvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgXSxcbiAgZXhwb3J0czogW1RvdXJDb21tb25zQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBUb3VyQ29tbW9uc01vZHVsZSB7IH1cbiJdfQ==