/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_point.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function TourPoint() { }
if (false) {
    /** @type {?} */
    TourPoint.prototype.lat;
    /** @type {?} */
    TourPoint.prototype.lng;
    /** @type {?|undefined} */
    TourPoint.prototype.name;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG91cl9wb2ludC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3RvdXItY29tbW9ucy8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbC90b3VyX3BvaW50Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsK0JBSUM7OztJQUhHLHdCQUFZOztJQUNaLHdCQUFZOztJQUNaLHlCQUFjIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBUb3VyUG9pbnQge1xuICAgIGxhdDogbnVtYmVyO1xuICAgIGxuZzogbnVtYmVyO1xuICAgIG5hbWU/OiBzdHJpbmc7XG59XG4iXX0=