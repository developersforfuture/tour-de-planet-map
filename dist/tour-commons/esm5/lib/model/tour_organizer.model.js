/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_organizer.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var TourOrganizerType = {
    SINGLE: "single",
    GROUP: "group",
};
export { TourOrganizerType };
/**
 * @record
 */
export function TourOrganizerModel() { }
if (false) {
    /** @type {?} */
    TourOrganizerModel.prototype.name;
    /** @type {?} */
    TourOrganizerModel.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG91cl9vcmdhbml6ZXIubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly90b3VyLWNvbW1vbnMvIiwic291cmNlcyI6WyJsaWIvbW9kZWwvdG91cl9vcmdhbml6ZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsSUFBWSxpQkFBaUI7SUFDekIsTUFBTSxVQUFXO0lBQ2pCLEtBQUssU0FBVTtFQUNsQjs7Ozs7QUFDRCx3Q0FHQzs7O0lBRkcsa0NBQWE7O0lBQ2Isa0NBQXlEIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gVG91ck9yZ2FuaXplclR5cGUge1xuICAgIFNJTkdMRSA9ICdzaW5nbGUnLFxuICAgIEdST1VQID0gJ2dyb3VwJyxcbn1cbmV4cG9ydCBpbnRlcmZhY2UgVG91ck9yZ2FuaXplck1vZGVsIHtcbiAgICBuYW1lOiBzdHJpbmc7XG4gICAgdHlwZTogVG91ck9yZ2FuaXplclR5cGUuU0lOR0xFIHwgVG91ck9yZ2FuaXplclR5cGUuR1JPVVA7XG59XG4iXX0=