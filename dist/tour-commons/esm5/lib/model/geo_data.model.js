/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/geo_data.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function GeoData() { }
if (false) {
    /** @type {?} */
    GeoData.prototype.events;
    /** @type {?} */
    GeoData.prototype.tours;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2VvX2RhdGEubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly90b3VyLWNvbW1vbnMvIiwic291cmNlcyI6WyJsaWIvbW9kZWwvZ2VvX2RhdGEubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQSw2QkFHQzs7O0lBRkcseUJBQWtCOztJQUNsQix3QkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUb3VyRXZlbnQgfSBmcm9tICcuL3RvdXJfZXZlbnQubW9kZWwnO1xuaW1wb3J0IHsgVG91ck1vZGVsIH0gZnJvbSAnLi90b3VyLm1vZGVsJztcblxuZXhwb3J0IGludGVyZmFjZSBHZW9EYXRhIHtcbiAgICBldmVudHM6IFRvdXJFdmVudDtcbiAgICB0b3VyczogVG91ck1vZGVsW107XG59XG4iXX0=