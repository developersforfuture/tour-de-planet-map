/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function TourModel() { }
if (false) {
    /** @type {?} */
    TourModel.prototype.title;
    /** @type {?} */
    TourModel.prototype.organizer;
    /** @type {?} */
    TourModel.prototype.segments;
    /** @type {?} */
    TourModel.prototype.from;
    /** @type {?} */
    TourModel.prototype.to;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG91ci5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3RvdXItY29tbW9ucy8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbC90b3VyLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBSUEsK0JBTUM7OztJQUxHLDBCQUFjOztJQUNkLDhCQUE4Qjs7SUFDOUIsNkJBQTZCOztJQUM3Qix5QkFBZ0I7O0lBQ2hCLHVCQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVG91clNlZ21lbnRNb2RlbCB9IGZyb20gJy4vdG91cl9zZWdtZW50Lm1vZGVsJztcbmltcG9ydCB7IFRvdXJPcmdhbml6ZXJNb2RlbCB9IGZyb20gJy4vdG91cl9vcmdhbml6ZXIubW9kZWwnO1xuaW1wb3J0IHsgVG91clBvaW50IH0gZnJvbSAnLi90b3VyX3BvaW50Lm1vZGVsJztcblxuZXhwb3J0IGludGVyZmFjZSBUb3VyTW9kZWwge1xuICAgIHRpdGxlOiBzdHJpbmc7XG4gICAgb3JnYW5pemVyOiBUb3VyT3JnYW5pemVyTW9kZWw7XG4gICAgc2VnbWVudHM6IFRvdXJTZWdtZW50TW9kZWxbXTtcbiAgICBmcm9tOiBUb3VyUG9pbnQ7XG4gICAgdG86IFRvdXJQb2ludDtcbn1cbiJdfQ==