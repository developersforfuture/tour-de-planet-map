/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/map_config.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function MapConfigModel() { }
if (false) {
    /** @type {?} */
    MapConfigModel.prototype.latitude;
    /** @type {?} */
    MapConfigModel.prototype.longitude;
    /** @type {?} */
    MapConfigModel.prototype.zoom;
    /** @type {?} */
    MapConfigModel.prototype.popup;
    /** @type {?} */
    MapConfigModel.prototype.map;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwX2NvbmZpZy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3RvdXItY29tbW9ucy8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbC9tYXBfY29uZmlnLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsb0NBTUM7OztJQUxHLGtDQUFpQjs7SUFDakIsbUNBQWtCOztJQUNsQiw4QkFBYTs7SUFDYiwrQkFBYzs7SUFDZCw2QkFBWSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgTWFwQ29uZmlnTW9kZWwge1xuICAgIGxhdGl0dWRlOiBudW1iZXI7XG4gICAgbG9uZ2l0dWRlOiBudW1iZXI7XG4gICAgem9vbTogbnVtYmVyO1xuICAgIHBvcHVwOiBzdHJpbmc7XG4gICAgbWFwOiBzdHJpbmc7XG59XG4iXX0=