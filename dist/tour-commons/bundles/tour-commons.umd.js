(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('openlayers')) :
    typeof define === 'function' && define.amd ? define('tour-commons', ['exports', '@angular/core', 'openlayers'], factory) :
    (global = global || self, factory(global['tour-commons'] = {}, global.ng.core, global.openlayers));
}(this, (function (exports, core, openlayers) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/tour-commons.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TourCommonsComponent = /** @class */ (function () {
        function TourCommonsComponent() {
        }
        TourCommonsComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'tour-commons',
                        template: "\n        <h1>Tour CommonsComponent</h1>\n    "
                    }] }
        ];
        return TourCommonsComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/tour-commons.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TourCommonsModule = /** @class */ (function () {
        function TourCommonsModule() {
        }
        TourCommonsModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [TourCommonsComponent],
                        imports: [],
                        exports: [TourCommonsComponent]
                    },] }
        ];
        return TourCommonsModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/model/tour_event.model.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function TourEvent() { }
    if (false) {
        /** @type {?} */
        TourEvent.prototype.point;
        /** @type {?} */
        TourEvent.prototype.author;
        /** @type {?} */
        TourEvent.prototype.title;
        /** @type {?} */
        TourEvent.prototype.url;
        /** @type {?} */
        TourEvent.prototype.date_to;
        /** @type {?} */
        TourEvent.prototype.date_from;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/model/tour_point.model.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function TourPoint() { }
    if (false) {
        /** @type {?} */
        TourPoint.prototype.lat;
        /** @type {?} */
        TourPoint.prototype.lng;
        /** @type {?|undefined} */
        TourPoint.prototype.name;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/model/geo_data.model.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function GeoData() { }
    if (false) {
        /** @type {?} */
        GeoData.prototype.events;
        /** @type {?} */
        GeoData.prototype.tours;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/model/tour.model.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function TourModel() { }
    if (false) {
        /** @type {?} */
        TourModel.prototype.title;
        /** @type {?} */
        TourModel.prototype.organizer;
        /** @type {?} */
        TourModel.prototype.segments;
        /** @type {?} */
        TourModel.prototype.from;
        /** @type {?} */
        TourModel.prototype.to;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/model/tour_organizer.model.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var TourOrganizerType = {
        SINGLE: "single",
        GROUP: "group",
    };
    /**
     * @record
     */
    function TourOrganizerModel() { }
    if (false) {
        /** @type {?} */
        TourOrganizerModel.prototype.name;
        /** @type {?} */
        TourOrganizerModel.prototype.type;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/model/tour_segment.model.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TourSegmentModel = /** @class */ (function () {
        function TourSegmentModel() {
        }
        return TourSegmentModel;
    }());
    if (false) {
        /** @type {?} */
        TourSegmentModel.prototype.title;
        /** @type {?} */
        TourSegmentModel.prototype.from;
        /** @type {?} */
        TourSegmentModel.prototype.to;
        /** @type {?} */
        TourSegmentModel.prototype.tour;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/model/index.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/services/ol-map-decorator.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var OlMapDecoratorService = /** @class */ (function () {
        function OlMapDecoratorService() {
            this.events = {};
            this.tourSegments = {};
            this.points = {};
            this.dateToColor = (/**
             * @param {?} date
             * @return {?}
             */
            function (date) {
                /** @type {?} */
                var minDate = new Date(date.getFullYear(), date.getMonth() - 1, 0);
                /** @type {?} */
                var maxDate = new Date(date.getFullYear(), date.getMonth(), 0);
                return 'hsl(' + ((date.getTime() - minDate.getTime()) / (maxDate.getTime() - minDate.getTime())) + 'turn, 100%, 50%)';
            });
        }
        /**
         * @param {?} config
         * @param {?} geoData
         * @return {?}
         */
        OlMapDecoratorService.prototype.initMap = /**
         * @param {?} config
         * @param {?} geoData
         * @return {?}
         */
        function (config, geoData) {
            var e_1, _a, e_2, _b, e_3, _c;
            var _this = this;
            this.source = new openlayers.source.OSM();
            this.popup = document.getElementById(config.popup);
            this.overlay = new openlayers.Overlay({ element: this.popup });
            /** @type {?} */
            var entries = {};
            /** @type {?} */
            var features = [];
            try {
                for (var _d = __values(Object.entries(geoData.events)), _e = _d.next(); !_e.done; _e = _d.next()) {
                    var _f = __read(_e.value, 2), eventId = _f[0], event_1 = _f[1];
                    /** @type {?} */
                    var feature = new openlayers.Feature(new openlayers.geom.Point(openlayers.proj.fromLonLat([event_1.point.lng, event_1.point.lat])));
                    /** @type {?} */
                    var id = 'event_' + eventId;
                    feature.setId(id);
                    features.push(feature);
                    this.events[id] = event_1;
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_e && !_e.done && (_a = _d.return)) _a.call(_d);
                }
                finally { if (e_1) throw e_1.error; }
            }
            try {
                for (var _g = __values(Object.entries(geoData.tours)), _h = _g.next(); !_h.done; _h = _g.next()) {
                    var _j = __read(_h.value, 2), tourId = _j[0], tour = _j[1];
                    /** @type {?} */
                    var tourFromFeature = new openlayers.Feature(new openlayers.geom.Point(openlayers.proj.fromLonLat([tour.from.lng, tour.from.lat])));
                    tourFromFeature.setId('tour_from_' + tourId);
                    features.push(tourFromFeature);
                    this.points['tour_from_' + tourId] = tour.from;
                    /** @type {?} */
                    var tourToFeature = new openlayers.Feature(new openlayers.geom.Point(openlayers.proj.fromLonLat([tour.from.lng, tour.from.lat])));
                    tourToFeature.setId('tour_to_' + tourId);
                    features.push(tourToFeature);
                    this.points['tour_to_' + tourId] = tour.to;
                    try {
                        for (var _k = (e_3 = void 0, __values(Object.entries(tour.segments))), _l = _k.next(); !_l.done; _l = _k.next()) {
                            var _m = __read(_l.value, 2), segmentId = _m[0], tourSegment = _m[1];
                            tourSegment.tour = tour;
                            /** @type {?} */
                            var feature = new openlayers.Feature(new openlayers.geom.LineString([
                                openlayers.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat]),
                                openlayers.proj.fromLonLat([tourSegment.to.lng, tourSegment.to.lat])
                            ]));
                            feature.setId('segment_' + segmentId);
                            features.push(feature);
                            this.tourSegments['segment_' + segmentId] = tourSegment;
                            /** @type {?} */
                            var fromFeature = new openlayers.Feature(new openlayers.geom.Point(openlayers.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                            fromFeature.setId('segment_from_' + segmentId);
                            features.push(fromFeature);
                            this.points['segment_from_' + segmentId] = tourSegment.from;
                            /** @type {?} */
                            var toFeature = new openlayers.Feature(new openlayers.geom.Point(openlayers.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                            toFeature.setId('segment_to_' + segmentId);
                            features.push(toFeature);
                            this.points['segment_to_' + segmentId] = tourSegment.to;
                        }
                    }
                    catch (e_3_1) { e_3 = { error: e_3_1 }; }
                    finally {
                        try {
                            if (_l && !_l.done && (_c = _k.return)) _c.call(_k);
                        }
                        finally { if (e_3) throw e_3.error; }
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_h && !_h.done && (_b = _g.return)) _b.call(_g);
                }
                finally { if (e_2) throw e_2.error; }
            }
            /** @type {?} */
            var layers = [
                new openlayers.layer.Tile({ source: this.source }),
                new openlayers.layer.Vector({
                    source: new openlayers.source.Vector({ features: features }),
                    zIndex: Infinity,
                    style: (/**
                     * @param {?} feature
                     * @return {?}
                     */
                    function (feature) {
                        /** @type {?} */
                        var id = feature.getId();
                        if (_this.events[id]) {
                            /** @type {?} */
                            var entry = _this.events[id];
                            /** @type {?} */
                            var date = new Date(entry.date_from * 1000);
                            return new openlayers.style.Style({
                                image: new openlayers.style.Circle({
                                    radius: 6,
                                    fill: new openlayers.style.Fill({ color: [225, 0, 255,] })
                                })
                            });
                        }
                        if (_this.points[id]) {
                            var _a = __read(id.split(/\_/), 3), domain = _a[0], loc = _a[1], entryId = _a[2];
                            if (loc === 'from' || loc === 'to') {
                                if (domain === 'segment') {
                                    return new openlayers.style.Style({
                                        image: new openlayers.style.Circle({
                                            radius: 7,
                                            fill: new openlayers.style.Fill({ color: [0, 0, 255,] })
                                        })
                                    });
                                }
                                else if (domain === 'tour') {
                                    return new openlayers.style.Style({
                                        image: new openlayers.style.Circle({
                                            radius: 5,
                                            fill: new openlayers.style.Fill({ color: [0, 255, 255, 0.6] })
                                        })
                                    });
                                }
                            }
                        }
                        return new openlayers.style.Style({
                            stroke: new openlayers.style.Stroke({
                                width: 3,
                                color: [255, 0, 0, 1]
                            }),
                            fill: new openlayers.style.Fill({
                                color: [0, 0, 255, 0.6]
                            })
                        });
                    }),
                })
            ];
            this.map = new openlayers.Map({
                controls: openlayers.control.defaults({ rotate: false }).extend([
                    new openlayers.control.FullScreen(),
                    new openlayers.control.OverviewMap({
                        layers: [
                            new openlayers.layer.Tile({ source: this.source })
                        ]
                    }),
                    new openlayers.control.ScaleLine({ minWidth: 120 })
                ]),
                interactions: openlayers.interaction.defaults({
                    altShiftDragRotate: false,
                    pinchRotate: false
                }),
                layers: layers,
                overlays: [this.overlay],
                target: config.map,
                view: new openlayers.View({
                    center: openlayers.proj.fromLonLat([config.longitude, config.latitude]),
                    zoom: config.zoom
                })
            });
            // todo: move them back to component, where it belongs to.
            this.map.on('pointermove', (/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                /** @type {?} */
                var pixel = _this.map.getEventPixel(event.originalEvent);
                /** @type {?} */
                var hit = _this.map.hasFeatureAtPixel(pixel, { hitTolerance: 10 });
                document.getElementById(_this.map.getTarget()).style.cursor = hit ? 'pointer' : '';
            }));
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        OlMapDecoratorService.prototype.onSingleClick = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) {
            var _this = this;
            this.map.on('singleclick', (/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                /** @type {?} */
                var usedEvents = [];
                /** @type {?} */
                var usedTourSegments = [];
                _this.map.forEachFeatureAtPixel(event.pixel, (/**
                 * @param {?} feature
                 * @param {?} layer
                 * @return {?}
                 */
                function (feature, layer) {
                    /** @type {?} */
                    var id = feature.getId();
                    if (id === undefined) {
                        return;
                    }
                    if (_this.events[id]) {
                        usedEvents.push(_this.events[id]);
                        return;
                    }
                    else if (_this.points[id]) {
                        var _a = __read(id.split(/\_/), 3), domain = _a[0], loc = _a[1], entryId = _a[2];
                        if (_this.tourSegments[domain + '_' + entryId]) {
                            usedTourSegments.push(_this.tourSegments[domain + '_' + entryId]);
                            return;
                        }
                    }
                }), { hitTolerance: 10 });
                usedTourSegments.sort((/**
                 * @param {?} x
                 * @param {?} y
                 * @return {?}
                 */
                function (x, y) { return x - y; }));
                usedEvents.sort((/**
                 * @param {?} x
                 * @param {?} y
                 * @return {?}
                 */
                function (x, y) { return x - y; }));
                /** @type {?} */
                var lng = 0;
                /** @type {?} */
                var lat = 0;
                /** @type {?} */
                var length = 0;
                usedEvents.forEach((/**
                 * @param {?} tourEvent
                 * @return {?}
                 */
                function (tourEvent) {
                    lng += tourEvent.point.lng;
                    lat += tourEvent.point.lat;
                }));
                length += usedEvents.length;
                // usedTourSegments.forEach(segment => {
                //   lng += segment.to.lng - segment.from.lng;
                //   lat += segment.to.lat - segment.from.lat;
                // });
                // length += usedTourSegments.length;
                lng /= length;
                lat /= length;
                if (usedEvents.length || usedTourSegments.length) {
                    _this.overlay.setPosition(openlayers.proj.fromLonLat([lng, lat]));
                }
                else {
                    _this.overlay.setPosition(undefined);
                }
                fn({ events: usedEvents, segments: usedTourSegments });
            }));
        };
        OlMapDecoratorService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        OlMapDecoratorService.ctorParameters = function () { return []; };
        /** @nocollapse */ OlMapDecoratorService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function OlMapDecoratorService_Factory() { return new OlMapDecoratorService(); }, token: OlMapDecoratorService, providedIn: "root" });
        return OlMapDecoratorService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        OlMapDecoratorService.prototype.map;
        /**
         * @type {?}
         * @private
         */
        OlMapDecoratorService.prototype.overlay;
        /**
         * @type {?}
         * @private
         */
        OlMapDecoratorService.prototype.popup;
        /**
         * @type {?}
         * @private
         */
        OlMapDecoratorService.prototype.source;
        /**
         * @type {?}
         * @private
         */
        OlMapDecoratorService.prototype.events;
        /**
         * @type {?}
         * @private
         */
        OlMapDecoratorService.prototype.tourSegments;
        /**
         * @type {?}
         * @private
         */
        OlMapDecoratorService.prototype.points;
        /**
         * @type {?}
         * @private
         */
        OlMapDecoratorService.prototype.dateToColor;
    }

    exports.OlMapDecoratorService = OlMapDecoratorService;
    exports.TourCommonsModule = TourCommonsModule;
    exports.TourOrganizerType = TourOrganizerType;
    exports.TourSegmentModel = TourSegmentModel;
    exports.ɵa = TourCommonsComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=tour-commons.umd.js.map
