import { Component, NgModule, Injectable, ɵɵdefineInjectable } from '@angular/core';
import { source, Overlay, Feature, geom, proj, layer, style, Map, control, interaction, View } from 'openlayers';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/tour-commons.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TourCommonsComponent {
}
TourCommonsComponent.decorators = [
    { type: Component, args: [{
                selector: 'tour-commons',
                template: `
        <h1>Tour CommonsComponent</h1>
    `
            }] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/tour-commons.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TourCommonsModule {
}
TourCommonsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [TourCommonsComponent],
                imports: [],
                exports: [TourCommonsComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_event.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function TourEvent() { }
if (false) {
    /** @type {?} */
    TourEvent.prototype.point;
    /** @type {?} */
    TourEvent.prototype.author;
    /** @type {?} */
    TourEvent.prototype.title;
    /** @type {?} */
    TourEvent.prototype.url;
    /** @type {?} */
    TourEvent.prototype.date_to;
    /** @type {?} */
    TourEvent.prototype.date_from;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_point.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function TourPoint() { }
if (false) {
    /** @type {?} */
    TourPoint.prototype.lat;
    /** @type {?} */
    TourPoint.prototype.lng;
    /** @type {?|undefined} */
    TourPoint.prototype.name;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/geo_data.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function GeoData() { }
if (false) {
    /** @type {?} */
    GeoData.prototype.events;
    /** @type {?} */
    GeoData.prototype.tours;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function TourModel() { }
if (false) {
    /** @type {?} */
    TourModel.prototype.title;
    /** @type {?} */
    TourModel.prototype.organizer;
    /** @type {?} */
    TourModel.prototype.segments;
    /** @type {?} */
    TourModel.prototype.from;
    /** @type {?} */
    TourModel.prototype.to;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_organizer.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const TourOrganizerType = {
    SINGLE: "single",
    GROUP: "group",
};
/**
 * @record
 */
function TourOrganizerModel() { }
if (false) {
    /** @type {?} */
    TourOrganizerModel.prototype.name;
    /** @type {?} */
    TourOrganizerModel.prototype.type;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_segment.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TourSegmentModel {
}
if (false) {
    /** @type {?} */
    TourSegmentModel.prototype.title;
    /** @type {?} */
    TourSegmentModel.prototype.from;
    /** @type {?} */
    TourSegmentModel.prototype.to;
    /** @type {?} */
    TourSegmentModel.prototype.tour;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/index.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: lib/services/ol-map-decorator.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OlMapDecoratorService {
    constructor() {
        this.events = {};
        this.tourSegments = {};
        this.points = {};
        this.dateToColor = (/**
         * @param {?} date
         * @return {?}
         */
        (date) => {
            /** @type {?} */
            const minDate = new Date(date.getFullYear(), date.getMonth() - 1, 0);
            /** @type {?} */
            const maxDate = new Date(date.getFullYear(), date.getMonth(), 0);
            return 'hsl(' + ((date.getTime() - minDate.getTime()) / (maxDate.getTime() - minDate.getTime())) + 'turn, 100%, 50%)';
        });
    }
    /**
     * @param {?} config
     * @param {?} geoData
     * @return {?}
     */
    initMap(config, geoData) {
        this.source = new source.OSM();
        this.popup = document.getElementById(config.popup);
        this.overlay = new Overlay({ element: this.popup });
        /** @type {?} */
        const entries = {};
        /** @type {?} */
        const features = [];
        for (const [eventId, event] of Object.entries(geoData.events)) {
            /** @type {?} */
            const feature = new Feature(new geom.Point(proj.fromLonLat([event.point.lng, event.point.lat])));
            /** @type {?} */
            const id = 'event_' + eventId;
            feature.setId(id);
            features.push(feature);
            this.events[id] = event;
        }
        for (const [tourId, tour] of Object.entries(geoData.tours)) {
            /** @type {?} */
            const tourFromFeature = new Feature(new geom.Point(proj.fromLonLat([tour.from.lng, tour.from.lat])));
            tourFromFeature.setId('tour_from_' + tourId);
            features.push(tourFromFeature);
            this.points['tour_from_' + tourId] = tour.from;
            /** @type {?} */
            const tourToFeature = new Feature(new geom.Point(proj.fromLonLat([tour.from.lng, tour.from.lat])));
            tourToFeature.setId('tour_to_' + tourId);
            features.push(tourToFeature);
            this.points['tour_to_' + tourId] = tour.to;
            for (const [segmentId, tourSegment] of Object.entries(tour.segments)) {
                tourSegment.tour = tour;
                /** @type {?} */
                const feature = new Feature(new geom.LineString([
                    proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat]),
                    proj.fromLonLat([tourSegment.to.lng, tourSegment.to.lat])
                ]));
                feature.setId('segment_' + segmentId);
                features.push(feature);
                this.tourSegments['segment_' + segmentId] = tourSegment;
                /** @type {?} */
                const fromFeature = new Feature(new geom.Point(proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                fromFeature.setId('segment_from_' + segmentId);
                features.push(fromFeature);
                this.points['segment_from_' + segmentId] = tourSegment.from;
                /** @type {?} */
                const toFeature = new Feature(new geom.Point(proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                toFeature.setId('segment_to_' + segmentId);
                features.push(toFeature);
                this.points['segment_to_' + segmentId] = tourSegment.to;
            }
        }
        /** @type {?} */
        const layers = [
            new layer.Tile({ source: this.source }),
            new layer.Vector({
                source: new source.Vector({ features }),
                zIndex: Infinity,
                style: (/**
                 * @param {?} feature
                 * @return {?}
                 */
                (feature) => {
                    /** @type {?} */
                    const id = feature.getId();
                    if (this.events[id]) {
                        /** @type {?} */
                        const entry = this.events[id];
                        /** @type {?} */
                        const date = new Date(entry.date_from * 1000);
                        return new style.Style({
                            image: new style.Circle({
                                radius: 6,
                                fill: new style.Fill({ color: [225, 0, 255,] })
                            })
                        });
                    }
                    if (this.points[id]) {
                        const [domain, loc, entryId] = id.split(/\_/);
                        if (loc === 'from' || loc === 'to') {
                            if (domain === 'segment') {
                                return new style.Style({
                                    image: new style.Circle({
                                        radius: 7,
                                        fill: new style.Fill({ color: [0, 0, 255,] })
                                    })
                                });
                            }
                            else if (domain === 'tour') {
                                return new style.Style({
                                    image: new style.Circle({
                                        radius: 5,
                                        fill: new style.Fill({ color: [0, 255, 255, 0.6] })
                                    })
                                });
                            }
                        }
                    }
                    return new style.Style({
                        stroke: new style.Stroke({
                            width: 3,
                            color: [255, 0, 0, 1]
                        }),
                        fill: new style.Fill({
                            color: [0, 0, 255, 0.6]
                        })
                    });
                }),
            })
        ];
        this.map = new Map({
            controls: control.defaults({ rotate: false }).extend([
                new control.FullScreen(),
                new control.OverviewMap({
                    layers: [
                        new layer.Tile({ source: this.source })
                    ]
                }),
                new control.ScaleLine({ minWidth: 120 })
            ]),
            interactions: interaction.defaults({
                altShiftDragRotate: false,
                pinchRotate: false
            }),
            layers,
            overlays: [this.overlay],
            target: config.map,
            view: new View({
                center: proj.fromLonLat([config.longitude, config.latitude]),
                zoom: config.zoom
            })
        });
        // todo: move them back to component, where it belongs to.
        this.map.on('pointermove', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            /** @type {?} */
            const pixel = this.map.getEventPixel(event.originalEvent);
            /** @type {?} */
            const hit = this.map.hasFeatureAtPixel(pixel, { hitTolerance: 10 });
            document.getElementById(this.map.getTarget()).style.cursor = hit ? 'pointer' : '';
        }));
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    onSingleClick(fn) {
        this.map.on('singleclick', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            /** @type {?} */
            const usedEvents = [];
            /** @type {?} */
            const usedTourSegments = [];
            this.map.forEachFeatureAtPixel(event.pixel, (/**
             * @param {?} feature
             * @param {?} layer
             * @return {?}
             */
            (feature, layer) => {
                /** @type {?} */
                const id = feature.getId();
                if (id === undefined) {
                    return;
                }
                if (this.events[id]) {
                    usedEvents.push(this.events[id]);
                    return;
                }
                else if (this.points[id]) {
                    const [domain, loc, entryId] = id.split(/\_/);
                    if (this.tourSegments[domain + '_' + entryId]) {
                        usedTourSegments.push(this.tourSegments[domain + '_' + entryId]);
                        return;
                    }
                }
            }), { hitTolerance: 10 });
            usedTourSegments.sort((/**
             * @param {?} x
             * @param {?} y
             * @return {?}
             */
            (x, y) => x - y));
            usedEvents.sort((/**
             * @param {?} x
             * @param {?} y
             * @return {?}
             */
            (x, y) => x - y));
            /** @type {?} */
            let lng = 0;
            /** @type {?} */
            let lat = 0;
            /** @type {?} */
            let length = 0;
            usedEvents.forEach((/**
             * @param {?} tourEvent
             * @return {?}
             */
            tourEvent => {
                lng += tourEvent.point.lng;
                lat += tourEvent.point.lat;
            }));
            length += usedEvents.length;
            // usedTourSegments.forEach(segment => {
            //   lng += segment.to.lng - segment.from.lng;
            //   lat += segment.to.lat - segment.from.lat;
            // });
            // length += usedTourSegments.length;
            lng /= length;
            lat /= length;
            if (usedEvents.length || usedTourSegments.length) {
                this.overlay.setPosition(proj.fromLonLat([lng, lat]));
            }
            else {
                this.overlay.setPosition(undefined);
            }
            fn({ events: usedEvents, segments: usedTourSegments });
        }));
    }
}
OlMapDecoratorService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
OlMapDecoratorService.ctorParameters = () => [];
/** @nocollapse */ OlMapDecoratorService.ngInjectableDef = ɵɵdefineInjectable({ factory: function OlMapDecoratorService_Factory() { return new OlMapDecoratorService(); }, token: OlMapDecoratorService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.map;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.overlay;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.popup;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.source;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.events;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.tourSegments;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.points;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.dateToColor;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/services/index.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: tour-commons.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { OlMapDecoratorService, TourCommonsModule, TourOrganizerType, TourSegmentModel, TourCommonsComponent as ɵa };
//# sourceMappingURL=tour-commons.js.map
