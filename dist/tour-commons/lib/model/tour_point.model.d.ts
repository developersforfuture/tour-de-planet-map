export interface TourPoint {
    lat: number;
    lng: number;
    name?: string;
}
