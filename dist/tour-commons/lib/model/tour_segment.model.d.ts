import { TourPoint } from './tour_point.model';
import { TourModel } from './tour.model';
export declare class TourSegmentModel {
    title?: string;
    from: TourPoint;
    to: TourPoint;
    tour?: TourModel;
}
