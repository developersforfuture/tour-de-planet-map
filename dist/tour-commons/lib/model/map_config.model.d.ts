export interface MapConfigModel {
    latitude: number;
    longitude: number;
    zoom: number;
    popup: string;
    map: string;
}
