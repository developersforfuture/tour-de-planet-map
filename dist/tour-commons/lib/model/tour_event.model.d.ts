import { TourPoint } from './tour_point.model';
export interface TourEvent {
    point: TourPoint;
    author: string;
    title: string;
    url: string;
    date_to: number;
    date_from: number;
}
