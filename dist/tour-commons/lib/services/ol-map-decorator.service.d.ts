import { MapConfigModel } from '../model/map_config.model';
import { GeoData } from '../model';
export declare class OlMapDecoratorService {
    private map;
    private overlay;
    private popup;
    private source;
    private events;
    private tourSegments;
    private points;
    constructor();
    initMap(config: MapConfigModel, geoData: GeoData): void;
    onSingleClick(fn: CallableFunction): void;
    private dateToColor;
}
