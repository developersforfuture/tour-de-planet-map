/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of tour-commons
 */
export { TourCommonsModule } from './lib/tour-commons.module';
export { TourOrganizerType, TourSegmentModel } from './lib/model';
export { OlMapDecoratorService } from './lib/services';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3RvdXItY29tbW9ucy8iLCJzb3VyY2VzIjpbInB1YmxpYy1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFJQSxrQ0FBYywyQkFBMkIsQ0FBQztBQUMxQyxvREFBYyxhQUFhLENBQUM7QUFDNUIsc0NBQWMsZ0JBQWdCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIHRvdXItY29tbW9uc1xuICovXG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL3RvdXItY29tbW9ucy5tb2R1bGUnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvbW9kZWwnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvc2VydmljZXMnO1xuIl19