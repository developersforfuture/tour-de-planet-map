/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_segment.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class TourSegmentModel {
}
if (false) {
    /** @type {?} */
    TourSegmentModel.prototype.title;
    /** @type {?} */
    TourSegmentModel.prototype.from;
    /** @type {?} */
    TourSegmentModel.prototype.to;
    /** @type {?} */
    TourSegmentModel.prototype.tour;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG91cl9zZWdtZW50Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdG91ci1jb21tb25zLyIsInNvdXJjZXMiOlsibGliL21vZGVsL3RvdXJfc2VnbWVudC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUdBLE1BQU0sT0FBTyxnQkFBZ0I7Q0FLNUI7OztJQUpHLGlDQUFlOztJQUNmLGdDQUFnQjs7SUFDaEIsOEJBQWM7O0lBQ2QsZ0NBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVG91clBvaW50IH0gZnJvbSAnLi90b3VyX3BvaW50Lm1vZGVsJztcbmltcG9ydCB7IFRvdXJNb2RlbCB9IGZyb20gJy4vdG91ci5tb2RlbCc7XG5cbmV4cG9ydCBjbGFzcyBUb3VyU2VnbWVudE1vZGVsIHtcbiAgICB0aXRsZT86IHN0cmluZztcbiAgICBmcm9tOiBUb3VyUG9pbnQ7XG4gICAgdG86IFRvdXJQb2ludDtcbiAgICB0b3VyPzogVG91ck1vZGVsO1xufVxuIl19