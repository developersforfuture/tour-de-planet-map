/**
 * @fileoverview added by tsickle
 * Generated from: lib/model/tour_event.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function TourEvent() { }
if (false) {
    /** @type {?} */
    TourEvent.prototype.point;
    /** @type {?} */
    TourEvent.prototype.author;
    /** @type {?} */
    TourEvent.prototype.title;
    /** @type {?} */
    TourEvent.prototype.url;
    /** @type {?} */
    TourEvent.prototype.date_to;
    /** @type {?} */
    TourEvent.prototype.date_from;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG91cl9ldmVudC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3RvdXItY29tbW9ucy8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbC90b3VyX2V2ZW50Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBRUEsK0JBT0M7OztJQU5HLDBCQUFpQjs7SUFDakIsMkJBQWU7O0lBQ2YsMEJBQWM7O0lBQ2Qsd0JBQVk7O0lBQ1osNEJBQWdCOztJQUNoQiw4QkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUb3VyUG9pbnQgfSBmcm9tICcuL3RvdXJfcG9pbnQubW9kZWwnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFRvdXJFdmVudCB7XG4gICAgcG9pbnQ6IFRvdXJQb2ludDtcbiAgICBhdXRob3I6IHN0cmluZztcbiAgICB0aXRsZTogc3RyaW5nO1xuICAgIHVybDogc3RyaW5nO1xuICAgIGRhdGVfdG86IG51bWJlcjtcbiAgICBkYXRlX2Zyb206IG51bWJlcjtcbn1cbiJdfQ==