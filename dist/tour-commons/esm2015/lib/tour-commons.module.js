/**
 * @fileoverview added by tsickle
 * Generated from: lib/tour-commons.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { TourCommonsComponent } from './tour-commons.component';
export class TourCommonsModule {
}
TourCommonsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [TourCommonsComponent],
                imports: [],
                exports: [TourCommonsComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG91ci1jb21tb25zLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3RvdXItY29tbW9ucy8iLCJzb3VyY2VzIjpbImxpYi90b3VyLWNvbW1vbnMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQVFoRSxNQUFNLE9BQU8saUJBQWlCOzs7WUFON0IsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLG9CQUFvQixDQUFDO2dCQUNwQyxPQUFPLEVBQUUsRUFDUjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQzthQUNoQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBUb3VyQ29tbW9uc0NvbXBvbmVudCB9IGZyb20gJy4vdG91ci1jb21tb25zLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1RvdXJDb21tb25zQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICBdLFxuICBleHBvcnRzOiBbVG91ckNvbW1vbnNDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFRvdXJDb21tb25zTW9kdWxlIHsgfVxuIl19