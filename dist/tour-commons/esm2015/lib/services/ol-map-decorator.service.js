/**
 * @fileoverview added by tsickle
 * Generated from: lib/services/ol-map-decorator.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as ol from 'openlayers';
import * as i0 from "@angular/core";
export class OlMapDecoratorService {
    constructor() {
        this.events = {};
        this.tourSegments = {};
        this.points = {};
        this.dateToColor = (/**
         * @param {?} date
         * @return {?}
         */
        (date) => {
            /** @type {?} */
            const minDate = new Date(date.getFullYear(), date.getMonth() - 1, 0);
            /** @type {?} */
            const maxDate = new Date(date.getFullYear(), date.getMonth(), 0);
            return 'hsl(' + ((date.getTime() - minDate.getTime()) / (maxDate.getTime() - minDate.getTime())) + 'turn, 100%, 50%)';
        });
    }
    /**
     * @param {?} config
     * @param {?} geoData
     * @return {?}
     */
    initMap(config, geoData) {
        this.source = new ol.source.OSM();
        this.popup = document.getElementById(config.popup);
        this.overlay = new ol.Overlay({ element: this.popup });
        /** @type {?} */
        const entries = {};
        /** @type {?} */
        const features = [];
        for (const [eventId, event] of Object.entries(geoData.events)) {
            /** @type {?} */
            const feature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([event.point.lng, event.point.lat])));
            /** @type {?} */
            const id = 'event_' + eventId;
            feature.setId(id);
            features.push(feature);
            this.events[id] = event;
        }
        for (const [tourId, tour] of Object.entries(geoData.tours)) {
            /** @type {?} */
            const tourFromFeature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([tour.from.lng, tour.from.lat])));
            tourFromFeature.setId('tour_from_' + tourId);
            features.push(tourFromFeature);
            this.points['tour_from_' + tourId] = tour.from;
            /** @type {?} */
            const tourToFeature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([tour.from.lng, tour.from.lat])));
            tourToFeature.setId('tour_to_' + tourId);
            features.push(tourToFeature);
            this.points['tour_to_' + tourId] = tour.to;
            for (const [segmentId, tourSegment] of Object.entries(tour.segments)) {
                tourSegment.tour = tour;
                /** @type {?} */
                const feature = new ol.Feature(new ol.geom.LineString([
                    ol.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat]),
                    ol.proj.fromLonLat([tourSegment.to.lng, tourSegment.to.lat])
                ]));
                feature.setId('segment_' + segmentId);
                features.push(feature);
                this.tourSegments['segment_' + segmentId] = tourSegment;
                /** @type {?} */
                const fromFeature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                fromFeature.setId('segment_from_' + segmentId);
                features.push(fromFeature);
                this.points['segment_from_' + segmentId] = tourSegment.from;
                /** @type {?} */
                const toFeature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                toFeature.setId('segment_to_' + segmentId);
                features.push(toFeature);
                this.points['segment_to_' + segmentId] = tourSegment.to;
            }
        }
        /** @type {?} */
        const layers = [
            new ol.layer.Tile({ source: this.source }),
            new ol.layer.Vector({
                source: new ol.source.Vector({ features }),
                zIndex: Infinity,
                style: (/**
                 * @param {?} feature
                 * @return {?}
                 */
                (feature) => {
                    /** @type {?} */
                    const id = feature.getId();
                    if (this.events[id]) {
                        /** @type {?} */
                        const entry = this.events[id];
                        /** @type {?} */
                        const date = new Date(entry.date_from * 1000);
                        return new ol.style.Style({
                            image: new ol.style.Circle({
                                radius: 6,
                                fill: new ol.style.Fill({ color: [225, 0, 255,] })
                            })
                        });
                    }
                    if (this.points[id]) {
                        const [domain, loc, entryId] = id.split(/\_/);
                        if (loc === 'from' || loc === 'to') {
                            if (domain === 'segment') {
                                return new ol.style.Style({
                                    image: new ol.style.Circle({
                                        radius: 7,
                                        fill: new ol.style.Fill({ color: [0, 0, 255,] })
                                    })
                                });
                            }
                            else if (domain === 'tour') {
                                return new ol.style.Style({
                                    image: new ol.style.Circle({
                                        radius: 5,
                                        fill: new ol.style.Fill({ color: [0, 255, 255, 0.6] })
                                    })
                                });
                            }
                        }
                    }
                    return new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            width: 3,
                            color: [255, 0, 0, 1]
                        }),
                        fill: new ol.style.Fill({
                            color: [0, 0, 255, 0.6]
                        })
                    });
                }),
            })
        ];
        this.map = new ol.Map({
            controls: ol.control.defaults({ rotate: false }).extend([
                new ol.control.FullScreen(),
                new ol.control.OverviewMap({
                    layers: [
                        new ol.layer.Tile({ source: this.source })
                    ]
                }),
                new ol.control.ScaleLine({ minWidth: 120 })
            ]),
            interactions: ol.interaction.defaults({
                altShiftDragRotate: false,
                pinchRotate: false
            }),
            layers,
            overlays: [this.overlay],
            target: config.map,
            view: new ol.View({
                center: ol.proj.fromLonLat([config.longitude, config.latitude]),
                zoom: config.zoom
            })
        });
        // todo: move them back to component, where it belongs to.
        this.map.on('pointermove', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            /** @type {?} */
            const pixel = this.map.getEventPixel(event.originalEvent);
            /** @type {?} */
            const hit = this.map.hasFeatureAtPixel(pixel, { hitTolerance: 10 });
            document.getElementById(this.map.getTarget()).style.cursor = hit ? 'pointer' : '';
        }));
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    onSingleClick(fn) {
        this.map.on('singleclick', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            /** @type {?} */
            const usedEvents = [];
            /** @type {?} */
            const usedTourSegments = [];
            this.map.forEachFeatureAtPixel(event.pixel, (/**
             * @param {?} feature
             * @param {?} layer
             * @return {?}
             */
            (feature, layer) => {
                /** @type {?} */
                const id = feature.getId();
                if (id === undefined) {
                    return;
                }
                if (this.events[id]) {
                    usedEvents.push(this.events[id]);
                    return;
                }
                else if (this.points[id]) {
                    const [domain, loc, entryId] = id.split(/\_/);
                    if (this.tourSegments[domain + '_' + entryId]) {
                        usedTourSegments.push(this.tourSegments[domain + '_' + entryId]);
                        return;
                    }
                }
            }), { hitTolerance: 10 });
            usedTourSegments.sort((/**
             * @param {?} x
             * @param {?} y
             * @return {?}
             */
            (x, y) => x - y));
            usedEvents.sort((/**
             * @param {?} x
             * @param {?} y
             * @return {?}
             */
            (x, y) => x - y));
            /** @type {?} */
            let lng = 0;
            /** @type {?} */
            let lat = 0;
            /** @type {?} */
            let length = 0;
            usedEvents.forEach((/**
             * @param {?} tourEvent
             * @return {?}
             */
            tourEvent => {
                lng += tourEvent.point.lng;
                lat += tourEvent.point.lat;
            }));
            length += usedEvents.length;
            // usedTourSegments.forEach(segment => {
            //   lng += segment.to.lng - segment.from.lng;
            //   lat += segment.to.lat - segment.from.lat;
            // });
            // length += usedTourSegments.length;
            lng /= length;
            lat /= length;
            if (usedEvents.length || usedTourSegments.length) {
                this.overlay.setPosition(ol.proj.fromLonLat([lng, lat]));
            }
            else {
                this.overlay.setPosition(undefined);
            }
            fn({ events: usedEvents, segments: usedTourSegments });
        }));
    }
}
OlMapDecoratorService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
OlMapDecoratorService.ctorParameters = () => [];
/** @nocollapse */ OlMapDecoratorService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function OlMapDecoratorService_Factory() { return new OlMapDecoratorService(); }, token: OlMapDecoratorService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.map;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.overlay;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.popup;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.source;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.events;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.tourSegments;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.points;
    /**
     * @type {?}
     * @private
     */
    OlMapDecoratorService.prototype.dateToColor;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2wtbWFwLWRlY29yYXRvci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdG91ci1jb21tb25zLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL29sLW1hcC1kZWNvcmF0b3Iuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxLQUFLLEVBQUUsTUFBTSxZQUFZLENBQUM7O0FBT2pDLE1BQU0sT0FBTyxxQkFBcUI7SUFTaEM7UUFKUSxXQUFNLEdBQThCLEVBQUUsQ0FBQztRQUN2QyxpQkFBWSxHQUFxQyxFQUFFLENBQUM7UUFDcEQsV0FBTSxHQUE4QixFQUFFLENBQUM7UUEwTXZDLGdCQUFXOzs7O1FBQUksQ0FBQyxJQUFJLEVBQUUsRUFBRTs7a0JBQ3hCLE9BQU8sR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7O2tCQUM5RCxPQUFPLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDaEUsT0FBTyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsR0FBRyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsR0FBRyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxHQUFHLGtCQUFrQixDQUFDO1FBQ3hILENBQUMsRUFBQTtJQTVNZSxDQUFDOzs7Ozs7SUFHakIsT0FBTyxDQUFDLE1BQXNCLEVBQUUsT0FBZ0I7UUFDOUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDbEMsSUFBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFDLENBQUMsQ0FBQzs7Y0FBVSxPQUFPLEdBQUcsRUFBRTs7Y0FDckUsUUFBUSxHQUFHLEVBQUU7UUFFbkIsS0FBSyxNQUFNLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFOztrQkFDdkQsT0FBTyxHQUFHLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FDNUIsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUMxRTs7a0JBQ0ssRUFBRSxHQUFHLFFBQVEsR0FBRyxPQUFPO1lBQzdCLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQztTQUN6QjtRQUVELEtBQUssTUFBTSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTs7a0JBQ3BELGVBQWUsR0FBRyxJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQ3BDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FDdEU7WUFDRCxlQUFlLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsQ0FBQztZQUM3QyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7O2tCQUV6QyxhQUFhLEdBQUcsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUNsQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQ3RFO1lBQ0QsYUFBYSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLENBQUM7WUFDekMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1lBRTNDLEtBQUssTUFBTSxDQUFDLFNBQVMsRUFBRSxXQUFXLENBQUMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDcEUsV0FBVyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7O3NCQUNsQixPQUFPLEdBQUcsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUM1QixJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO29CQUNyQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ2hFLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDN0QsQ0FBQyxDQUNIO2dCQUNELE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxDQUFDO2dCQUN0QyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUMsR0FBRyxXQUFXLENBQUM7O3NCQUVsRCxXQUFXLEdBQUcsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUNoQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQ3BGO2dCQUNELFdBQVcsQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQyxDQUFDO2dCQUMvQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsR0FBRyxTQUFTLENBQUMsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDOztzQkFFdEQsU0FBUyxHQUFHLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FDOUIsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUNwRjtnQkFDRCxTQUFTLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUMsQ0FBQztnQkFDM0MsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQzthQUN6RDtTQUNGOztjQUVLLE1BQU0sR0FBRztZQUNiLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBQyxDQUFDO1lBQ3hDLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7Z0JBQ2xCLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUMsUUFBUSxFQUFDLENBQUM7Z0JBQ3hDLE1BQU0sRUFBRSxRQUFRO2dCQUNoQixLQUFLOzs7O2dCQUFFLENBQUMsT0FBTyxFQUFFLEVBQUU7OzBCQUNYLEVBQUUsR0FBRyxPQUFPLENBQUMsS0FBSyxFQUFFO29CQUMxQixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEVBQUU7OzhCQUNiLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQzs7OEJBQ3ZCLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQzt3QkFFN0MsT0FBTyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDOzRCQUN4QixLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztnQ0FDekIsTUFBTSxFQUFFLENBQUM7Z0NBQ1QsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQyxLQUFLLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRyxFQUFDLENBQUM7NkJBQ2xELENBQUM7eUJBQ0gsQ0FBQyxDQUFDO3FCQUNKO29CQUVELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsRUFBRTs4QkFDYixDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7d0JBQzdDLElBQUksR0FBRyxLQUFLLE1BQU0sSUFBSSxHQUFHLEtBQUssSUFBSSxFQUFFOzRCQUNsQyxJQUFJLE1BQU0sS0FBSyxTQUFTLEVBQUU7Z0NBQ3hCLE9BQU8sSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztvQ0FDeEIsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7d0NBQ3pCLE1BQU0sRUFBRSxDQUFDO3dDQUNULElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUcsRUFBQyxDQUFDO3FDQUNoRCxDQUFDO2lDQUNILENBQUMsQ0FBQzs2QkFDSjtpQ0FBTSxJQUFJLE1BQU0sS0FBSyxNQUFNLEVBQUU7Z0NBQzVCLE9BQU8sSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztvQ0FDeEIsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7d0NBQ3pCLE1BQU0sRUFBRSxDQUFDO3dDQUNULElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUMsQ0FBQztxQ0FDckQsQ0FBQztpQ0FDSCxDQUFDLENBQUM7NkJBQ0o7eUJBQ0Y7cUJBQ0Y7b0JBRUQsT0FBTyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO3dCQUN4QixNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzs0QkFDMUIsS0FBSyxFQUFFLENBQUM7NEJBQ1IsS0FBSyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO3lCQUN0QixDQUFDO3dCQUNGLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDOzRCQUN0QixLQUFLLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUM7eUJBQ3hCLENBQUM7cUJBQ0gsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQTthQUNGLENBQUM7U0FDSDtRQUVELElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDO1lBQ3BCLFFBQVEsRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztnQkFDcEQsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRTtnQkFDM0IsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztvQkFDekIsTUFBTSxFQUFFO3dCQUNOLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBQyxDQUFDO3FCQUN6QztpQkFDRixDQUFDO2dCQUNGLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBQyxRQUFRLEVBQUUsR0FBRyxFQUFDLENBQUM7YUFDMUMsQ0FBQztZQUNGLFlBQVksRUFBRSxFQUFFLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztnQkFDcEMsa0JBQWtCLEVBQUUsS0FBSztnQkFDekIsV0FBVyxFQUFFLEtBQUs7YUFDbkIsQ0FBQztZQUNGLE1BQU07WUFDTixRQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ3hCLE1BQU0sRUFBRSxNQUFNLENBQUMsR0FBRztZQUNsQixJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDO2dCQUNoQixNQUFNLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDL0QsSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJO2FBQ2xCLENBQUM7U0FDSCxDQUFDLENBQUM7UUFFSCwwREFBMEQ7UUFDMUQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsYUFBYTs7OztRQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7O2tCQUM3QixLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQzs7a0JBQ25ELEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxFQUFDLFlBQVksRUFBRSxFQUFFLEVBQUMsQ0FBQztZQUNqRSxRQUFRLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDcEYsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELGFBQWEsQ0FBQyxFQUFvQjtRQUNoQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxhQUFhOzs7O1FBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTs7a0JBQzdCLFVBQVUsR0FBRyxFQUFFOztrQkFDZixnQkFBZ0IsR0FBRyxFQUFFO1lBQzNCLElBQUksQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLEtBQUs7Ozs7O1lBQUUsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUU7O3NCQUN2RCxFQUFFLEdBQVcsT0FBTyxDQUFDLEtBQUssRUFBRTtnQkFDbEMsSUFBSSxFQUFFLEtBQUssU0FBUyxFQUFFO29CQUNwQixPQUFPO2lCQUNSO2dCQUVELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsRUFBRTtvQkFDbkIsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLE9BQU87aUJBQ1I7cUJBQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxFQUFFOzBCQUNwQixDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7b0JBRTdDLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsR0FBRyxHQUFHLE9BQU8sQ0FBQyxFQUFFO3dCQUM3QyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsR0FBRyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQ2pFLE9BQU87cUJBQ1I7aUJBQ0Y7WUFDSCxDQUFDLEdBQUUsRUFBQyxZQUFZLEVBQUUsRUFBRSxFQUFDLENBQUMsQ0FBQztZQUN2QixnQkFBZ0IsQ0FBQyxJQUFJOzs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUFDO1lBQ3ZDLFVBQVUsQ0FBQyxJQUFJOzs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUFDOztnQkFFN0IsR0FBRyxHQUFHLENBQUM7O2dCQUNQLEdBQUcsR0FBRyxDQUFDOztnQkFDUCxNQUFNLEdBQUcsQ0FBQztZQUVkLFVBQVUsQ0FBQyxPQUFPOzs7O1lBQUMsU0FBUyxDQUFDLEVBQUU7Z0JBQzdCLEdBQUcsSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztnQkFDM0IsR0FBRyxJQUFJLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1lBQzdCLENBQUMsRUFBQyxDQUFDO1lBRUgsTUFBTSxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUM7WUFDNUIsd0NBQXdDO1lBQ3hDLDhDQUE4QztZQUM5Qyw4Q0FBOEM7WUFDOUMsTUFBTTtZQUNOLHFDQUFxQztZQUVyQyxHQUFHLElBQUksTUFBTSxDQUFDO1lBQ2QsR0FBRyxJQUFJLE1BQU0sQ0FBQztZQUVkLElBQUksVUFBVSxDQUFDLE1BQU0sSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUMxRDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNyQztZQUVELEVBQUUsQ0FBQyxFQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFDLENBQUMsQ0FBQztRQUN2RCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7OztZQWxORixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7Ozs7Ozs7SUFFQyxvQ0FBb0I7Ozs7O0lBQ3BCLHdDQUE0Qjs7Ozs7SUFDNUIsc0NBQTJCOzs7OztJQUMzQix1Q0FBOEI7Ozs7O0lBQzlCLHVDQUErQzs7Ozs7SUFDL0MsNkNBQTREOzs7OztJQUM1RCx1Q0FBK0M7Ozs7O0lBME0vQyw0Q0FJQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIG9sIGZyb20gJ29wZW5sYXllcnMnO1xuaW1wb3J0IHsgTWFwQ29uZmlnTW9kZWwgfSBmcm9tICcuLi9tb2RlbC9tYXBfY29uZmlnLm1vZGVsJztcbmltcG9ydCB7IEdlb0RhdGEsIFRvdXJQb2ludCwgVG91ckV2ZW50LCBUb3VyU2VnbWVudE1vZGVsfSBmcm9tICcuLi9tb2RlbCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE9sTWFwRGVjb3JhdG9yU2VydmljZSB7XG4gIHByaXZhdGUgbWFwOiBvbC5NYXA7XG4gIHByaXZhdGUgb3ZlcmxheTogb2wuT3ZlckxheTtcbiAgcHJpdmF0ZSBwb3B1cDogSFRNTEVsZW1lbnQ7XG4gIHByaXZhdGUgc291cmNlOiBvbC5zb3VyY2UuT1NNO1xuICBwcml2YXRlIGV2ZW50czoge1tpZDogc3RyaW5nXTogVG91ckV2ZW50fSA9IHt9O1xuICBwcml2YXRlIHRvdXJTZWdtZW50czoge1tpZDogc3RyaW5nXTogVG91clNlZ21lbnRNb2RlbH0gPSB7fTtcbiAgcHJpdmF0ZSBwb2ludHM6IHtbaWQ6IHN0cmluZ106IFRvdXJQb2ludH0gPSB7fTtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG5cbiAgaW5pdE1hcChjb25maWc6IE1hcENvbmZpZ01vZGVsLCBnZW9EYXRhOiBHZW9EYXRhKTogdm9pZCB7XG4gICAgdGhpcy5zb3VyY2UgPSBuZXcgb2wuc291cmNlLk9TTSgpO1xuICAgIHRoaXMucG9wdXAgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb25maWcucG9wdXApO1xuICAgIHRoaXMub3ZlcmxheSA9IG5ldyBvbC5PdmVybGF5KHtlbGVtZW50OiB0aGlzLnBvcHVwfSk7ICAgIGNvbnN0IGVudHJpZXMgPSB7fTtcbiAgICBjb25zdCBmZWF0dXJlcyA9IFtdO1xuXG4gICAgZm9yIChjb25zdCBbZXZlbnRJZCwgZXZlbnRdIG9mIE9iamVjdC5lbnRyaWVzKGdlb0RhdGEuZXZlbnRzKSkge1xuICAgICAgY29uc3QgZmVhdHVyZSA9IG5ldyBvbC5GZWF0dXJlKFxuICAgICAgICBuZXcgb2wuZ2VvbS5Qb2ludChvbC5wcm9qLmZyb21Mb25MYXQoW2V2ZW50LnBvaW50LmxuZywgZXZlbnQucG9pbnQubGF0XSkpXG4gICAgICApO1xuICAgICAgY29uc3QgaWQgPSAnZXZlbnRfJyArIGV2ZW50SWQ7XG4gICAgICBmZWF0dXJlLnNldElkKGlkKTtcbiAgICAgIGZlYXR1cmVzLnB1c2goZmVhdHVyZSk7XG4gICAgICB0aGlzLmV2ZW50c1tpZF0gPSBldmVudDtcbiAgICB9XG5cbiAgICBmb3IgKGNvbnN0IFt0b3VySWQsIHRvdXJdIG9mIE9iamVjdC5lbnRyaWVzKGdlb0RhdGEudG91cnMpKSB7XG4gICAgICBjb25zdCB0b3VyRnJvbUZlYXR1cmUgPSBuZXcgb2wuRmVhdHVyZShcbiAgICAgICAgbmV3IG9sLmdlb20uUG9pbnQob2wucHJvai5mcm9tTG9uTGF0KFt0b3VyLmZyb20ubG5nLCB0b3VyLmZyb20ubGF0XSkpXG4gICAgICApO1xuICAgICAgdG91ckZyb21GZWF0dXJlLnNldElkKCd0b3VyX2Zyb21fJyArIHRvdXJJZCk7XG4gICAgICBmZWF0dXJlcy5wdXNoKHRvdXJGcm9tRmVhdHVyZSk7XG4gICAgICB0aGlzLnBvaW50c1sndG91cl9mcm9tXycgKyB0b3VySWRdID0gdG91ci5mcm9tO1xuXG4gICAgICBjb25zdCB0b3VyVG9GZWF0dXJlID0gbmV3IG9sLkZlYXR1cmUoXG4gICAgICAgIG5ldyBvbC5nZW9tLlBvaW50KG9sLnByb2ouZnJvbUxvbkxhdChbdG91ci5mcm9tLmxuZywgdG91ci5mcm9tLmxhdF0pKVxuICAgICAgKTtcbiAgICAgIHRvdXJUb0ZlYXR1cmUuc2V0SWQoJ3RvdXJfdG9fJyArIHRvdXJJZCk7XG4gICAgICBmZWF0dXJlcy5wdXNoKHRvdXJUb0ZlYXR1cmUpO1xuICAgICAgdGhpcy5wb2ludHNbJ3RvdXJfdG9fJyArIHRvdXJJZF0gPSB0b3VyLnRvO1xuXG4gICAgICBmb3IgKGNvbnN0IFtzZWdtZW50SWQsIHRvdXJTZWdtZW50XSBvZiBPYmplY3QuZW50cmllcyh0b3VyLnNlZ21lbnRzKSkge1xuICAgICAgICB0b3VyU2VnbWVudC50b3VyID0gdG91cjtcbiAgICAgICAgY29uc3QgZmVhdHVyZSA9IG5ldyBvbC5GZWF0dXJlKFxuICAgICAgICAgIG5ldyBvbC5nZW9tLkxpbmVTdHJpbmcoW1xuICAgICAgICAgICAgb2wucHJvai5mcm9tTG9uTGF0KFt0b3VyU2VnbWVudC5mcm9tLmxuZywgdG91clNlZ21lbnQuZnJvbS5sYXRdKSxcbiAgICAgICAgICAgIG9sLnByb2ouZnJvbUxvbkxhdChbdG91clNlZ21lbnQudG8ubG5nLCB0b3VyU2VnbWVudC50by5sYXRdKVxuICAgICAgICAgIF0pXG4gICAgICAgICk7XG4gICAgICAgIGZlYXR1cmUuc2V0SWQoJ3NlZ21lbnRfJyArIHNlZ21lbnRJZCk7XG4gICAgICAgIGZlYXR1cmVzLnB1c2goZmVhdHVyZSk7XG4gICAgICAgIHRoaXMudG91clNlZ21lbnRzWydzZWdtZW50XycgKyBzZWdtZW50SWRdID0gdG91clNlZ21lbnQ7XG5cbiAgICAgICAgY29uc3QgZnJvbUZlYXR1cmUgPSBuZXcgb2wuRmVhdHVyZShcbiAgICAgICAgICBuZXcgb2wuZ2VvbS5Qb2ludChvbC5wcm9qLmZyb21Mb25MYXQoW3RvdXJTZWdtZW50LmZyb20ubG5nLCB0b3VyU2VnbWVudC5mcm9tLmxhdF0pKVxuICAgICAgICApO1xuICAgICAgICBmcm9tRmVhdHVyZS5zZXRJZCgnc2VnbWVudF9mcm9tXycgKyBzZWdtZW50SWQpO1xuICAgICAgICBmZWF0dXJlcy5wdXNoKGZyb21GZWF0dXJlKTtcbiAgICAgICAgdGhpcy5wb2ludHNbJ3NlZ21lbnRfZnJvbV8nICsgc2VnbWVudElkXSA9IHRvdXJTZWdtZW50LmZyb207XG5cbiAgICAgICAgY29uc3QgdG9GZWF0dXJlID0gbmV3IG9sLkZlYXR1cmUoXG4gICAgICAgICAgbmV3IG9sLmdlb20uUG9pbnQob2wucHJvai5mcm9tTG9uTGF0KFt0b3VyU2VnbWVudC5mcm9tLmxuZywgdG91clNlZ21lbnQuZnJvbS5sYXRdKSlcbiAgICAgICAgKTtcbiAgICAgICAgdG9GZWF0dXJlLnNldElkKCdzZWdtZW50X3RvXycgKyBzZWdtZW50SWQpO1xuICAgICAgICBmZWF0dXJlcy5wdXNoKHRvRmVhdHVyZSk7XG4gICAgICAgIHRoaXMucG9pbnRzWydzZWdtZW50X3RvXycgKyBzZWdtZW50SWRdID0gdG91clNlZ21lbnQudG87XG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgbGF5ZXJzID0gW1xuICAgICAgbmV3IG9sLmxheWVyLlRpbGUoe3NvdXJjZTogdGhpcy5zb3VyY2V9KSxcbiAgICAgIG5ldyBvbC5sYXllci5WZWN0b3Ioe1xuICAgICAgICBzb3VyY2U6IG5ldyBvbC5zb3VyY2UuVmVjdG9yKHtmZWF0dXJlc30pLFxuICAgICAgICB6SW5kZXg6IEluZmluaXR5LFxuICAgICAgICBzdHlsZTogKGZlYXR1cmUpID0+IHtcbiAgICAgICAgICBjb25zdCBpZCA9IGZlYXR1cmUuZ2V0SWQoKTtcbiAgICAgICAgICBpZiAodGhpcy5ldmVudHNbaWRdKSB7XG4gICAgICAgICAgICBjb25zdCBlbnRyeSA9IHRoaXMuZXZlbnRzW2lkXTtcbiAgICAgICAgICAgIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZShlbnRyeS5kYXRlX2Zyb20gKiAxMDAwKTtcblxuICAgICAgICAgICAgcmV0dXJuIG5ldyBvbC5zdHlsZS5TdHlsZSh7XG4gICAgICAgICAgICAgIGltYWdlOiBuZXcgb2wuc3R5bGUuQ2lyY2xlKHtcbiAgICAgICAgICAgICAgICByYWRpdXM6IDYsXG4gICAgICAgICAgICAgICAgZmlsbDogbmV3IG9sLnN0eWxlLkZpbGwoe2NvbG9yOiBbMjI1LCAwLCAyNTUsIF19KVxuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKHRoaXMucG9pbnRzW2lkXSkge1xuICAgICAgICAgICAgY29uc3QgW2RvbWFpbiwgbG9jLCBlbnRyeUlkXSA9IGlkLnNwbGl0KC9cXF8vKTtcbiAgICAgICAgICAgIGlmIChsb2MgPT09ICdmcm9tJyB8fCBsb2MgPT09ICd0bycpIHtcbiAgICAgICAgICAgICAgaWYgKGRvbWFpbiA9PT0gJ3NlZ21lbnQnKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBvbC5zdHlsZS5TdHlsZSh7XG4gICAgICAgICAgICAgICAgICBpbWFnZTogbmV3IG9sLnN0eWxlLkNpcmNsZSh7XG4gICAgICAgICAgICAgICAgICAgIHJhZGl1czogNyxcbiAgICAgICAgICAgICAgICAgICAgZmlsbDogbmV3IG9sLnN0eWxlLkZpbGwoe2NvbG9yOiBbMCwgMCwgMjU1LCBdfSlcbiAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoZG9tYWluID09PSAndG91cicpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IG9sLnN0eWxlLlN0eWxlKHtcbiAgICAgICAgICAgICAgICAgIGltYWdlOiBuZXcgb2wuc3R5bGUuQ2lyY2xlKHtcbiAgICAgICAgICAgICAgICAgICAgcmFkaXVzOiA1LFxuICAgICAgICAgICAgICAgICAgICBmaWxsOiBuZXcgb2wuc3R5bGUuRmlsbCh7Y29sb3I6IFswLCAyNTUsIDI1NSwgMC42XX0pXG4gICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcmV0dXJuIG5ldyBvbC5zdHlsZS5TdHlsZSh7XG4gICAgICAgICAgICBzdHJva2U6IG5ldyBvbC5zdHlsZS5TdHJva2Uoe1xuICAgICAgICAgICAgICB3aWR0aDogMyxcbiAgICAgICAgICAgICAgY29sb3I6IFsyNTUsIDAsIDAsIDFdXG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIGZpbGw6IG5ldyBvbC5zdHlsZS5GaWxsKHtcbiAgICAgICAgICAgICAgY29sb3I6IFswLCAwLCAyNTUsIDAuNl1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0sXG4gICAgICB9KVxuICAgIF07XG5cbiAgICB0aGlzLm1hcCA9IG5ldyBvbC5NYXAoe1xuICAgICAgY29udHJvbHM6IG9sLmNvbnRyb2wuZGVmYXVsdHMoe3JvdGF0ZTogZmFsc2V9KS5leHRlbmQoW1xuICAgICAgICBuZXcgb2wuY29udHJvbC5GdWxsU2NyZWVuKCksXG4gICAgICAgIG5ldyBvbC5jb250cm9sLk92ZXJ2aWV3TWFwKHtcbiAgICAgICAgICBsYXllcnM6IFtcbiAgICAgICAgICAgIG5ldyBvbC5sYXllci5UaWxlKHtzb3VyY2U6IHRoaXMuc291cmNlfSlcbiAgICAgICAgICBdXG4gICAgICAgIH0pLFxuICAgICAgICBuZXcgb2wuY29udHJvbC5TY2FsZUxpbmUoe21pbldpZHRoOiAxMjB9KVxuICAgICAgXSksXG4gICAgICBpbnRlcmFjdGlvbnM6IG9sLmludGVyYWN0aW9uLmRlZmF1bHRzKHtcbiAgICAgICAgYWx0U2hpZnREcmFnUm90YXRlOiBmYWxzZSxcbiAgICAgICAgcGluY2hSb3RhdGU6IGZhbHNlXG4gICAgICB9KSxcbiAgICAgIGxheWVycyxcbiAgICAgIG92ZXJsYXlzOiBbdGhpcy5vdmVybGF5XSxcbiAgICAgIHRhcmdldDogY29uZmlnLm1hcCxcbiAgICAgIHZpZXc6IG5ldyBvbC5WaWV3KHtcbiAgICAgICAgY2VudGVyOiBvbC5wcm9qLmZyb21Mb25MYXQoW2NvbmZpZy5sb25naXR1ZGUsIGNvbmZpZy5sYXRpdHVkZV0pLFxuICAgICAgICB6b29tOiBjb25maWcuem9vbVxuICAgICAgfSlcbiAgICB9KTtcblxuICAgIC8vIHRvZG86IG1vdmUgdGhlbSBiYWNrIHRvIGNvbXBvbmVudCwgd2hlcmUgaXQgYmVsb25ncyB0by5cbiAgICB0aGlzLm1hcC5vbigncG9pbnRlcm1vdmUnLCAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0IHBpeGVsID0gdGhpcy5tYXAuZ2V0RXZlbnRQaXhlbChldmVudC5vcmlnaW5hbEV2ZW50KTtcbiAgICAgIGNvbnN0IGhpdCA9IHRoaXMubWFwLmhhc0ZlYXR1cmVBdFBpeGVsKHBpeGVsLCB7aGl0VG9sZXJhbmNlOiAxMH0pO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQodGhpcy5tYXAuZ2V0VGFyZ2V0KCkpLnN0eWxlLmN1cnNvciA9IGhpdCA/ICdwb2ludGVyJyA6ICcnO1xuICAgIH0pO1xuICB9XG5cbiAgb25TaW5nbGVDbGljayhmbjogQ2FsbGFibGVGdW5jdGlvbikge1xuICAgIHRoaXMubWFwLm9uKCdzaW5nbGVjbGljaycsIChldmVudCkgPT4ge1xuICAgICAgY29uc3QgdXNlZEV2ZW50cyA9IFtdO1xuICAgICAgY29uc3QgdXNlZFRvdXJTZWdtZW50cyA9IFtdO1xuICAgICAgdGhpcy5tYXAuZm9yRWFjaEZlYXR1cmVBdFBpeGVsKGV2ZW50LnBpeGVsLCAoZmVhdHVyZSwgbGF5ZXIpID0+IHtcbiAgICAgICAgY29uc3QgaWQ6IHN0cmluZyA9IGZlYXR1cmUuZ2V0SWQoKTtcbiAgICAgICAgaWYgKGlkID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5ldmVudHNbaWRdKSB7XG4gICAgICAgICAgdXNlZEV2ZW50cy5wdXNoKHRoaXMuZXZlbnRzW2lkXSk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucG9pbnRzW2lkXSkge1xuICAgICAgICAgIGNvbnN0IFtkb21haW4sIGxvYywgZW50cnlJZF0gPSBpZC5zcGxpdCgvXFxfLyk7XG5cbiAgICAgICAgICBpZiAodGhpcy50b3VyU2VnbWVudHNbZG9tYWluICsgJ18nICsgZW50cnlJZF0pIHtcbiAgICAgICAgICAgIHVzZWRUb3VyU2VnbWVudHMucHVzaCh0aGlzLnRvdXJTZWdtZW50c1tkb21haW4gKyAnXycgKyBlbnRyeUlkXSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9LCB7aGl0VG9sZXJhbmNlOiAxMH0pO1xuICAgICAgdXNlZFRvdXJTZWdtZW50cy5zb3J0KCh4LCB5KSA9PiB4IC0geSk7XG4gICAgICB1c2VkRXZlbnRzLnNvcnQoKHgsIHkpID0+IHggLSB5KTtcblxuICAgICAgbGV0IGxuZyA9IDA7XG4gICAgICBsZXQgbGF0ID0gMDtcbiAgICAgIGxldCBsZW5ndGggPSAwO1xuXG4gICAgICB1c2VkRXZlbnRzLmZvckVhY2godG91ckV2ZW50ID0+IHtcbiAgICAgICAgbG5nICs9IHRvdXJFdmVudC5wb2ludC5sbmc7XG4gICAgICAgIGxhdCArPSB0b3VyRXZlbnQucG9pbnQubGF0O1xuICAgICAgfSk7XG5cbiAgICAgIGxlbmd0aCArPSB1c2VkRXZlbnRzLmxlbmd0aDtcbiAgICAgIC8vIHVzZWRUb3VyU2VnbWVudHMuZm9yRWFjaChzZWdtZW50ID0+IHtcbiAgICAgIC8vICAgbG5nICs9IHNlZ21lbnQudG8ubG5nIC0gc2VnbWVudC5mcm9tLmxuZztcbiAgICAgIC8vICAgbGF0ICs9IHNlZ21lbnQudG8ubGF0IC0gc2VnbWVudC5mcm9tLmxhdDtcbiAgICAgIC8vIH0pO1xuICAgICAgLy8gbGVuZ3RoICs9IHVzZWRUb3VyU2VnbWVudHMubGVuZ3RoO1xuXG4gICAgICBsbmcgLz0gbGVuZ3RoO1xuICAgICAgbGF0IC89IGxlbmd0aDtcblxuICAgICAgaWYgKHVzZWRFdmVudHMubGVuZ3RoIHx8IHVzZWRUb3VyU2VnbWVudHMubGVuZ3RoKSB7XG4gICAgICAgIHRoaXMub3ZlcmxheS5zZXRQb3NpdGlvbihvbC5wcm9qLmZyb21Mb25MYXQoW2xuZywgbGF0XSkpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5vdmVybGF5LnNldFBvc2l0aW9uKHVuZGVmaW5lZCk7XG4gICAgICB9XG5cbiAgICAgIGZuKHtldmVudHM6IHVzZWRFdmVudHMsIHNlZ21lbnRzOiB1c2VkVG91clNlZ21lbnRzfSk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGRhdGVUb0NvbG9yID0gIChkYXRlKSA9PiB7XG4gICAgY29uc3QgbWluRGF0ZSA9IG5ldyBEYXRlKGRhdGUuZ2V0RnVsbFllYXIoKSwgZGF0ZS5nZXRNb250aCgpIC0gMSwgMCk7XG4gICAgY29uc3QgbWF4RGF0ZSA9IG5ldyBEYXRlKGRhdGUuZ2V0RnVsbFllYXIoKSwgZGF0ZS5nZXRNb250aCgpLCAwKTtcbiAgICByZXR1cm4gJ2hzbCgnICsgKChkYXRlLmdldFRpbWUoKSAtIG1pbkRhdGUuZ2V0VGltZSgpKSAvIChtYXhEYXRlLmdldFRpbWUoKSAtIG1pbkRhdGUuZ2V0VGltZSgpKSkgKyAndHVybiwgMTAwJSwgNTAlKSc7XG4gIH1cbn1cbiJdfQ==