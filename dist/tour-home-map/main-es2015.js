(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "../../node_modules/raw-loader/dist/cjs.js!./src/app/components/app.component.html":
/*!*****************************************************************************************************************************************************!*\
  !*** /home/maximilian/OpenSource/DevelopersForFuture/tour-de-rebel-map/node_modules/raw-loader/dist/cjs.js!./src/app/components/app.component.html ***!
  \*****************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-map [latitude]=\"51.48253\" [longitude]=\"7.21506\" [zoom]=\"5\"></app-map>");

/***/ }),

/***/ "../../node_modules/raw-loader/dist/cjs.js!./src/app/components/map/map.component.html":
/*!*********************************************************************************************************************************************************!*\
  !*** /home/maximilian/OpenSource/DevelopersForFuture/tour-de-rebel-map/node_modules/raw-loader/dist/cjs.js!./src/app/components/map/map.component.html ***!
  \*********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"map-container\">\n  <div class=\"map-frame\">\n    <div id=\"map\"></div>\n  </div>\n  <div id=\"popup\" class=\"ol-popup\">\n    <ul data-title=\"Events\" *ngIf=\"eventsInPopup.length\">\n      <li *ngFor=\"let event of eventsInPopup\">\n        <a href=\"{{ event.url }}\" [title]=\"getEventTitle(event, false)\">{{ getEventTitle(event, true) }}</a>\n      </li>\n    </ul>\n    <ul data-title=\"Segments\" *ngIf=\"segmentsInPopup.length\">\n      <li *ngFor=\"let segment of segmentsInPopup\">\n        {{ getSegmentTitle(segment) }}\n      </li>\n    </ul>\n  </div>\n</div>\n");

/***/ }),

/***/ "../../node_modules/tslib/tslib.es6.js":
/*!*********************************************************************************************************!*\
  !*** /home/maximilian/OpenSource/DevelopersForFuture/tour-de-rebel-map/node_modules/tslib/tslib.es6.js ***!
  \*********************************************************************************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "../tour-commons/src/lib/model/index.ts":
/*!**********************************************!*\
  !*** ../tour-commons/src/lib/model/index.ts ***!
  \**********************************************/
/*! exports provided: TourOrganizerType, TourSegmentModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _tour_organizer_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tour_organizer.model */ "../tour-commons/src/lib/model/tour_organizer.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TourOrganizerType", function() { return _tour_organizer_model__WEBPACK_IMPORTED_MODULE_1__["TourOrganizerType"]; });

/* harmony import */ var _tour_segment_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tour_segment.model */ "../tour-commons/src/lib/model/tour_segment.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TourSegmentModel", function() { return _tour_segment_model__WEBPACK_IMPORTED_MODULE_2__["TourSegmentModel"]; });






/***/ }),

/***/ "../tour-commons/src/lib/model/tour_organizer.model.ts":
/*!*************************************************************!*\
  !*** ../tour-commons/src/lib/model/tour_organizer.model.ts ***!
  \*************************************************************/
/*! exports provided: TourOrganizerType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TourOrganizerType", function() { return TourOrganizerType; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");

var TourOrganizerType;
(function (TourOrganizerType) {
    TourOrganizerType["SINGLE"] = "single";
    TourOrganizerType["GROUP"] = "group";
})(TourOrganizerType || (TourOrganizerType = {}));


/***/ }),

/***/ "../tour-commons/src/lib/model/tour_segment.model.ts":
/*!***********************************************************!*\
  !*** ../tour-commons/src/lib/model/tour_segment.model.ts ***!
  \***********************************************************/
/*! exports provided: TourSegmentModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TourSegmentModel", function() { return TourSegmentModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");

class TourSegmentModel {
}


/***/ }),

/***/ "../tour-commons/src/lib/services/index.ts":
/*!*************************************************!*\
  !*** ../tour-commons/src/lib/services/index.ts ***!
  \*************************************************/
/*! exports provided: services, OlMapDecoratorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "services", function() { return services; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ol_map_decorator_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ol-map-decorator.service */ "../tour-commons/src/lib/services/ol-map-decorator.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OlMapDecoratorService", function() { return _ol_map_decorator_service__WEBPACK_IMPORTED_MODULE_1__["OlMapDecoratorService"]; });



const services = [
    _ol_map_decorator_service__WEBPACK_IMPORTED_MODULE_1__["OlMapDecoratorService"]
];



/***/ }),

/***/ "../tour-commons/src/lib/services/ol-map-decorator.service.ts":
/*!********************************************************************!*\
  !*** ../tour-commons/src/lib/services/ol-map-decorator.service.ts ***!
  \********************************************************************/
/*! exports provided: OlMapDecoratorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OlMapDecoratorService", function() { return OlMapDecoratorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var openlayers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! openlayers */ "../tour-commons/node_modules/openlayers/dist/ol.js");
/* harmony import */ var openlayers__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(openlayers__WEBPACK_IMPORTED_MODULE_2__);



let OlMapDecoratorService = class OlMapDecoratorService {
    constructor() {
        this.events = {};
        this.tourSegments = {};
        this.points = {};
        this.dateToColor = (date) => {
            const minDate = new Date(date.getFullYear(), date.getMonth() - 1, 0);
            const maxDate = new Date(date.getFullYear(), date.getMonth(), 0);
            return 'hsl(' + ((date.getTime() - minDate.getTime()) / (maxDate.getTime() - minDate.getTime())) + 'turn, 100%, 50%)';
        };
    }
    initMap(config, geoData) {
        this.source = new openlayers__WEBPACK_IMPORTED_MODULE_2__["source"].OSM();
        this.popup = document.getElementById(config.popup);
        this.overlay = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Overlay"]({ element: this.popup });
        const entries = {};
        const features = [];
        for (const [eventId, event] of Object.entries(geoData.events)) {
            const feature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].Point(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([event.point.lng, event.point.lat])));
            const id = 'event_' + eventId;
            feature.setId(id);
            features.push(feature);
            this.events[id] = event;
        }
        for (const [counter, tour] of Object.entries(geoData.tours)) {
            const tourId = tour.id;
            const tourFromFeature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].Point(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tour.from.lng, tour.from.lat])));
            tourFromFeature.setId('tour_from_' + tourId);
            features.push(tourFromFeature);
            this.points['tour_from_' + tourId] = tour.from;
            const tourToFeature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].Point(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tour.from.lng, tour.from.lat])));
            tourToFeature.setId('tour_to_' + tourId);
            features.push(tourToFeature);
            this.points['tour_to_' + tourId] = tour.to;
            for (const [segmentCounter, tourSegment] of Object.entries(tour.segments)) {
                const segmentId = tourSegment.id;
                tourSegment.tour = tour;
                const feature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].LineString([
                    openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tourSegment.from.lng, tourSegment.from.lat]),
                    openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tourSegment.to.lng, tourSegment.to.lat])
                ]));
                feature.setId('segment_' + segmentId);
                features.push(feature);
                this.tourSegments['segment_' + segmentId] = tourSegment;
                const fromFeature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].Point(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                fromFeature.setId('segment_from_' + segmentId);
                features.push(fromFeature);
                this.points['segment_from_' + segmentId] = tourSegment.from;
                const toFeature = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Feature"](new openlayers__WEBPACK_IMPORTED_MODULE_2__["geom"].Point(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([tourSegment.from.lng, tourSegment.from.lat])));
                toFeature.setId('segment_to_' + segmentId);
                features.push(toFeature);
                this.points['segment_to_' + segmentId] = tourSegment.to;
            }
        }
        const layers = [
            new openlayers__WEBPACK_IMPORTED_MODULE_2__["layer"].Tile({ source: this.source }),
            new openlayers__WEBPACK_IMPORTED_MODULE_2__["layer"].Vector({
                source: new openlayers__WEBPACK_IMPORTED_MODULE_2__["source"].Vector({ features }),
                zIndex: Infinity,
                style: (feature) => {
                    const id = feature.getId();
                    if (this.events[id]) {
                        const entry = this.events[id];
                        return new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Style({
                            image: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Circle({
                                radius: 6,
                                fill: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Fill({ color: [225, 0, 255,] })
                            })
                        });
                    }
                    if (this.points[id]) {
                        const [domain, loc, entryId] = id.split(/\_/);
                        if (loc === 'from' || loc === 'to') {
                            if (domain === 'segment') {
                                return new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Style({
                                    image: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Circle({
                                        radius: 7,
                                        fill: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Fill({ color: [0, 0, 255,] })
                                    })
                                });
                            }
                            else if (domain === 'tour') {
                                return new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Style({
                                    image: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Circle({
                                        radius: 5,
                                        fill: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Fill({ color: [0, 255, 255, 0.6] })
                                    })
                                });
                            }
                        }
                    }
                    return new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Style({
                        stroke: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Stroke({
                            width: 3,
                            color: [255, 0, 0, 1]
                        }),
                        fill: new openlayers__WEBPACK_IMPORTED_MODULE_2__["style"].Fill({
                            color: [0, 0, 255, 0.6]
                        })
                    });
                },
            })
        ];
        this.map = new openlayers__WEBPACK_IMPORTED_MODULE_2__["Map"]({
            controls: openlayers__WEBPACK_IMPORTED_MODULE_2__["control"].defaults({ rotate: false }).extend([
                new openlayers__WEBPACK_IMPORTED_MODULE_2__["control"].FullScreen(),
                new openlayers__WEBPACK_IMPORTED_MODULE_2__["control"].OverviewMap({
                    layers: [
                        new openlayers__WEBPACK_IMPORTED_MODULE_2__["layer"].Tile({ source: this.source })
                    ]
                }),
                new openlayers__WEBPACK_IMPORTED_MODULE_2__["control"].ScaleLine({ minWidth: 120 })
            ]),
            interactions: openlayers__WEBPACK_IMPORTED_MODULE_2__["interaction"].defaults({
                altShiftDragRotate: false,
                pinchRotate: false
            }),
            layers,
            overlays: [this.overlay],
            target: config.map,
            view: new openlayers__WEBPACK_IMPORTED_MODULE_2__["View"]({
                center: openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([config.longitude, config.latitude]),
                zoom: config.zoom
            })
        });
        // todo: move them back to component, where it belongs to.
        this.map.on('pointermove', (event) => {
            const pixel = this.map.getEventPixel(event.originalEvent);
            const hit = this.map.hasFeatureAtPixel(pixel, { hitTolerance: 10 });
            document.getElementById(this.map.getTarget()).style.cursor = hit ? 'pointer' : '';
        });
    }
    onSingleClick(fn) {
        this.map.on('singleclick', (event) => {
            const usedEvents = [];
            const usedTourSegments = [];
            this.map.forEachFeatureAtPixel(event.pixel, (feature, layer) => {
                const id = feature.getId();
                if (id === undefined) {
                    return;
                }
                if (this.events[id]) {
                    usedEvents.push(this.events[id]);
                    return;
                }
                else if (this.points[id]) {
                    const [domain, loc, entryId] = id.split(/\_/);
                    if (this.tourSegments[domain + '_' + entryId]) {
                        usedTourSegments.push(this.tourSegments[domain + '_' + entryId]);
                        return;
                    }
                }
            }, { hitTolerance: 10 });
            usedTourSegments.sort((x, y) => x - y);
            usedEvents.sort((x, y) => x - y);
            let lng = 0;
            let lat = 0;
            let length = 0;
            usedEvents.forEach(tourEvent => {
                lng += tourEvent.point.lng;
                lat += tourEvent.point.lat;
            });
            length += usedEvents.length;
            // usedTourSegments.forEach(segment => {
            //   lng += segment.to.lng - segment.from.lng;
            //   lat += segment.to.lat - segment.from.lat;
            // });
            // length += usedTourSegments.length;
            lng /= length;
            lat /= length;
            if (usedEvents.length || usedTourSegments.length) {
                this.overlay.setPosition(openlayers__WEBPACK_IMPORTED_MODULE_2__["proj"].fromLonLat([lng, lat]));
            }
            else {
                this.overlay.setPosition(undefined);
            }
            fn({ events: usedEvents, segments: usedTourSegments });
        });
    }
};
OlMapDecoratorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], OlMapDecoratorService);



/***/ }),

/***/ "../tour-commons/src/lib/tour-commons.component.ts":
/*!*********************************************************!*\
  !*** ../tour-commons/src/lib/tour-commons.component.ts ***!
  \*********************************************************/
/*! exports provided: TourCommonsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TourCommonsComponent", function() { return TourCommonsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm2015/core.js");


let TourCommonsComponent = class TourCommonsComponent {
};
TourCommonsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'tour-commons',
        template: `
        <h1>Tour CommonsComponent</h1>
    `
    })
], TourCommonsComponent);



/***/ }),

/***/ "../tour-commons/src/lib/tour-commons.module.ts":
/*!******************************************************!*\
  !*** ../tour-commons/src/lib/tour-commons.module.ts ***!
  \******************************************************/
/*! exports provided: TourCommonsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TourCommonsModule", function() { return TourCommonsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _tour_commons_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tour-commons.component */ "../tour-commons/src/lib/tour-commons.component.ts");



let TourCommonsModule = class TourCommonsModule {
};
TourCommonsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_tour_commons_component__WEBPACK_IMPORTED_MODULE_2__["TourCommonsComponent"]],
        imports: [],
        exports: [_tour_commons_component__WEBPACK_IMPORTED_MODULE_2__["TourCommonsComponent"]]
    })
], TourCommonsModule);



/***/ }),

/***/ "../tour-commons/src/public-api.ts":
/*!*****************************************!*\
  !*** ../tour-commons/src/public-api.ts ***!
  \*****************************************/
/*! exports provided: TourCommonsModule, services, TourOrganizerType, TourSegmentModel, OlMapDecoratorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _lib_tour_commons_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./lib/tour-commons.module */ "../tour-commons/src/lib/tour-commons.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TourCommonsModule", function() { return _lib_tour_commons_module__WEBPACK_IMPORTED_MODULE_1__["TourCommonsModule"]; });

/* harmony import */ var _lib_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./lib/model */ "../tour-commons/src/lib/model/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TourOrganizerType", function() { return _lib_model__WEBPACK_IMPORTED_MODULE_2__["TourOrganizerType"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TourSegmentModel", function() { return _lib_model__WEBPACK_IMPORTED_MODULE_2__["TourSegmentModel"]; });

/* harmony import */ var _lib_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./lib/services */ "../tour-commons/src/lib/services/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "services", function() { return _lib_services__WEBPACK_IMPORTED_MODULE_3__["services"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OlMapDecoratorService", function() { return _lib_services__WEBPACK_IMPORTED_MODULE_3__["OlMapDecoratorService"]; });

/*
 * Public API Surface of tour-commons
 */






/***/ }),

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/components/app.component.scss":
/*!***********************************************!*\
  !*** ./src/app/components/app.component.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9qZWN0cy90b3VyLWhvbWUtbWFwL3NyYy9hcHAvY29tcG9uZW50cy9hcHAuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/components/app.component.ts":
/*!*********************************************!*\
  !*** ./src/app/components/app.component.ts ***!
  \*********************************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'tour-home-map';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "../../node_modules/raw-loader/dist/cjs.js!./src/app/components/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/components/app.component.scss")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/components/index.ts":
/*!*************************************!*\
  !*** ./src/app/components/index.ts ***!
  \*************************************/
/*! exports provided: components, AppComponent, MapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "components", function() { return components; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component */ "./src/app/components/app.component.ts");
/* harmony import */ var _map_map_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./map/map.component */ "./src/app/components/map/map.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return _app_component__WEBPACK_IMPORTED_MODULE_1__["AppComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MapComponent", function() { return _map_map_component__WEBPACK_IMPORTED_MODULE_2__["MapComponent"]; });




const components = [
    _app_component__WEBPACK_IMPORTED_MODULE_1__["AppComponent"],
    _map_map_component__WEBPACK_IMPORTED_MODULE_2__["MapComponent"],
];




/***/ }),

/***/ "./src/app/components/map/map.component.scss":
/*!***************************************************!*\
  !*** ./src/app/components/map/map.component.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".map-container {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  margin: 30px;\n}\n\n.map-frame {\n  border: 2px solid black;\n  height: 100%;\n}\n\n#map {\n  height: 100%;\n}\n\n.ol-box {\n  box-sizing: border-box;\n  border-radius: 2px;\n  border: 2px solid #00f;\n}\n\n.ol-mouse-position {\n  top: 8px;\n  right: 8px;\n  position: absolute;\n}\n\n.ol-scale-line {\n  background: rgba(0, 60, 136, 0.3);\n  border-radius: 4px;\n  bottom: 8px;\n  left: 8px;\n  padding: 2px;\n  position: absolute;\n}\n\n.ol-scale-line-inner {\n  border: 1px solid #eee;\n  border-top: none;\n  color: #eee;\n  font-size: 10px;\n  text-align: center;\n  margin: 1px;\n  will-change: contents, width;\n  -webkit-transition: all 0.25s;\n  transition: all 0.25s;\n}\n\n.ol-scale-bar {\n  position: absolute;\n  bottom: 8px;\n  left: 8px;\n}\n\n.ol-scale-step-marker {\n  width: 1px;\n  height: 15px;\n  background-color: #000;\n  float: right;\n  z-Index: 10;\n}\n\n.ol-scale-step-text {\n  position: absolute;\n  bottom: -5px;\n  font-size: 12px;\n  z-Index: 11;\n  color: #000;\n  text-shadow: -2px 0 #fff, 0 2px #fff, 2px 0 #fff, 0 -2px #fff;\n}\n\n.ol-scale-text {\n  position: absolute;\n  font-size: 14px;\n  text-align: center;\n  bottom: 25px;\n  color: #000;\n  text-shadow: -2px 0 #fff, 0 2px #fff, 2px 0 #fff, 0 -2px #fff;\n}\n\n.ol-scale-singlebar {\n  position: relative;\n  height: 10px;\n  z-Index: 9;\n  border: 1px solid #000;\n}\n\n.ol-unsupported {\n  display: none;\n}\n\n.ol-unselectable, .ol-viewport {\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  -webkit-tap-highlight-color: transparent;\n}\n\n.ol-selectable {\n  -webkit-touch-callout: default;\n  -webkit-user-select: text;\n  -moz-user-select: text;\n  -ms-user-select: text;\n  user-select: text;\n}\n\n.ol-grabbing {\n  cursor: -webkit-grabbing;\n  cursor: grabbing;\n}\n\n.ol-grab {\n  cursor: move;\n  cursor: -webkit-grab;\n  cursor: grab;\n}\n\n.ol-control {\n  position: absolute;\n  background-color: rgba(255, 255, 255, 0.4);\n  border-radius: 4px;\n  padding: 2px;\n}\n\n.ol-control:hover {\n  background-color: rgba(255, 255, 255, 0.6);\n}\n\n.ol-zoom {\n  top: 0.5em;\n  left: 0.5em;\n}\n\n.ol-rotate {\n  top: 0.5em;\n  right: 0.5em;\n  -webkit-transition: opacity 0.25s linear, visibility 0s linear;\n  transition: opacity 0.25s linear, visibility 0s linear;\n}\n\n.ol-rotate.ol-hidden {\n  opacity: 0;\n  visibility: hidden;\n  -webkit-transition: opacity 0.25s linear, visibility 0s linear 0.25s;\n  transition: opacity 0.25s linear, visibility 0s linear 0.25s;\n}\n\n.ol-zoom-extent {\n  top: 4.643em;\n  left: 0.5em;\n}\n\n.ol-full-screen {\n  right: 0.5em;\n  top: 0.5em;\n}\n\n.ol-control button {\n  display: block;\n  margin: 1px;\n  padding: 0;\n  color: #fff;\n  font-size: 1.14em;\n  font-weight: 700;\n  text-decoration: none;\n  text-align: center;\n  height: 1.375em;\n  width: 1.375em;\n  line-height: 0.4em;\n  background-color: rgba(0, 60, 136, 0.5);\n  border: none;\n  border-radius: 2px;\n}\n\n.ol-control button::-moz-focus-inner {\n  border: none;\n  padding: 0;\n}\n\n.ol-zoom-extent button {\n  line-height: 1.4em;\n}\n\n.ol-compass {\n  display: block;\n  font-weight: 400;\n  font-size: 1.2em;\n  will-change: transform;\n}\n\n.ol-touch .ol-control button {\n  font-size: 1.5em;\n}\n\n.ol-touch .ol-zoom-extent {\n  top: 5.5em;\n}\n\n.ol-control button:focus, .ol-control button:hover {\n  text-decoration: none;\n  background-color: rgba(0, 60, 136, 0.7);\n}\n\n.ol-zoom .ol-zoom-in {\n  border-radius: 2px 2px 0 0;\n}\n\n.ol-zoom .ol-zoom-out {\n  border-radius: 0 0 2px 2px;\n}\n\n.ol-attribution {\n  text-align: right;\n  bottom: 0.5em;\n  right: 0.5em;\n  max-width: calc(100% - 1.3em);\n}\n\n.ol-attribution ul {\n  margin: 0;\n  padding: 0 0.5em;\n  color: #000;\n  text-shadow: 0 0 2px #fff;\n}\n\n.ol-attribution li {\n  display: inline;\n  list-style: none;\n}\n\n.ol-attribution li:not(:last-child):after {\n  content: \" \";\n}\n\n.ol-attribution img {\n  max-height: 2em;\n  max-width: inherit;\n  vertical-align: middle;\n}\n\n.ol-attribution button, .ol-attribution ul {\n  display: inline-block;\n}\n\n.ol-attribution.ol-collapsed ul {\n  display: none;\n}\n\n.ol-attribution:not(.ol-collapsed) {\n  background: rgba(255, 255, 255, 0.8);\n}\n\n.ol-attribution.ol-uncollapsible {\n  bottom: 0;\n  right: 0;\n  border-radius: 4px 0 0;\n}\n\n.ol-attribution.ol-uncollapsible img {\n  margin-top: -0.2em;\n  max-height: 1.6em;\n}\n\n.ol-attribution.ol-uncollapsible button {\n  display: none;\n}\n\n.ol-zoomslider {\n  top: 4.5em;\n  left: 0.5em;\n  height: 200px;\n}\n\n.ol-zoomslider button {\n  position: relative;\n  height: 10px;\n}\n\n.ol-touch .ol-zoomslider {\n  top: 5.5em;\n}\n\n.ol-overviewmap {\n  left: 0.5em;\n  bottom: 0.5em;\n}\n\n.ol-overviewmap.ol-uncollapsible {\n  bottom: 0;\n  left: 0;\n  border-radius: 0 4px 0 0;\n}\n\n.ol-overviewmap .ol-overviewmap-map, .ol-overviewmap button {\n  display: inline-block;\n}\n\n.ol-overviewmap .ol-overviewmap-map {\n  border: 1px solid #7b98bc;\n  height: 150px;\n  margin: 2px;\n  width: 150px;\n}\n\n.ol-overviewmap:not(.ol-collapsed) button {\n  bottom: 1px;\n  left: 2px;\n  position: absolute;\n}\n\n.ol-overviewmap.ol-collapsed .ol-overviewmap-map, .ol-overviewmap.ol-uncollapsible button {\n  display: none;\n}\n\n.ol-overviewmap:not(.ol-collapsed) {\n  background: rgba(255, 255, 255, 0.8);\n}\n\n.ol-overviewmap-box {\n  border: 2px dotted rgba(0, 60, 136, 0.7);\n}\n\n.ol-overviewmap .ol-overviewmap-box:hover {\n  cursor: move;\n}\n\n/*# sourceMappingURL=ol.css.map */\n\n.ol-popup {\n  position: absolute;\n  background-color: white;\n  -webkit-filter: drop-shadow(0 1px 4px rgba(0, 0, 0, 0.2));\n          filter: drop-shadow(0 1px 4px rgba(0, 0, 0, 0.2));\n  border-radius: 10px;\n  border: 1px solid #cccccc;\n  bottom: 12px;\n  left: -50px;\n  white-space: nowrap;\n  z-index: 20;\n}\n\n.ol-popup:after, .ol-popup:before {\n  top: 100%;\n  border: solid transparent;\n  content: \" \";\n  height: 0;\n  width: 0;\n  position: absolute;\n  pointer-events: none;\n}\n\n.ol-popup:after {\n  border-top-color: white;\n  border-width: 10px;\n  left: 48px;\n  margin-left: -10px;\n}\n\n.ol-popup:before {\n  border-top-color: #cccccc;\n  border-width: 11px;\n  left: 48px;\n  margin-left: -11px;\n}\n\n.ol-popup ul {\n  margin: 0.5em 1em;\n  list-style: none;\n}\n\n.ol-popup ul::before {\n  content: attr(data-title);\n  font-weight: bold;\n}\n\n.ol-scale-bar {\n  top: 0.5em;\n  bottom: unset;\n  left: unset;\n  right: 3.25em;\n  margin-right: 14px;\n  margin-top: 3px;\n}\n\n.ol-scale-step-text {\n  text-shadow: 0 2px 2px #fff, 0 -2px 2px #fff, 2px 0 2px #fff, -2px 0 2px #fff;\n}\n\n#osm-de-rebel {\n  height: 500px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL21heGltaWxpYW4vT3BlblNvdXJjZS9EZXZlbG9wZXJzRm9yRnV0dXJlL3RvdXItZGUtcmViZWwtbWFwL3Byb2plY3RzL3RvdXItaG9tZS1tYXAvc3JjL2FwcC9jb21wb25lbnRzL21hcC9tYXAuY29tcG9uZW50LnNjc3MiLCJwcm9qZWN0cy90b3VyLWhvbWUtbWFwL3NyYy9hcHAvY29tcG9uZW50cy9tYXAvbWFwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtBQ0NGOztBREVBO0VBQ0UsdUJBQUE7RUFDQSxZQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0FDQ0Y7O0FERUE7RUFBUSxzQkFBQTtFQUFzQixrQkFBQTtFQUFrQixzQkFBQTtBQ0loRDs7QURKc0U7RUFBbUIsUUFBQTtFQUFRLFVBQUE7RUFBVSxrQkFBQTtBQ1UzRzs7QURWNkg7RUFBZSxpQ0FBQTtFQUE2QixrQkFBQTtFQUFrQixXQUFBO0VBQVcsU0FBQTtFQUFTLFlBQUE7RUFBWSxrQkFBQTtBQ21CM047O0FEbkI2TztFQUFxQixzQkFBQTtFQUFzQixnQkFBQTtFQUFnQixXQUFBO0VBQVcsZUFBQTtFQUFlLGtCQUFBO0VBQWtCLFdBQUE7RUFBVyw0QkFBQTtFQUEyQiw2QkFBQTtFQUFBLHFCQUFBO0FDOEIxWDs7QUQ5QjhZO0VBQWMsa0JBQUE7RUFBa0IsV0FBQTtFQUFXLFNBQUE7QUNvQ3piOztBRHBDa2M7RUFBc0IsVUFBQTtFQUFVLFlBQUE7RUFBWSxzQkFBQTtFQUFzQixZQUFBO0VBQVksV0FBQTtBQzRDaGhCOztBRDVDMmhCO0VBQW9CLGtCQUFBO0VBQWtCLFlBQUE7RUFBWSxlQUFBO0VBQWUsV0FBQTtFQUFXLFdBQUE7RUFBVyw2REFBQTtBQ3FEbG5COztBRHJENHFCO0VBQWUsa0JBQUE7RUFBa0IsZUFBQTtFQUFlLGtCQUFBO0VBQWtCLFlBQUE7RUFBWSxXQUFBO0VBQVcsNkRBQUE7QUM4RHJ3Qjs7QUQ5RCt6QjtFQUFvQixrQkFBQTtFQUFrQixZQUFBO0VBQVksVUFBQTtFQUFVLHNCQUFBO0FDcUUzM0I7O0FEckVpNUI7RUFBZ0IsYUFBQTtBQ3lFajZCOztBRHpFODZCO0VBQThCLDJCQUFBO0VBQTJCLHlCQUFBO0VBQXlCLHNCQUFBO0VBQXNCLHFCQUFBO0VBQXFCLGlCQUFBO0VBQWlCLHdDQUFBO0FDa0Y1akM7O0FEbEZvbUM7RUFBZSw4QkFBQTtFQUE4Qix5QkFBQTtFQUF5QixzQkFBQTtFQUFzQixxQkFBQTtFQUFxQixpQkFBQTtBQzBGcnRDOztBRDFGc3VDO0VBQWEsd0JBQUE7RUFBNkMsZ0JBQUE7QUNnR2h5Qzs7QURoR2d6QztFQUFTLFlBQUE7RUFBWSxvQkFBQTtFQUFxQyxZQUFBO0FDdUcxMkM7O0FEdkdzM0M7RUFBWSxrQkFBQTtFQUFrQiwwQ0FBQTtFQUFzQyxrQkFBQTtFQUFrQixZQUFBO0FDOEc1OEM7O0FEOUd3OUM7RUFBa0IsMENBQUE7QUNrSDErQzs7QURsSGdoRDtFQUFTLFVBQUE7RUFBUyxXQUFBO0FDdUhsaUQ7O0FEdkg0aUQ7RUFBVyxVQUFBO0VBQVMsWUFBQTtFQUFXLDhEQUFBO0VBQUEsc0RBQUE7QUM2SDNrRDs7QUQ3SCtuRDtFQUFxQixVQUFBO0VBQVUsa0JBQUE7RUFBa0Isb0VBQUE7RUFBQSw0REFBQTtBQ21JaHJEOztBRG5JeXVEO0VBQWdCLFlBQUE7RUFBWSxXQUFBO0FDd0lyd0Q7O0FEeEkrd0Q7RUFBZ0IsWUFBQTtFQUFXLFVBQUE7QUM2STF5RDs7QUQ3SW16RDtFQUFtQixjQUFBO0VBQWMsV0FBQTtFQUFXLFVBQUE7RUFBVSxXQUFBO0VBQVcsaUJBQUE7RUFBaUIsZ0JBQUE7RUFBZ0IscUJBQUE7RUFBcUIsa0JBQUE7RUFBa0IsZUFBQTtFQUFlLGNBQUE7RUFBYyxrQkFBQTtFQUFpQix1Q0FBQTtFQUFtQyxZQUFBO0VBQVksa0JBQUE7QUM4SnpoRTs7QUQ5SjJpRTtFQUFxQyxZQUFBO0VBQVksVUFBQTtBQ21LNWxFOztBRG5Lc21FO0VBQXVCLGtCQUFBO0FDdUs3bkU7O0FEdksrb0U7RUFBWSxjQUFBO0VBQWMsZ0JBQUE7RUFBZ0IsZ0JBQUE7RUFBZ0Isc0JBQUE7QUM4S3pzRTs7QUQ5Syt0RTtFQUE2QixnQkFBQTtBQ2tMNXZFOztBRGxMNHdFO0VBQTBCLFVBQUE7QUNzTHR5RTs7QUR0TGd6RTtFQUFrRCxxQkFBQTtFQUFxQix1Q0FBQTtBQzJMdjNFOztBRDNMMDVFO0VBQXFCLDBCQUFBO0FDK0wvNkU7O0FEL0x5OEU7RUFBc0IsMEJBQUE7QUNtTS85RTs7QURuTXkvRTtFQUFnQixpQkFBQTtFQUFpQixhQUFBO0VBQVksWUFBQTtFQUFXLDZCQUFBO0FDME1qakY7O0FEMU04a0Y7RUFBbUIsU0FBQTtFQUFTLGdCQUFBO0VBQWUsV0FBQTtFQUFXLHlCQUFBO0FDaU5wb0Y7O0FEak42cEY7RUFBbUIsZUFBQTtFQUFlLGdCQUFBO0FDc04vckY7O0FEdE4rc0Y7RUFBMEMsWUFBQTtBQzBOenZGOztBRDFOcXdGO0VBQW9CLGVBQUE7RUFBZSxrQkFBQTtFQUFrQixzQkFBQTtBQ2dPMXpGOztBRGhPZzFGO0VBQTBDLHFCQUFBO0FDb08xM0Y7O0FEcE8rNEY7RUFBZ0MsYUFBQTtBQ3dPLzZGOztBRHhPNDdGO0VBQW1DLG9DQUFBO0FDNE8vOUY7O0FENU8rL0Y7RUFBaUMsU0FBQTtFQUFTLFFBQUE7RUFBUSxzQkFBQTtBQ2tQampHOztBRGxQdWtHO0VBQXFDLGtCQUFBO0VBQWlCLGlCQUFBO0FDdVA3bkc7O0FEdlA4b0c7RUFBd0MsYUFBQTtBQzJQdHJHOztBRDNQbXNHO0VBQWUsVUFBQTtFQUFVLFdBQUE7RUFBVSxhQUFBO0FDaVF0dUc7O0FEalFtdkc7RUFBc0Isa0JBQUE7RUFBa0IsWUFBQTtBQ3NRM3hHOztBRHRRdXlHO0VBQXlCLFVBQUE7QUMwUWgwRzs7QUQxUTAwRztFQUFnQixXQUFBO0VBQVUsYUFBQTtBQytRcDJHOztBRC9RZzNHO0VBQWlDLFNBQUE7RUFBUyxPQUFBO0VBQU8sd0JBQUE7QUNxUmo2Rzs7QURyUnk3RztFQUEyRCxxQkFBQTtBQ3lScC9HOztBRHpSeWdIO0VBQW9DLHlCQUFBO0VBQXlCLGFBQUE7RUFBYSxXQUFBO0VBQVcsWUFBQTtBQ2dTOWxIOztBRGhTMG1IO0VBQTBDLFdBQUE7RUFBVyxTQUFBO0VBQVMsa0JBQUE7QUNzU3hxSDs7QUR0UzBySDtFQUF5RixhQUFBO0FDMFNueEg7O0FEMVNneUg7RUFBbUMsb0NBQUE7QUM4U24wSDs7QUQ5U20ySDtFQUFvQix3Q0FBQTtBQ2tUdjNIOztBRGxUMjVIO0VBQTBDLFlBQUE7QUNzVHI4SDs7QURyVEEsaUNBQUE7O0FBQ0E7RUFDQyxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EseURBQUE7VUFBQSxpREFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtBQ3dURDs7QURyVEE7RUFDQyxTQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0FDd1REOztBRHJUQTtFQUNDLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7QUN3VEQ7O0FEclRBO0VBQ0MseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtBQ3dURDs7QURyVEE7RUFDQyxpQkFBQTtFQUNBLGdCQUFBO0FDd1REOztBRHJUQTtFQUNDLHlCQUFBO0VBQ0EsaUJBQUE7QUN3VEQ7O0FEclRBO0VBQ0MsVUFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ3dURDs7QURyVEE7RUFDQyw2RUFBQTtBQ3dURDs7QURwVEE7RUFDSSxhQUFBO0FDdVRKIiwiZmlsZSI6InByb2plY3RzL3RvdXItaG9tZS1tYXAvc3JjL2FwcC9jb21wb25lbnRzL21hcC9tYXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFwLWNvbnRhaW5lciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgYm90dG9tOiAwO1xuICBtYXJnaW46IDMwcHg7XG59XG5cbi5tYXAtZnJhbWUge1xuICBib3JkZXI6IDJweCBzb2xpZCBibGFjaztcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4jbWFwIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4ub2wtYm94e2JveC1zaXppbmc6Ym9yZGVyLWJveDtib3JkZXItcmFkaXVzOjJweDtib3JkZXI6MnB4IHNvbGlkICMwMGZ9Lm9sLW1vdXNlLXBvc2l0aW9ue3RvcDo4cHg7cmlnaHQ6OHB4O3Bvc2l0aW9uOmFic29sdXRlfS5vbC1zY2FsZS1saW5le2JhY2tncm91bmQ6cmdiYSgwLDYwLDEzNiwuMyk7Ym9yZGVyLXJhZGl1czo0cHg7Ym90dG9tOjhweDtsZWZ0OjhweDtwYWRkaW5nOjJweDtwb3NpdGlvbjphYnNvbHV0ZX0ub2wtc2NhbGUtbGluZS1pbm5lcntib3JkZXI6MXB4IHNvbGlkICNlZWU7Ym9yZGVyLXRvcDpub25lO2NvbG9yOiNlZWU7Zm9udC1zaXplOjEwcHg7dGV4dC1hbGlnbjpjZW50ZXI7bWFyZ2luOjFweDt3aWxsLWNoYW5nZTpjb250ZW50cyx3aWR0aDt0cmFuc2l0aW9uOmFsbCAuMjVzfS5vbC1zY2FsZS1iYXJ7cG9zaXRpb246YWJzb2x1dGU7Ym90dG9tOjhweDtsZWZ0OjhweH0ub2wtc2NhbGUtc3RlcC1tYXJrZXJ7d2lkdGg6MXB4O2hlaWdodDoxNXB4O2JhY2tncm91bmQtY29sb3I6IzAwMDtmbG9hdDpyaWdodDt6LUluZGV4OjEwfS5vbC1zY2FsZS1zdGVwLXRleHR7cG9zaXRpb246YWJzb2x1dGU7Ym90dG9tOi01cHg7Zm9udC1zaXplOjEycHg7ei1JbmRleDoxMTtjb2xvcjojMDAwO3RleHQtc2hhZG93Oi0ycHggMCAjZmZmLDAgMnB4ICNmZmYsMnB4IDAgI2ZmZiwwIC0ycHggI2ZmZn0ub2wtc2NhbGUtdGV4dHtwb3NpdGlvbjphYnNvbHV0ZTtmb250LXNpemU6MTRweDt0ZXh0LWFsaWduOmNlbnRlcjtib3R0b206MjVweDtjb2xvcjojMDAwO3RleHQtc2hhZG93Oi0ycHggMCAjZmZmLDAgMnB4ICNmZmYsMnB4IDAgI2ZmZiwwIC0ycHggI2ZmZn0ub2wtc2NhbGUtc2luZ2xlYmFye3Bvc2l0aW9uOnJlbGF0aXZlO2hlaWdodDoxMHB4O3otSW5kZXg6OTtib3JkZXI6MXB4IHNvbGlkICMwMDB9Lm9sLXVuc3VwcG9ydGVke2Rpc3BsYXk6bm9uZX0ub2wtdW5zZWxlY3RhYmxlLC5vbC12aWV3cG9ydHstd2Via2l0LXRvdWNoLWNhbGxvdXQ6bm9uZTstd2Via2l0LXVzZXItc2VsZWN0Om5vbmU7LW1vei11c2VyLXNlbGVjdDpub25lOy1tcy11c2VyLXNlbGVjdDpub25lO3VzZXItc2VsZWN0Om5vbmU7LXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOnRyYW5zcGFyZW50fS5vbC1zZWxlY3RhYmxley13ZWJraXQtdG91Y2gtY2FsbG91dDpkZWZhdWx0Oy13ZWJraXQtdXNlci1zZWxlY3Q6dGV4dDstbW96LXVzZXItc2VsZWN0OnRleHQ7LW1zLXVzZXItc2VsZWN0OnRleHQ7dXNlci1zZWxlY3Q6dGV4dH0ub2wtZ3JhYmJpbmd7Y3Vyc29yOi13ZWJraXQtZ3JhYmJpbmc7Y3Vyc29yOi1tb3otZ3JhYmJpbmc7Y3Vyc29yOmdyYWJiaW5nfS5vbC1ncmFie2N1cnNvcjptb3ZlO2N1cnNvcjotd2Via2l0LWdyYWI7Y3Vyc29yOi1tb3otZ3JhYjtjdXJzb3I6Z3JhYn0ub2wtY29udHJvbHtwb3NpdGlvbjphYnNvbHV0ZTtiYWNrZ3JvdW5kLWNvbG9yOnJnYmEoMjU1LDI1NSwyNTUsLjQpO2JvcmRlci1yYWRpdXM6NHB4O3BhZGRpbmc6MnB4fS5vbC1jb250cm9sOmhvdmVye2JhY2tncm91bmQtY29sb3I6cmdiYSgyNTUsMjU1LDI1NSwuNil9Lm9sLXpvb217dG9wOi41ZW07bGVmdDouNWVtfS5vbC1yb3RhdGV7dG9wOi41ZW07cmlnaHQ6LjVlbTt0cmFuc2l0aW9uOm9wYWNpdHkgLjI1cyBsaW5lYXIsdmlzaWJpbGl0eSAwcyBsaW5lYXJ9Lm9sLXJvdGF0ZS5vbC1oaWRkZW57b3BhY2l0eTowO3Zpc2liaWxpdHk6aGlkZGVuO3RyYW5zaXRpb246b3BhY2l0eSAuMjVzIGxpbmVhcix2aXNpYmlsaXR5IDBzIGxpbmVhciAuMjVzfS5vbC16b29tLWV4dGVudHt0b3A6NC42NDNlbTtsZWZ0Oi41ZW19Lm9sLWZ1bGwtc2NyZWVue3JpZ2h0Oi41ZW07dG9wOi41ZW19Lm9sLWNvbnRyb2wgYnV0dG9ue2Rpc3BsYXk6YmxvY2s7bWFyZ2luOjFweDtwYWRkaW5nOjA7Y29sb3I6I2ZmZjtmb250LXNpemU6MS4xNGVtO2ZvbnQtd2VpZ2h0OjcwMDt0ZXh0LWRlY29yYXRpb246bm9uZTt0ZXh0LWFsaWduOmNlbnRlcjtoZWlnaHQ6MS4zNzVlbTt3aWR0aDoxLjM3NWVtO2xpbmUtaGVpZ2h0Oi40ZW07YmFja2dyb3VuZC1jb2xvcjpyZ2JhKDAsNjAsMTM2LC41KTtib3JkZXI6bm9uZTtib3JkZXItcmFkaXVzOjJweH0ub2wtY29udHJvbCBidXR0b246Oi1tb3otZm9jdXMtaW5uZXJ7Ym9yZGVyOm5vbmU7cGFkZGluZzowfS5vbC16b29tLWV4dGVudCBidXR0b257bGluZS1oZWlnaHQ6MS40ZW19Lm9sLWNvbXBhc3N7ZGlzcGxheTpibG9jaztmb250LXdlaWdodDo0MDA7Zm9udC1zaXplOjEuMmVtO3dpbGwtY2hhbmdlOnRyYW5zZm9ybX0ub2wtdG91Y2ggLm9sLWNvbnRyb2wgYnV0dG9ue2ZvbnQtc2l6ZToxLjVlbX0ub2wtdG91Y2ggLm9sLXpvb20tZXh0ZW50e3RvcDo1LjVlbX0ub2wtY29udHJvbCBidXR0b246Zm9jdXMsLm9sLWNvbnRyb2wgYnV0dG9uOmhvdmVye3RleHQtZGVjb3JhdGlvbjpub25lO2JhY2tncm91bmQtY29sb3I6cmdiYSgwLDYwLDEzNiwuNyl9Lm9sLXpvb20gLm9sLXpvb20taW57Ym9yZGVyLXJhZGl1czoycHggMnB4IDAgMH0ub2wtem9vbSAub2wtem9vbS1vdXR7Ym9yZGVyLXJhZGl1czowIDAgMnB4IDJweH0ub2wtYXR0cmlidXRpb257dGV4dC1hbGlnbjpyaWdodDtib3R0b206LjVlbTtyaWdodDouNWVtO21heC13aWR0aDpjYWxjKDEwMCUgLSAxLjNlbSl9Lm9sLWF0dHJpYnV0aW9uIHVse21hcmdpbjowO3BhZGRpbmc6MCAuNWVtO2NvbG9yOiMwMDA7dGV4dC1zaGFkb3c6MCAwIDJweCAjZmZmfS5vbC1hdHRyaWJ1dGlvbiBsaXtkaXNwbGF5OmlubGluZTtsaXN0LXN0eWxlOm5vbmV9Lm9sLWF0dHJpYnV0aW9uIGxpOm5vdCg6bGFzdC1jaGlsZCk6YWZ0ZXJ7Y29udGVudDpcIiBcIn0ub2wtYXR0cmlidXRpb24gaW1ne21heC1oZWlnaHQ6MmVtO21heC13aWR0aDppbmhlcml0O3ZlcnRpY2FsLWFsaWduOm1pZGRsZX0ub2wtYXR0cmlidXRpb24gYnV0dG9uLC5vbC1hdHRyaWJ1dGlvbiB1bHtkaXNwbGF5OmlubGluZS1ibG9ja30ub2wtYXR0cmlidXRpb24ub2wtY29sbGFwc2VkIHVse2Rpc3BsYXk6bm9uZX0ub2wtYXR0cmlidXRpb246bm90KC5vbC1jb2xsYXBzZWQpe2JhY2tncm91bmQ6cmdiYSgyNTUsMjU1LDI1NSwuOCl9Lm9sLWF0dHJpYnV0aW9uLm9sLXVuY29sbGFwc2libGV7Ym90dG9tOjA7cmlnaHQ6MDtib3JkZXItcmFkaXVzOjRweCAwIDB9Lm9sLWF0dHJpYnV0aW9uLm9sLXVuY29sbGFwc2libGUgaW1ne21hcmdpbi10b3A6LS4yZW07bWF4LWhlaWdodDoxLjZlbX0ub2wtYXR0cmlidXRpb24ub2wtdW5jb2xsYXBzaWJsZSBidXR0b257ZGlzcGxheTpub25lfS5vbC16b29tc2xpZGVye3RvcDo0LjVlbTtsZWZ0Oi41ZW07aGVpZ2h0OjIwMHB4fS5vbC16b29tc2xpZGVyIGJ1dHRvbntwb3NpdGlvbjpyZWxhdGl2ZTtoZWlnaHQ6MTBweH0ub2wtdG91Y2ggLm9sLXpvb21zbGlkZXJ7dG9wOjUuNWVtfS5vbC1vdmVydmlld21hcHtsZWZ0Oi41ZW07Ym90dG9tOi41ZW19Lm9sLW92ZXJ2aWV3bWFwLm9sLXVuY29sbGFwc2libGV7Ym90dG9tOjA7bGVmdDowO2JvcmRlci1yYWRpdXM6MCA0cHggMCAwfS5vbC1vdmVydmlld21hcCAub2wtb3ZlcnZpZXdtYXAtbWFwLC5vbC1vdmVydmlld21hcCBidXR0b257ZGlzcGxheTppbmxpbmUtYmxvY2t9Lm9sLW92ZXJ2aWV3bWFwIC5vbC1vdmVydmlld21hcC1tYXB7Ym9yZGVyOjFweCBzb2xpZCAjN2I5OGJjO2hlaWdodDoxNTBweDttYXJnaW46MnB4O3dpZHRoOjE1MHB4fS5vbC1vdmVydmlld21hcDpub3QoLm9sLWNvbGxhcHNlZCkgYnV0dG9ue2JvdHRvbToxcHg7bGVmdDoycHg7cG9zaXRpb246YWJzb2x1dGV9Lm9sLW92ZXJ2aWV3bWFwLm9sLWNvbGxhcHNlZCAub2wtb3ZlcnZpZXdtYXAtbWFwLC5vbC1vdmVydmlld21hcC5vbC11bmNvbGxhcHNpYmxlIGJ1dHRvbntkaXNwbGF5Om5vbmV9Lm9sLW92ZXJ2aWV3bWFwOm5vdCgub2wtY29sbGFwc2VkKXtiYWNrZ3JvdW5kOnJnYmEoMjU1LDI1NSwyNTUsLjgpfS5vbC1vdmVydmlld21hcC1ib3h7Ym9yZGVyOjJweCBkb3R0ZWQgcmdiYSgwLDYwLDEzNiwuNyl9Lm9sLW92ZXJ2aWV3bWFwIC5vbC1vdmVydmlld21hcC1ib3g6aG92ZXJ7Y3Vyc29yOm1vdmV9XG4vKiMgc291cmNlTWFwcGluZ1VSTD1vbC5jc3MubWFwICovXG4ub2wtcG9wdXAge1xuXHRwb3NpdGlvbjogYWJzb2x1dGU7XG5cdGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuXHRmaWx0ZXI6IGRyb3Atc2hhZG93KDAgMXB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMikpO1xuXHRib3JkZXItcmFkaXVzOiAxMHB4O1xuXHRib3JkZXI6IDFweCBzb2xpZCAjY2NjY2NjO1xuXHRib3R0b206IDEycHg7XG5cdGxlZnQ6IC01MHB4O1xuXHR3aGl0ZS1zcGFjZTogbm93cmFwO1xuXHR6LWluZGV4OiAyMDtcbn1cblxuLm9sLXBvcHVwOmFmdGVyLCAub2wtcG9wdXA6YmVmb3JlIHtcblx0dG9wOiAxMDAlO1xuXHRib3JkZXI6IHNvbGlkIHRyYW5zcGFyZW50O1xuXHRjb250ZW50OiBcIiBcIjtcblx0aGVpZ2h0OiAwO1xuXHR3aWR0aDogMDtcblx0cG9zaXRpb246IGFic29sdXRlO1xuXHRwb2ludGVyLWV2ZW50czogbm9uZTtcbn1cblxuLm9sLXBvcHVwOmFmdGVyIHtcblx0Ym9yZGVyLXRvcC1jb2xvcjogd2hpdGU7XG5cdGJvcmRlci13aWR0aDogMTBweDtcblx0bGVmdDogNDhweDtcblx0bWFyZ2luLWxlZnQ6IC0xMHB4O1xufVxuXG4ub2wtcG9wdXA6YmVmb3JlIHtcblx0Ym9yZGVyLXRvcC1jb2xvcjogI2NjY2NjYztcblx0Ym9yZGVyLXdpZHRoOiAxMXB4O1xuXHRsZWZ0OiA0OHB4O1xuXHRtYXJnaW4tbGVmdDogLTExcHg7XG59XG5cbi5vbC1wb3B1cCB1bCB7XG5cdG1hcmdpbjogMC41ZW0gMWVtO1xuXHRsaXN0LXN0eWxlOiBub25lO1xufVxuXG4ub2wtcG9wdXAgdWw6OmJlZm9yZSB7XG5cdGNvbnRlbnQ6IGF0dHIoZGF0YS10aXRsZSk7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ub2wtc2NhbGUtYmFye1xuXHR0b3A6IC41ZW07XG5cdGJvdHRvbTogdW5zZXQ7XG5cdGxlZnQ6IHVuc2V0O1xuXHRyaWdodDogMy4yNWVtO1xuXHRtYXJnaW4tcmlnaHQ6IDE0cHg7XG5cdG1hcmdpbi10b3A6IDNweDtcbn1cblxuLm9sLXNjYWxlLXN0ZXAtdGV4dCB7XG5cdHRleHQtc2hhZG93OiAwIDJweCAycHggI2ZmZiwgMCAtMnB4IDJweCAjZmZmLCAycHggMCAycHggI2ZmZiwgLTJweCAwIDJweCAjZmZmO1xufVxuXG5cbiNvc20tZGUtcmViZWwge1xuICAgIGhlaWdodDogNTAwcHg7XG59IiwiLm1hcC1jb250YWluZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgbWFyZ2luOiAzMHB4O1xufVxuXG4ubWFwLWZyYW1lIHtcbiAgYm9yZGVyOiAycHggc29saWQgYmxhY2s7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuI21hcCB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLm9sLWJveCB7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgYm9yZGVyOiAycHggc29saWQgIzAwZjtcbn1cblxuLm9sLW1vdXNlLXBvc2l0aW9uIHtcbiAgdG9wOiA4cHg7XG4gIHJpZ2h0OiA4cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cblxuLm9sLXNjYWxlLWxpbmUge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDYwLCAxMzYsIDAuMyk7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm90dG9tOiA4cHg7XG4gIGxlZnQ6IDhweDtcbiAgcGFkZGluZzogMnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5vbC1zY2FsZS1saW5lLWlubmVyIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2VlZTtcbiAgYm9yZGVyLXRvcDogbm9uZTtcbiAgY29sb3I6ICNlZWU7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDFweDtcbiAgd2lsbC1jaGFuZ2U6IGNvbnRlbnRzLCB3aWR0aDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMjVzO1xufVxuXG4ub2wtc2NhbGUtYmFyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDhweDtcbiAgbGVmdDogOHB4O1xufVxuXG4ub2wtc2NhbGUtc3RlcC1tYXJrZXIge1xuICB3aWR0aDogMXB4O1xuICBoZWlnaHQ6IDE1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG4gIGZsb2F0OiByaWdodDtcbiAgei1JbmRleDogMTA7XG59XG5cbi5vbC1zY2FsZS1zdGVwLXRleHQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogLTVweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB6LUluZGV4OiAxMTtcbiAgY29sb3I6ICMwMDA7XG4gIHRleHQtc2hhZG93OiAtMnB4IDAgI2ZmZiwgMCAycHggI2ZmZiwgMnB4IDAgI2ZmZiwgMCAtMnB4ICNmZmY7XG59XG5cbi5vbC1zY2FsZS10ZXh0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBmb250LXNpemU6IDE0cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm90dG9tOiAyNXB4O1xuICBjb2xvcjogIzAwMDtcbiAgdGV4dC1zaGFkb3c6IC0ycHggMCAjZmZmLCAwIDJweCAjZmZmLCAycHggMCAjZmZmLCAwIC0ycHggI2ZmZjtcbn1cblxuLm9sLXNjYWxlLXNpbmdsZWJhciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiAxMHB4O1xuICB6LUluZGV4OiA5O1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDAwO1xufVxuXG4ub2wtdW5zdXBwb3J0ZWQge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4ub2wtdW5zZWxlY3RhYmxlLCAub2wtdmlld3BvcnQge1xuICAtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7XG4gIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XG4gIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XG4gIC1tcy11c2VyLXNlbGVjdDogbm9uZTtcbiAgdXNlci1zZWxlY3Q6IG5vbmU7XG4gIC13ZWJraXQtdGFwLWhpZ2hsaWdodC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5cbi5vbC1zZWxlY3RhYmxlIHtcbiAgLXdlYmtpdC10b3VjaC1jYWxsb3V0OiBkZWZhdWx0O1xuICAtd2Via2l0LXVzZXItc2VsZWN0OiB0ZXh0O1xuICAtbW96LXVzZXItc2VsZWN0OiB0ZXh0O1xuICAtbXMtdXNlci1zZWxlY3Q6IHRleHQ7XG4gIHVzZXItc2VsZWN0OiB0ZXh0O1xufVxuXG4ub2wtZ3JhYmJpbmcge1xuICBjdXJzb3I6IC13ZWJraXQtZ3JhYmJpbmc7XG4gIGN1cnNvcjogLW1vei1ncmFiYmluZztcbiAgY3Vyc29yOiBncmFiYmluZztcbn1cblxuLm9sLWdyYWIge1xuICBjdXJzb3I6IG1vdmU7XG4gIGN1cnNvcjogLXdlYmtpdC1ncmFiO1xuICBjdXJzb3I6IC1tb3otZ3JhYjtcbiAgY3Vyc29yOiBncmFiO1xufVxuXG4ub2wtY29udHJvbCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjQpO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIHBhZGRpbmc6IDJweDtcbn1cblxuLm9sLWNvbnRyb2w6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XG59XG5cbi5vbC16b29tIHtcbiAgdG9wOiAwLjVlbTtcbiAgbGVmdDogMC41ZW07XG59XG5cbi5vbC1yb3RhdGUge1xuICB0b3A6IDAuNWVtO1xuICByaWdodDogMC41ZW07XG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMC4yNXMgbGluZWFyLCB2aXNpYmlsaXR5IDBzIGxpbmVhcjtcbn1cblxuLm9sLXJvdGF0ZS5vbC1oaWRkZW4ge1xuICBvcGFjaXR5OiAwO1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMC4yNXMgbGluZWFyLCB2aXNpYmlsaXR5IDBzIGxpbmVhciAwLjI1cztcbn1cblxuLm9sLXpvb20tZXh0ZW50IHtcbiAgdG9wOiA0LjY0M2VtO1xuICBsZWZ0OiAwLjVlbTtcbn1cblxuLm9sLWZ1bGwtc2NyZWVuIHtcbiAgcmlnaHQ6IDAuNWVtO1xuICB0b3A6IDAuNWVtO1xufVxuXG4ub2wtY29udHJvbCBidXR0b24ge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAxcHg7XG4gIHBhZGRpbmc6IDA7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDEuMTRlbTtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGhlaWdodDogMS4zNzVlbTtcbiAgd2lkdGg6IDEuMzc1ZW07XG4gIGxpbmUtaGVpZ2h0OiAwLjRlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCA2MCwgMTM2LCAwLjUpO1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDJweDtcbn1cblxuLm9sLWNvbnRyb2wgYnV0dG9uOjotbW96LWZvY3VzLWlubmVyIHtcbiAgYm9yZGVyOiBub25lO1xuICBwYWRkaW5nOiAwO1xufVxuXG4ub2wtem9vbS1leHRlbnQgYnV0dG9uIHtcbiAgbGluZS1oZWlnaHQ6IDEuNGVtO1xufVxuXG4ub2wtY29tcGFzcyB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXdlaWdodDogNDAwO1xuICBmb250LXNpemU6IDEuMmVtO1xuICB3aWxsLWNoYW5nZTogdHJhbnNmb3JtO1xufVxuXG4ub2wtdG91Y2ggLm9sLWNvbnRyb2wgYnV0dG9uIHtcbiAgZm9udC1zaXplOiAxLjVlbTtcbn1cblxuLm9sLXRvdWNoIC5vbC16b29tLWV4dGVudCB7XG4gIHRvcDogNS41ZW07XG59XG5cbi5vbC1jb250cm9sIGJ1dHRvbjpmb2N1cywgLm9sLWNvbnRyb2wgYnV0dG9uOmhvdmVyIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDYwLCAxMzYsIDAuNyk7XG59XG5cbi5vbC16b29tIC5vbC16b29tLWluIHtcbiAgYm9yZGVyLXJhZGl1czogMnB4IDJweCAwIDA7XG59XG5cbi5vbC16b29tIC5vbC16b29tLW91dCB7XG4gIGJvcmRlci1yYWRpdXM6IDAgMCAycHggMnB4O1xufVxuXG4ub2wtYXR0cmlidXRpb24ge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgYm90dG9tOiAwLjVlbTtcbiAgcmlnaHQ6IDAuNWVtO1xuICBtYXgtd2lkdGg6IGNhbGMoMTAwJSAtIDEuM2VtKTtcbn1cblxuLm9sLWF0dHJpYnV0aW9uIHVsIHtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwIDAuNWVtO1xuICBjb2xvcjogIzAwMDtcbiAgdGV4dC1zaGFkb3c6IDAgMCAycHggI2ZmZjtcbn1cblxuLm9sLWF0dHJpYnV0aW9uIGxpIHtcbiAgZGlzcGxheTogaW5saW5lO1xuICBsaXN0LXN0eWxlOiBub25lO1xufVxuXG4ub2wtYXR0cmlidXRpb24gbGk6bm90KDpsYXN0LWNoaWxkKTphZnRlciB7XG4gIGNvbnRlbnQ6IFwiIFwiO1xufVxuXG4ub2wtYXR0cmlidXRpb24gaW1nIHtcbiAgbWF4LWhlaWdodDogMmVtO1xuICBtYXgtd2lkdGg6IGluaGVyaXQ7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG5cbi5vbC1hdHRyaWJ1dGlvbiBidXR0b24sIC5vbC1hdHRyaWJ1dGlvbiB1bCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cblxuLm9sLWF0dHJpYnV0aW9uLm9sLWNvbGxhcHNlZCB1bCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5vbC1hdHRyaWJ1dGlvbjpub3QoLm9sLWNvbGxhcHNlZCkge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOCk7XG59XG5cbi5vbC1hdHRyaWJ1dGlvbi5vbC11bmNvbGxhcHNpYmxlIHtcbiAgYm90dG9tOiAwO1xuICByaWdodDogMDtcbiAgYm9yZGVyLXJhZGl1czogNHB4IDAgMDtcbn1cblxuLm9sLWF0dHJpYnV0aW9uLm9sLXVuY29sbGFwc2libGUgaW1nIHtcbiAgbWFyZ2luLXRvcDogLTAuMmVtO1xuICBtYXgtaGVpZ2h0OiAxLjZlbTtcbn1cblxuLm9sLWF0dHJpYnV0aW9uLm9sLXVuY29sbGFwc2libGUgYnV0dG9uIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLm9sLXpvb21zbGlkZXIge1xuICB0b3A6IDQuNWVtO1xuICBsZWZ0OiAwLjVlbTtcbiAgaGVpZ2h0OiAyMDBweDtcbn1cblxuLm9sLXpvb21zbGlkZXIgYnV0dG9uIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDEwcHg7XG59XG5cbi5vbC10b3VjaCAub2wtem9vbXNsaWRlciB7XG4gIHRvcDogNS41ZW07XG59XG5cbi5vbC1vdmVydmlld21hcCB7XG4gIGxlZnQ6IDAuNWVtO1xuICBib3R0b206IDAuNWVtO1xufVxuXG4ub2wtb3ZlcnZpZXdtYXAub2wtdW5jb2xsYXBzaWJsZSB7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgYm9yZGVyLXJhZGl1czogMCA0cHggMCAwO1xufVxuXG4ub2wtb3ZlcnZpZXdtYXAgLm9sLW92ZXJ2aWV3bWFwLW1hcCwgLm9sLW92ZXJ2aWV3bWFwIGJ1dHRvbiB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cblxuLm9sLW92ZXJ2aWV3bWFwIC5vbC1vdmVydmlld21hcC1tYXAge1xuICBib3JkZXI6IDFweCBzb2xpZCAjN2I5OGJjO1xuICBoZWlnaHQ6IDE1MHB4O1xuICBtYXJnaW46IDJweDtcbiAgd2lkdGg6IDE1MHB4O1xufVxuXG4ub2wtb3ZlcnZpZXdtYXA6bm90KC5vbC1jb2xsYXBzZWQpIGJ1dHRvbiB7XG4gIGJvdHRvbTogMXB4O1xuICBsZWZ0OiAycHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cblxuLm9sLW92ZXJ2aWV3bWFwLm9sLWNvbGxhcHNlZCAub2wtb3ZlcnZpZXdtYXAtbWFwLCAub2wtb3ZlcnZpZXdtYXAub2wtdW5jb2xsYXBzaWJsZSBidXR0b24ge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4ub2wtb3ZlcnZpZXdtYXA6bm90KC5vbC1jb2xsYXBzZWQpIHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjgpO1xufVxuXG4ub2wtb3ZlcnZpZXdtYXAtYm94IHtcbiAgYm9yZGVyOiAycHggZG90dGVkIHJnYmEoMCwgNjAsIDEzNiwgMC43KTtcbn1cblxuLm9sLW92ZXJ2aWV3bWFwIC5vbC1vdmVydmlld21hcC1ib3g6aG92ZXIge1xuICBjdXJzb3I6IG1vdmU7XG59XG5cbi8qIyBzb3VyY2VNYXBwaW5nVVJMPW9sLmNzcy5tYXAgKi9cbi5vbC1wb3B1cCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGZpbHRlcjogZHJvcC1zaGFkb3coMCAxcHggNHB4IHJnYmEoMCwgMCwgMCwgMC4yKSk7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2NjY2M7XG4gIGJvdHRvbTogMTJweDtcbiAgbGVmdDogLTUwcHg7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIHotaW5kZXg6IDIwO1xufVxuXG4ub2wtcG9wdXA6YWZ0ZXIsIC5vbC1wb3B1cDpiZWZvcmUge1xuICB0b3A6IDEwMCU7XG4gIGJvcmRlcjogc29saWQgdHJhbnNwYXJlbnQ7XG4gIGNvbnRlbnQ6IFwiIFwiO1xuICBoZWlnaHQ6IDA7XG4gIHdpZHRoOiAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuXG4ub2wtcG9wdXA6YWZ0ZXIge1xuICBib3JkZXItdG9wLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXdpZHRoOiAxMHB4O1xuICBsZWZ0OiA0OHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG59XG5cbi5vbC1wb3B1cDpiZWZvcmUge1xuICBib3JkZXItdG9wLWNvbG9yOiAjY2NjY2NjO1xuICBib3JkZXItd2lkdGg6IDExcHg7XG4gIGxlZnQ6IDQ4cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTFweDtcbn1cblxuLm9sLXBvcHVwIHVsIHtcbiAgbWFyZ2luOiAwLjVlbSAxZW07XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG59XG5cbi5vbC1wb3B1cCB1bDo6YmVmb3JlIHtcbiAgY29udGVudDogYXR0cihkYXRhLXRpdGxlKTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5vbC1zY2FsZS1iYXIge1xuICB0b3A6IDAuNWVtO1xuICBib3R0b206IHVuc2V0O1xuICBsZWZ0OiB1bnNldDtcbiAgcmlnaHQ6IDMuMjVlbTtcbiAgbWFyZ2luLXJpZ2h0OiAxNHB4O1xuICBtYXJnaW4tdG9wOiAzcHg7XG59XG5cbi5vbC1zY2FsZS1zdGVwLXRleHQge1xuICB0ZXh0LXNoYWRvdzogMCAycHggMnB4ICNmZmYsIDAgLTJweCAycHggI2ZmZiwgMnB4IDAgMnB4ICNmZmYsIC0ycHggMCAycHggI2ZmZjtcbn1cblxuI29zbS1kZS1yZWJlbCB7XG4gIGhlaWdodDogNTAwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/components/map/map.component.ts":
/*!*************************************************!*\
  !*** ./src/app/components/map/map.component.ts ***!
  \*************************************************/
/*! exports provided: MapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapComponent", function() { return MapComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_geo_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/geo_data.service */ "./src/app/services/geo_data.service.ts");
/* harmony import */ var projects_tour_commons_src_public_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! projects/tour-commons/src/public-api */ "../tour-commons/src/public-api.ts");




let MapComponent = class MapComponent {
    constructor(mapService, geoDataService) {
        this.mapService = mapService;
        this.geoDataService = geoDataService;
        this.eventsInPopup = [];
        this.segmentsInPopup = [];
    }
    ngAfterViewInit() {
        this.geoDataService.getGeoEntries().subscribe((geoData) => {
            this.mapService.initMap({
                zoom: this.zoom,
                latitude: this.latitude,
                longitude: this.longitude,
                popup: 'popup',
                map: 'map'
            }, geoData);
            this.mapService.onSingleClick((event) => {
                this.eventsInPopup = event.events;
                this.segmentsInPopup = event.segments;
            });
        });
    }
    getEventTitle(event, short) {
        let title = (event.author !== undefined ? event.author + ': ' : '') + event.title + ', ';
        title += (event.point.name ? event.point.name + ', ' : '');
        if (short) {
            return title;
        }
        title += new Date(event.date_from * 1000)
            .toLocaleString(undefined, { year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit' });
        if (event.date_to) {
            title += ' - ' + new Date(event.date_to * 1000)
                .toLocaleString(undefined, { year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit' });
        }
        return title;
    }
    getSegmentTitle(segment) {
        let title = '';
        if (segment.from.name || segment.to.name) {
            title += '(' + (segment.from.name ? segment.from.name + ' - ' : '') + segment.to.name + ')';
        }
        if (segment.tour) {
            title += ' - Tour: ' + segment.tour.title;
            if (segment.tour.from.name || segment.tour.to.name) {
                title += ' (' + (segment.tour.from.name ? segment.tour.from.name + ' - ' : '') + segment.tour.to.name + ')';
            }
        }
        return title;
    }
    dateToString(dateInSeconds) {
        return new Date(dateInSeconds * 1000)
            .toLocaleString(undefined, { year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit' });
    }
};
MapComponent.ctorParameters = () => [
    { type: projects_tour_commons_src_public_api__WEBPACK_IMPORTED_MODULE_3__["OlMapDecoratorService"] },
    { type: _services_geo_data_service__WEBPACK_IMPORTED_MODULE_2__["GeoDataService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], MapComponent.prototype, "zoom", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], MapComponent.prototype, "latitude", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], MapComponent.prototype, "longitude", void 0);
MapComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-map',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./map.component.html */ "../../node_modules/raw-loader/dist/cjs.js!./src/app/components/map/map.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./map.component.scss */ "./src/app/components/map/map.component.scss")).default]
    })
], MapComponent);



/***/ }),

/***/ "./src/app/services/app.module.ts":
/*!****************************************!*\
  !*** ./src/app/services/app.module.ts ***!
  \****************************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "../../node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components */ "./src/app/components/index.ts");
/* harmony import */ var _components_app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/app.component */ "./src/app/components/app.component.ts");
/* harmony import */ var projects_tour_commons_src_public_api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! projects/tour-commons/src/public-api */ "../tour-commons/src/public-api.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/fesm2015/http.js");







let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            ..._components__WEBPACK_IMPORTED_MODULE_3__["components"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            projects_tour_commons_src_public_api__WEBPACK_IMPORTED_MODULE_5__["TourCommonsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"]
        ],
        bootstrap: [_components_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/services/geo_data.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/geo_data.service.ts ***!
  \**********************************************/
/*! exports provided: GeoDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeoDataService", function() { return GeoDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "../../node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "../../node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");






let GeoDataService = class GeoDataService {
    constructor($http) {
        this.$http = $http;
    }
    getGeoEntries() {
        return this.$http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].apiUrl + '/geo')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])((error) => Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error.json)));
    }
};
GeoDataService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] }
];
GeoDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], GeoDataService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false,
    apiUrl: 'http://localhost:3031'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "../../node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_services_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/services/app.module */ "./src/app/services/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_services_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/maximilian/OpenSource/DevelopersForFuture/tour-de-rebel-map/projects/tour-home-map/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map